		<div id="footer">
			<div id="footer_nav">
				<ul>
					<li><a href="<?php echo $copy[LANG]['rules_file']; ?>" target="_blank"><?php echo $copy[LANG]['contest_rules']; ?></a></li><li><a href="<?php echo $copy[LANG]['privacy_policy_url']; ?>" target="_blank"><?php echo $copy[LANG]['privacy_policy']; ?></a></li><li><a href="<?php echo $copy[LANG]['contact_us_url']; ?>" target="_blank"><?php echo $copy[LANG]['contact_us']; ?></a></li>
				</ul>
				<p class="copyright"><?php echo $copy[LANG]['copyright']; ?></p>
			</div>
		</div>
	</div>
	<div class="overlay">
		<div id="login_popup" class="popup">
			<div class="popup_header">
				<h3>
					<?php echo $copy[LANG]['login']; ?>
					<span class="popup_close fa-stack fa-lg">
						<i class="fa fa-circle fa-stack-2x"></i>
						<i class="fa fa-times fa-stack-1x fa-inverse"></i>
					</span>
				</h3>
			</div>
			<div class="popup_content">
				<div id="login_form_disclaimer">
					<?php echo $copy[LANG]['registration_note']; ?>
				</div>
				<form id="login_form" action="" method="post">
					<div class="form_row">
						<label for="username_email" class="info_label">
							<?php echo $copy[LANG]['username_email_label']; ?>
							<span class="info_icon"></span>
							<div class="info_box"><?php echo $copy[LANG]['username_login_info']; ?></div>
						</label>
						<input type="text" name="username_email" id="username_email" class="textfield" value="">
						<span class="error_msg"></span>
					</div>
					<div class="form_row">
						<label for="login_pass"><?php echo $copy[LANG]['password_label']; ?></label>
						<input type="password" name="password" id="login_pass" class="textfield" value="">
						<span class="error_msg"></span>
					</div>
					<div class="form_row clearfix">
						<a href="javascript:void(0);" class="forgot_password_link"><?php echo $copy[LANG]['forgot_password']; ?></a>
						<input type="submit" id="login_submit" class="btn btn-default" value="<?php echo $copy[LANG]['login']; ?>">
					</div>
					<div class="form_row">
						<div class="error_messages body_copy"></div>
					</div>
					<div class="form_row text-right">
						<p class="body_copy"><br><?php echo $copy[LANG]['not_registered']; ?><br><a href="javascript:void(0);" class="register_link"><?php echo $copy[LANG]['register_now']; ?></a></p>
					</div>
				</form>
			</div>
		</div>
		<!-- registration popup -->
		<div id="register_popup" class="popup">
			<div class="popup_header">
				<h3>
					<?php echo $copy[LANG]['register']; ?>
					<span class="popup_close fa-stack fa-lg">
						<i class="fa fa-circle fa-stack-2x"></i>
						<i class="fa fa-times fa-stack-1x fa-inverse"></i>
					</span>
				</h3>
			</div>
			<div class="popup_content">
				<form id="register_form" action="" method="post">
					<div id="regi_disclaimer">
						<?php echo $copy[LANG]['registration_note']; ?>
					</div>
					<div class="form_row col_wrapper">
						<div class="col col_6">
							<label for="first_name"><?php echo $copy[LANG]['first_name_label']; ?></label>
							<input type="text" name="first_name" id="first_name" class="textfield" value="">
							<span class="error_msg"></span>
						</div>
						<div class="col col_6">
							<label for="last_name"><?php echo $copy[LANG]['last_name_label']; ?></label>
							<input type="text" name="last_name" id="last_name" class="textfield" value="">
							<span class="error_msg"></span>
						</div>
					</div>
					<div class="form_row col_wrapper">
						<div class="col col_6">
							<label for="username" class="info_label">
								<?php echo $copy[LANG]['username_label']; ?>
								<span class="info_icon"></span>
								<div class="info_box"><?php echo $copy[LANG]['username_register_info']; ?></div>
							</label>
							<input type="text" name="username" id="username" class="textfield" value="">
							<span class="error_msg"></span>
						</div>
						<div class="col col_6">
							<label for="phone_number"><?php echo $copy[LANG]['phone_number_label']; ?></label>
							<input type="text" name="phone_number" id="phone_number" class="textfield" value="">
							<span class="error_msg"></span>
						</div>
					</div>
					<div class="form_row col_wrapper">
						<div class="col col_6">
							<label for="agency_name"><?php echo $copy[LANG]['agency_name_label']; ?></label>
							<input type="text" name="agency_name" id="agency_name" class="textfield" value="">
							<span class="error_msg"></span>
						</div>
						<div class="col col_6">
							<label for="iata_number"><?php echo $copy[LANG]['iata_number_label']; ?></label>
							<input type="text" name="iata_number" id="iata_number" class="textfield" value="">
							<span class="error_msg"></span>
						</div>
					</div>
					<div class="form_row">
						<label for="email"><?php echo $copy[LANG]['email_label']; ?></label>
						<input type="text" name="email" id="email" class="textfield" value="">
						<span class="error_msg"></span>
					</div>
					<div class="form_row">
						<label for="confirm_email"><?php echo $copy[LANG]['confirm_email_label']; ?></label>
						<input type="text" name="confirm_email" id="confirm_email" class="textfield" value="">
						<span class="error_msg"></span>
					</div>
					<div class="form_row">
						<label for="register_pass"><?php echo $copy[LANG]['password_label']; ?></label>
						<input type="password" name="password" id="register_pass" class="textfield" value="">
						<span class="error_msg"></span>
					</div>
					<div class="form_row">
						<label for="confirm_pass"><?php echo $copy[LANG]['confirm_password_label']; ?></label>
						<input type="password" name="confirm_password" id="confirm_pass" class="textfield" value="">
						<span class="error_msg"></span>
					</div>
					<div class="form_row">
						<div class="checkbox">
							<input type="checkbox" name="rules_check" id="rules_check" value="1">
							<label for="rules_check"><?php echo $copy[LANG]['rules_check_label']; ?></label>
						</div>
						<span class="error_msg"></span>
					</div>
					<div class="form_row">
						<div class="checkbox">
							<input type="checkbox" name="comm_check" id="comm_check" value="1">
							<label for="comm_check"><?php echo $copy[LANG]['comm_check_label']; ?></label>
						</div>
						<span class="error_msg"></span>
					</div>
					<div class="form_row">
						<input type="submit" id="register_submit" class="btn btn-default" value="<?php echo $copy[LANG]['register_button']; ?>">
						<label><?php echo $copy[LANG]['validation_label']; ?></label>
						<div class="captcha"></div>
						<span class="error_msg"></span>
						<script>
							$('.captcha').QapTcha({
								disabledSubmit:false,
								txtLock:'<?php echo $copy[LANG]['validation_text']; ?>',
								txtUnlock:'<?php echo $copy[LANG]['captcha_valid']; ?>',
								PHPfile:'captcha_validation.php'
						    });
						</script>
					</div>
					<div class="form_row">
						<div class="error_messages body_copy"></div>
					</div>
				</form>
			</div>
		</div>
		<div id="forgot_password_popup" class="popup">
			<div class="popup_header">
				<h3>
					<?php echo $copy[LANG]['forgot_password']; ?>
					<span class="popup_close fa-stack fa-lg">
						<i class="fa fa-circle fa-stack-2x"></i>
						<i class="fa fa-times fa-stack-1x fa-inverse"></i>
					</span>
				</h3>
			</div>
			<div class="popup_content">
				<form id="forgot_password_form" action="" method="post">
					<div class="form_row">
						<p class="body_copy"><?php echo $copy[LANG]['forgot_password_instructions']; ?></p>
					</div>
					<div class="form_row">
						<label for="forgot_email"><?php echo $copy[LANG]['email_label']; ?></label>
						<input type="text" name="email" id="forgot_email" class="textfield" value="">
						<span class="error_msg"></span>
					</div>
					<div class="form_row clearfix">
						<input type="submit" id="forgot_password_submit" class="btn btn-default" value="<?php echo $copy[LANG]['submit']; ?>">
					</div>
					<div class="form_row">
						<div class="error_messages body_copy"></div>
						<div class="success_messages body_copy"></div>
					</div>
				</form>
			</div>
		</div>
		<?php if(!empty($reset_token)): ?>
			<?php 
				$now = date('Y-m-d H:i:s');
				$sql = "SELECT * FROM user WHERE reset_password_token = '$reset_token' AND reset_password_expire > '$now'";		
				$result = $db->query($sql);
			?>
			<?php if($result->num_rows != 0): ?>
				<div id="reset_password_popup" class="popup">
					<div class="popup_header">
						<h3>
							<?php echo $copy[LANG]['reset_password']; ?>
							<span class="popup_close fa-stack fa-lg">
								<i class="fa fa-circle fa-stack-2x"></i>
								<i class="fa fa-times fa-stack-1x fa-inverse"></i>
							</span>
						</h3>
					</div>
					<div class="popup_content">
						<form id="reset_password_form" action="" method="post">
							<div class="form_row">
								<label for="new_password"><?php echo $copy[LANG]['new_password_label']; ?></label>
								<input type="password" name="password" id="new_password" class="textfield" value="">
								<span class="error_msg"></span>
							</div>
							<div class="form_row">
								<label for="confirm_new_password"><?php echo $copy[LANG]['confirm_new_password_label']; ?></label>
								<input type="password" name="confirm_password" id="confirm_new_password" class="textfield" value="">
								<span class="error_msg"></span>
							</div>
							<div class="form_row clearfix">
								<input type="hidden" name="reset_password_token" value="<?php echo $reset_token; ?>">
								<input type="submit" id="reset_submit" class="btn btn-default" value="<?php echo $copy[LANG]['submit']; ?>">
							</div>
							<div class="form_row">
								<div class="error_messages body_copy"></div>
								<div class="success_messages body_copy"></div>
							</div>
						</form>
					</div>
				</div>
				<script>
					open_popup('#reset_password_popup');
				</script>
			<?php else: ?>
				<script>
					var popup = create_popup('<?php echo $copy[LANG]['Error']; ?>', '<?php echo $copy[LANG]['reset_token_invalid']; ?>');
					open_popup(popup);
				</script>
			<?php endif; ?>
		<?php endif; ?>
	</div>
	<?php echo $analitics[LANG]; ?>
	<script> 
		var $buoop = {vs:{i:8,f:15,o:12.1,s:5.1},c:2}; 
		function $buo_f(){ 
			var e = document.createElement("script"); 
			e.src = "//browser-update.org/update.js"; 
			document.body.appendChild(e);
		};
		try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
		catch(e){window.attachEvent("onload", $buo_f)}
	</script> 
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-62258932-3', 'auto');
		ga('send', 'pageview');
	</script>
</body>
</html>