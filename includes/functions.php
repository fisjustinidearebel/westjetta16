<?php
					
	// get user
	function get_user_by_id($user_id = null){
		global $db;

		$sql = "SELECT
					user_id,
					username,
					points AS total_points
				FROM user
				WHERE user_id = $user_id";
		$result = $db->query($sql);
		$user = $result->fetch_assoc();
		return $user;
	}
	

	function get_user_leaderboard_row($user_id = null){
		global $db;

/*
		//No longer requires v_booking_points also rewritten to be more efficient
		$sql = "SELECT 
					user_id, 
					username,
					IFNULL(question_points, 0 ) AS question_points, 
					points as total_points,
					FIND_IN_SET( points, ( 
						SELECT 
							GROUP_CONCAT(DISTINCT points ORDER BY points DESC) 
						FROM user user_points
						)) AS rank, 
					(SELECT count(*) 
					 FROM user_booking 
					 WHERE user_id=$user_id
					) AS f_booking_points
					FROM user
					LEFT JOIN ( SELECT user_id,SUM(points) AS question_points FROM user_question_points GROUP BY user_id) user_question_points_sum USING(user_id)
					WHERE user_id=$user_id";
*/

		$sql = "SELECT 
					user_id,
					username,
					IFNULL(question_points, 0 ) AS question_points,
					f_booking_points,
					v_booking_points,
					points AS total_points,
					FIND_IN_SET( points, ( 
						SELECT
							GROUP_CONCAT(DISTINCT points ORDER BY points DESC)
						FROM user user_points
					)) AS rank
				FROM user
				LEFT JOIN (
					SELECT 
						user_id,
						SUM(points) AS question_points
					FROM user_question_points
					GROUP BY user_id
				) user_question_points_sum USING(user_id)
				LEFT JOIN (
					SELECT
						user_id,
						IFNULL(SUM(bt1.point_value), 0)+IFNULL(SUM(bt3.point_value), 0)+IFNULL(SUM(bt4.point_value), 0) as f_booking_points,
						IFNULL(SUM(bt2.point_value), 0) as v_booking_points
					FROM user u
					LEFT JOIN user_booking ub USING(user_id)
					LEFT JOIN booking_type bt1 ON ub.booking_type_id = bt1.booking_type_id AND bt1.booking_type_id = 1
					LEFT JOIN booking_type bt2 ON ub.booking_type_id = bt2.booking_type_id AND bt2.booking_type_id = 2
					LEFT JOIN booking_type bt3 ON ub.booking_type_id = bt3.booking_type_id AND bt3.booking_type_id = 3
					LEFT JOIN booking_type bt4 ON ub.booking_type_id = bt4.booking_type_id AND bt4.booking_type_id = 4
					GROUP BY u.user_id
				) user_booking_points USING(user_id)
				WHERE user_id = $user_id";

		$result = $db->query($sql);
		$user = $result->fetch_assoc();
		return $user;
	}



	function get_full_leaderboard($page = 1){
		global $db;

		$num_per_page = 10;
		$offset = ($page-1) * $num_per_page;
		$leaderboard = array();
		

		$sql = "SELECT 
					user_id,
					username,
					IFNULL(question_points, 0 ) AS question_points,
					f_booking_points,
					v_booking_points,
					points AS total_points,
					FIND_IN_SET( points, ( 
						SELECT
							GROUP_CONCAT(DISTINCT points ORDER BY points DESC)
						FROM user user_points
					)) AS rank
				FROM user
				LEFT JOIN (
					SELECT 
						user_id,
						SUM(points) AS question_points
					FROM user_question_points
					GROUP BY user_id
				) user_question_points_sum USING(user_id)
				LEFT JOIN (
					SELECT
						user_id,
							IFNULL(SUM(bt1.point_value), 0)+IFNULL(SUM(bt3.point_value), 0)+IFNULL(SUM(bt4.point_value), 0) as f_booking_points,
						IFNULL(SUM(bt2.point_value), 0) as v_booking_points
					FROM user u
					LEFT JOIN user_booking ub USING(user_id)
					LEFT JOIN booking_type bt1 ON ub.booking_type_id = bt1.booking_type_id AND bt1.booking_type_id = 1
					LEFT JOIN booking_type bt2 ON ub.booking_type_id = bt2.booking_type_id AND bt2.booking_type_id = 2
					LEFT JOIN booking_type bt3 ON ub.booking_type_id = bt3.booking_type_id AND bt3.booking_type_id = 3
					LEFT JOIN booking_type bt4 ON ub.booking_type_id = bt4.booking_type_id AND bt4.booking_type_id = 4
					WHERE u.points > 0
					GROUP BY u.user_id
				) user_booking_points USING(user_id)
				ORDER BY rank asc
				LIMIT $offset, $num_per_page";

/*
		$sql = "SELECT 
					user_id, 
					username,IFNULL(question_points, 0 ) AS question_points, 
					points as total_points,
					f_booking_points,
					FIND_IN_SET( points, 
						( SELECT 
							GROUP_CONCAT(DISTINCT points 
						  ORDER BY points DESC) 
						  FROM user user_points)
						) AS rank
				FROM user
				LEFT JOIN 
					( SELECT user_id,SUM(points) AS question_points 
					  FROM user_question_points 
					  GROUP BY user_id
					) user_question_points_sum USING(user_id)
				LEFT JOIN 
					( SELECT user_id,count(*) AS f_booking_points 
					  FROM user_booking 
					  GROUP BY user_id) user_booking_points_sum USING(user_id)
				ORDER BY rank ASC
				LIMIT $offset, $num_per_page";
*/
		$result = $db->query($sql);
		while($user = $result->fetch_assoc()){
			$leaderboard[$user['user_id']] = $user;
		}
		return $leaderboard;
	}
	
	// get active event
	function get_active_event(){
		global $db;
		
		$now = date('Y-m-d H:i:s');
		$sql = "SELECT
					e.*,
					COUNT(question_id) as num_questions,
					SUM(point_value) as possible_points
				FROM event e
				LEFT JOIN question q USING(event_id) 
				WHERE '$now' BETWEEN start_date AND end_date
				GROUP BY event_id
				ORDER BY start_date ASC
				LIMIT 1";
		if(!$result = $db->query($sql))
			return false;
		if($result->num_rows == 0)
			return false;

		$event = $result->fetch_assoc();
		return $event;
	}
	// get next event
	function get_next_event(){
		global $db;
		
		$now = date('Y-m-d H:i:s');
		$sql = "SELECT
					*
				FROM event
				WHERE start_date > '$now'
				ORDER BY start_date ASC
				LIMIT 1";
		if(!$result = $db->query($sql))
			return false;
		if($result->num_rows == 0)
			return false;

		$event = $result->fetch_assoc();
		return $event;
	}

	// update user points
	function update_user_points($user_id = null, $points = 0){
		global $db;
		
		if(!empty($user_id)){
			$sql = "UPDATE user
					SET points = points + $points
					WHERE user_id = $user_id";
			if(!$result = $db->query($sql))
				return false;
		}
		return true;
	}
	
	// get single question html
	function get_single_question_html($question_id = null){
		global $db, $copy;
		
		$user_id = (isset($_SESSION['user']))? $_SESSION['user']['user_id'] : null;
		$html = '';
		$sql = "SELECT 
					*
				FROM question q
				LEFT JOIN question_type qt USING(question_type_id)
				WHERE question_id = $question_id";
		if(!$result = $db->query($sql))
			return false;
		
		if($result->num_rows == 0)
			return false;
		
		while ( $question = $result->fetch_assoc() ){
			$question_correct_class = '';
			$question_answered = false;
			$answers_html = '';
			$answers = array();
			
			$sql = "SELECT 
						a.answer_id,
						a.question_id,
						text_en,
						text_fr,
						correct,
						user_answer_id
					FROM answer a
					LEFT JOIN user_answer ua 
						ON a.answer_id = ua.answer_id AND ua.user_id = $user_id
					WHERE a.question_id = ".$question['question_id']."
					ORDER BY a.answer_id";
			if(!$result2 = $db->query($sql))
				return false;
			
			while( $answer = $result2->fetch_assoc() ){
				// check if user answered this question
				$user_answered = ($answer['user_answer_id']);
				$question_answered = ($question_answered || $user_answered);
	
				$answer_correct_class = '';
				// check if answer is correct
				if($answer['correct']){
					if($answer['user_answer_id'] && $question_correct_class != 'incorrect'){
						$question_correct_class = 'correct';
					}else{
						$question_correct_class = 'incorrect';
					}
				}elseif(!$answer['correct'] && $answer['user_answer_id'] != NULL){
					$question_correct_class = 'incorrect';
				}
				
				$answers[] = array(
					'answer_id' => $answer['answer_id'],
					'text_en' => $answer['text_en'],
					'text_fr' => $answer['text_fr'],
					'correct' => $answer['correct'],
					'answered' => $user_answered
				);
			}
			
			foreach($answers as $answer){
				switch($question['question_type_id']){
					case '3':
						$answers_html .= '<li class="col col_6 '.($question_answered && $answer['correct']?'correct':'').'">';
						$answers_html .= '<div class="checkbox">';
						$answers_html .= '<input type="checkbox" id="answer_'.$answer['answer_id'].'" class="question_'.$question['question_id'].'_answers" name="question_answer[]" value="'.$answer['answer_id'].'" '.($answer['answered']?'checked':'').' '.($question_answered?'disabled':'').'>';
						$answers_html .= '<label for="answer_'.$answer['answer_id'].'">'.$answer['text_'.LANG].'</label>';
						$answers_html .= '</div>';
						$answers_html .= '</li>';
					break;
					default:
						$answers_html .= '<li class="col col_6 '.($question_answered && $answer['correct']?'correct':'').'">';
						$answers_html .= '<div class="radio">';
						$answers_html .= '<input type="radio" id="answer_'.$answer['answer_id'].'" class="question_'.$question['question_id'].'_answers" name="question_answer" value="'.$answer['answer_id'].'" '.($answer['answered']?'checked':'').' '.($question_answered?'disabled':'').'>';
						$answers_html .= '<label for="answer_'.$answer['answer_id'].'">'.$answer['text_'.LANG].'</label>';
						$answers_html .= '</div>';
						$answers_html .= '</li>';
				}				
			}
			$html .= '<div id="question_'.$question['question_id'].'" class="question open '.($question_answered?$question_correct_class:'').'">';
			$html .= '<p class="question_text">'.$question['question_text_'.LANG].'</p>';
			$html .= '<form class="question_form" action="" method="post">';
			$html .= '<div class="answers">';
			$html .= '<ul class="col_wrapper">';
			$html .= $answers_html;
			$html .= '</ul>';
			if(!$question_answered){
				$html .= '<div class="submit_bar text-right">';
				$html .= '<input type="hidden" name="question_id" value="'.$question['question_id'].'">';
				$html .= '<input type="submit" class="btn btn-default" value="Submit">';
				$html .= '</div>';
			}else{
				if($question_correct_class == 'correct')
					$html .= '<p class="question_response">'.$copy[LANG]['correct'].'<br>'.$question['correct_response_'.LANG].'</p>';
				elseif($question_correct_class == 'incorrect')
					$html .= '<p class="question_response">'.$copy[LANG]['incorrect'].'<br>'.$question['incorrect_response_'.LANG].'</p>';
			}
			$html .= '</div>';
			$html .= '</form>';
			$html .= '</div>';
		}
		return $html;
	}




	// get question html
	function get_event_questions_html($event_id = null){
		global $db, $copy;
		
		$user_id = (isset($_SESSION['user']))? $_SESSION['user']['user_id'] : null;
		$num_opened = 0;
		$html = '';
		$sql = "SELECT 
					*
				FROM question q
				LEFT JOIN question_type qt USING(question_type_id)
				WHERE event_id = $event_id
				ORDER BY question_id";
		if(!$result = $db->query($sql))
			return false;
		
		if($result->num_rows == 0)
			return false;
		
		while ( $question = $result->fetch_assoc() ){
			$question_correct_class = '';
			$question_answered = false;
			$answers_html = '';
			$answers = array();
			
			$sql = "SELECT 
						a.answer_id,
						a.question_id,
						text_en,
						text_fr,
						correct,
						user_answer_id
					FROM answer a
					LEFT JOIN user_answer ua 
						ON a.answer_id = ua.answer_id AND ua.user_id = $user_id
					WHERE a.question_id = ".$question['question_id']."
					ORDER BY a.answer_id";
			if(!$result2 = $db->query($sql))
				return false;
			
			while( $answer = $result2->fetch_assoc() ){
				// check if user answered this question
				$user_answered = ($answer['user_answer_id']);
				$question_answered = ($question_answered || $user_answered);
	
				$answer_correct_class = '';
				// check if answer is correct
				if($answer['correct']){
					if($answer['user_answer_id'] && $question_correct_class != 'incorrect'){
						$question_correct_class = 'correct';
					}else{
						$question_correct_class = 'incorrect';
					}
				}elseif(!$answer['correct'] && $answer['user_answer_id'] != NULL){
					$question_correct_class = 'incorrect';
				}
				
				$answers[] = array(
					'answer_id' => $answer['answer_id'],
					'text_en' => $answer['text_en'],
					'text_fr' => $answer['text_fr'],
					'correct' => $answer['correct'],
					'answered' => $user_answered
				);
			}
			
			foreach($answers as $answer){
				switch($question['question_type_id']){
					case '3':
						$answers_html .= '<li class="col col_6 '.($question_answered && $answer['correct']?'correct':'').'">';
						$answers_html .= '<div class="checkbox">';
						$answers_html .= '<input type="checkbox" id="answer_'.$answer['answer_id'].'" class="question_'.$question['question_id'].'_answers" name="question_answer[]" value="'.$answer['answer_id'].'" '.($answer['answered']?'checked':'').' '.($question_answered?'disabled':'').'>';
						$answers_html .= '<label for="answer_'.$answer['answer_id'].'">'.$answer['text_'.LANG].'</label>';
						$answers_html .= '</div>';
						$answers_html .= '</li>';
					break;
					default:
						$answers_html .= '<li class="col col_6 '.($question_answered && $answer['correct']?'correct':'').'">';
						$answers_html .= '<div class="radio">';
						$answers_html .= '<input type="radio" id="answer_'.$answer['answer_id'].'" class="question_'.$question['question_id'].'_answers" name="question_answer" value="'.$answer['answer_id'].'" '.($answer['answered']?'checked':'').' '.($question_answered?'disabled':'').'>';
						$answers_html .= '<label for="answer_'.$answer['answer_id'].'">'.$answer['text_'.LANG].'</label>';
						$answers_html .= '</div>';
						$answers_html .= '</li>';
				}				
			}
			$html .= '<div id="question_'.$question['question_id'].'" class="question '.(!$question_answered && ++$num_opened == 1?'open':'').' '.($question_answered?$question_correct_class:'').'">';
			$html .= '<p class="question_text">'.$question['question_text_'.LANG].'</p>';
			$html .= '<form class="question_form" action="" method="post">';
			$html .= '<div class="answers">';
			$html .= '<ul class="col_wrapper">';
			$html .= $answers_html;
			$html .= '</ul>';
			if(!$question_answered){
				$html .= '<div class="submit_bar text-right">';
				$html .= '<input type="hidden" name="question_id" value="'.$question['question_id'].'">';
				$html .= '<input type="submit" class="btn btn-default" value="'.$copy[LANG]['submit'].'">';
				$html .= '</div>';
			}else{
				if($question_correct_class == 'correct')
					$html .= '<p class="question_response">'.$copy[LANG]['correct'].'<br>'.$question['correct_response_'.LANG].'</p>';
				elseif($question_correct_class == 'incorrect')
					$html .= '<p class="question_response">'.$copy[LANG]['incorrect'].'<br>'.$question['incorrect_response_'.LANG].'</p>';
			}
			$html .= '</div>';
			$html .= '</form>';
			$html .= '</div>';
		}
		return $html;
	}



	function get_booking_type_point_value($booking_type_id = null){
		global $db;

		$sql = "SELECT * FROM booking_type WHERE booking_type_id = $booking_type_id";		
		if(!$result = $db->query($sql))
			return false;
		if($result->num_rows == 0)
			return false;

		$row = $result->fetch_assoc();
		return $row['point_value'];
	}


	function get_user_question_results_for_event($event_id = null, $user_id = null){
		global $db;
	
		$sql = "SELECT
					COUNT(question_id) AS num_correct,
					SUM(point_value) AS points_earned
				FROM (
					SELECT 
						ua.user_id,
						q.question_id,
						q.point_value,
						SUM(CASE WHEN correct = '0' THEN 0 ELSE 1 END) AS num_correct_answers,
						COUNT(ua.answer_id) AS num_user_answers,
						COUNT(uc.answer_id) AS num_user_correct 
					FROM answer a
					LEFT JOIN question q USING(question_id)
					LEFT JOIN user_answer ua ON a.answer_id = ua.answer_id AND ua.user_id = $user_id
					LEFT JOIN user_answer uc ON a.answer_id = uc.answer_id AND uc.user_id = $user_id AND a.correct = '1'
					WHERE event_id = $event_id
					GROUP BY q.question_id
				) c
				WHERE num_correct_answers = num_user_answers 
					AND num_correct_answers = num_user_correct";
		if(!$result = $db->query($sql))
			return false;
		if($result->num_rows == 0)
			return false;
		
		$question_details = $result->fetch_assoc();
		
		$sql = "SELECT 
					COUNT(*) as num_answered
				FROM (
					SELECT 
						*
					FROM user_answer 
					LEFT JOIN question USING(question_id)
					WHERE user_id = $user_id
						AND event_id =  $event_id
					GROUP BY question_id
				) na";
		if(!$result = $db->query($sql))
			return false;
		if($result->num_rows == 0)
			return false;
			
		$row = $result->fetch_assoc();
		
		$question_details['num_answered'] = $row['num_answered'];
		return $question_details;
	}

























function escape($var){
	global $db;
	
	if (get_magic_quotes_gpc()) {
		return $var;
	} else {
		if(is_array($var)){
			foreach($var as &$val){
				$val = escape($val);
			}
			return $var;
		}
		return $db->real_escape_string($var);
	}
}


function base64_url_decode($input) {
    return base64_decode(strtr($input, '-_', '+/'));
}

/**
Validate an email address.
Provide email address (raw input)
Returns true if the email address has the email 
address format and the domain exists.
*/
function valid_email($email)
{
   $isValid = true;
   $atIndex = strrpos($email, "@");
   if (is_bool($atIndex) && !$atIndex)
   {
      $isValid = false;
   }
   else
   {
      $domain = substr($email, $atIndex+1);
      $local = substr($email, 0, $atIndex);
      $localLen = strlen($local);
      $domainLen = strlen($domain);
      if ($localLen < 1 || $localLen > 64)
      {
         // local part length exceeded
         $isValid = false;
      }
      else if ($domainLen < 1 || $domainLen > 255)
      {
         // domain part length exceeded
         $isValid = false;
      }
      else if ($local[0] == '.' || $local[$localLen-1] == '.')
      {
         // local part starts or ends with '.'
         $isValid = false;
      }
      else if (preg_match('/\\.\\./', $local))
      {
         // local part has two consecutive dots
         $isValid = false;
      }
      else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain))
      {
         // character not valid in domain part
         $isValid = false;
      }
      else if (preg_match('/\\.\\./', $domain))
      {
         // domain part has two consecutive dots
         $isValid = false;
      }
      else if (!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\","",$local)))
      {
         // character not valid in local part unless 
         // local part is quoted
         if (!preg_match('/^"(\\\\"|[^"])+"$/',
             str_replace("\\\\","",$local)))
         {
            $isValid = false;
         }
      }
   }
   return $isValid;
}




?>