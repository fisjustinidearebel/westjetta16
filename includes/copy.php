<?php 
	
	/***   NAV COPY   ***/
	/********************/
	// main
	$copy['en']['home'] = 'Homepage';
	$copy['en']['prizes'] = 'Prizes';
	$copy['en']['how_to_enter'] = 'How to enter';
	$copy['en']['leaderboard'] = 'Leaderboard';
	// user
	$copy['en']['welcome'] = 'Hi %s, you have <a href="leaderboard.php" class="user_ballots">%d</a> ballots.';
	$copy['en']['logout'] = 'Logout';
	// languages
	$copy['en']['english'] = 'English';
	$copy['en']['french'] = 'Français';
	// cta buttons
	$copy['en']['enter_now'] = 'Enter now';
	$copy['en']['enter_bookings'] = 'Enter your bookings';
	$copy['en']['submit_bookings'] = 'Submit your bookings';
	$copy['en']['play_the_game'] = 'Play the trivia game';
	// footer
	$copy['en']['contest_rules'] = 'Contest rules';
	$copy['en']['book_flights'] = 'Book WestJet flights';
	$copy['en']['book_flights_url'] = 'http://www.westjet.com/en/agent/index.page';
	$copy['en']['book_vacations'] = 'Book WestJet Vacations';
	$copy['en']['book_vacations_url'] = 'http://www.westjet.com/en/agent/index.page';
	$copy['en']['privacy_policy'] = 'Privacy policy';
	$copy['en']['privacy_policy_url'] = 'http://www.westjet.com/en/agent/privacy-policy.page';
	$copy['en']['contact_us'] = 'Contact us';
	$copy['en']['contact_us_url'] = 'http://westjet.com/agentresource';
	$copy['en']['rules_file'] = 'pdf/contest_rules.pdf';

	/***   BODY COPY   ***/
	/*********************/
	// home
	$copy['en']['title'] = 'Hooray for TAs';
	$copy['en']['heading'] = 'Hooray for TAs';
	$copy['en']['description'] = "Happy TA appreciation month!";
	$copy['en']['description_2'] = "Happy TA appreciation month!"; 

	$copy['en']['bookings_heading'] = 'Booked any WestJet flights or WestJet Vacations packages?';
	$copy['en']['bookings_description'] = 'Submit your bookings made between May 2 and May 31, 2016 for your chance to win. Earn one ballot for every WestJet flight ticket number entered and earn four additional bonus ballots if your booking is a Plus fare and two additional bonus ballot if your booking includes advance seat selection. Five ballots will be awarded for every WestJet Vacations package confirmation number submitted. All bookings must be entered by June 2, 2016.';
	$copy['en']['book_flights_cta'] = 'Book more WestJet flights';
	$copy['en']['book_vacations_cta'] = 'Book more WestJet Vacations';

	$copy['en']['game_heading'] = 'Play the WestJet trivia game';
	$copy['en']['game_description'] = 'Put your thinking cap on and test your WestJet knowledge with our weekly trivia games. Each trivia question answered correctly will give you one additional bonus ballot into the contest draws.';
	$copy['en']['no_more_questions'] = 'No questions at this time.';
	$copy['en']['come_back_message'] = 'Come back on %s for the next trivia game!';

	$copy['en']['how_to_enter_heading'] = 'How to enter';
	$copy['en']['how_to_enter_description'] = 'Submit your bookings made between May 2 and May 31, 2016 and play the weekly WestJet trivia game for your chance to win. All bookings must be entered by June 2, 2016.';
	$copy['en']['submit_flights_heading'] = 'Submit your WestJet flights';
	$copy['en']['submit_flights_description'] = 'Every WestJet flight ticket number you submit will earn you one ballot into the contest draws. You can also earn additional bonus ballots if your booking is a Plus fare and or if your booking includes advance seat selection.';
	$copy['en']['submit_vacations_heading'] = 'Submit your WestJet Vacations bookings';
	$copy['en']['submit_vacations_description'] = 'Each WestJet Vacations package confirmation number you enter will earn you five ballots for the contest draws.';
	$copy['en']['play_game_heading'] = 'Play the WestJet trivia game';
	$copy['en']['play_game_description'] = 'Earn even more chances to win. Each correctly answered trivia question will earn you one bonus ballot into contest draws.';

	$copy['en']['prizes_heading'] = 'What you can win';
	$copy['en']['prizes_description'] = 'Each ballot earned will give you a chance to win one of 30 amazing prizes. Be sure to enter all your WestJet and WestJet Vacations bookings and to play the WestJet trivia game to increase your chances of winning.';
	$copy['en']['grand_prize_heading'] = 'Flight prizes';
	$copy['en']['grand_prize_description'] = "Grab your sunscreen! Win one of 10 WestJet Vacations packages for two.";
	$copy['en']['round_trip_flights_heading'] = 'Round-trip flights';
	$copy['en']['round_trip_flights_description'] = 'Pack your bags! Win one of 20 round-trip flights for two on WestJet.';
//*	$copy['en']['cash_giveaways_heading'] = 'WestJet dollar giveaways';
//	$copy['en']['cash_giveaways_description'] = 'You could win one of 30 WestJet dollar giveaway prizes to redeem for future travel. Start planning!';

//
// PRIZES
//

	$copy['en']['grandbahia_heading'] = 'Grand Bahia Principe Jamaica';
	$copy['en']['grandbahia_description'] = "An all-inclusive WestJet Vacations package for two that includes round-trip airfare to Montego Bay, Jamaica and a seven night stay at the Grand Bahia Principe Jamaica.";
	$copy['fr']['grandbahia_heading'] = 'Grand Bahia Principe Jamaica';
	$copy['fr']['grandbahia_description'] = "Un forfait Vacances WestJet tout inclus pour deux personnes comprenant le vol aller-retour à Montego Bay, en Jamaïque, et sept nuitées au Grand Bahia Principe Jamaica.";

	$copy['en']['grandbahiasj_heading'] = 'Grand Bahia Principe San Juan';
	$copy['en']['grandbahiasj_description'] = "An all-inclusive WestJet Vacations package for two that includes round-trip airfare to Puerto Plata, Dominican Republic and a seven night stay at the Grand Bahia Principe San Juan.";
	$copy['fr']['grandbahiasj_heading'] = 'Grand Bahia Principe San Juan';
	$copy['fr']['grandbahiasj_description'] = "Un forfait Vacances WestJet tout inclus pour deux personnes comprenant le vol aller-retour à Puerto Plata, en République dominicaine, et sept nuitées au Grand Bahia Principe San Juan.";

	$copy['en']['buenaventura_heading'] = 'Buenaventura Grand Hotel & Spa';
	$copy['en']['buenaventura_description'] = "An all-inclusive WestJet Vacations package for two that includes round-trip airfare to Puerto Vallarta, Mexico and a three night stay at the Buenaventura Grand Hotel & Spa.";
	$copy['fr']['buenaventura_heading'] = 'Buenaventura Grand Hotel & Spa';
	$copy['fr']['buenaventura_description'] = "Un forfait Vacances WestJet tout inclus pour deux personnes comprenant le vol aller-retour à Puerto Vallarta, au Mexique, et trois nuitées au Buenaventura Grand Hotel & Spa.";

	$copy['en']['elcid_heading'] = 'El Cid Castilla Beach';
	$copy['en']['elcid_description'] = "An all-inclusive WestJet Vacations package for two that includes round-trip airfare to Mazatlan, Mexico and a three night stay at the El Cid Castilla Beach. ";
	$copy['fr']['elcid_heading'] = 'El Cid Castilla Beach';
	$copy['fr']['elcid_description'] = "Un forfait Vacances WestJet tout inclus pour deux personnes comprenant le vol aller-retour à Mazatlán, au Mexique, et trois nuitées au El Cid Castilla Beach. ";

	$copy['en']['honua_heading'] = 'Honua Kai Resort & Spa';
	$copy['en']['honua_description'] = "A WestJet Vacations package for two that includes round-trip airfare to Maui, Hawaii and a two night stay at the Honua Kai Resort & Spa.";
	$copy['fr']['honua_heading'] = 'Honua Kai Resort & Spa';
	$copy['fr']['honua_description'] = "Un forfait Vacances WestJet pour deux personnes comprenant le vol aller-retour à Maui, à Hawaii, et deux nuitées au Honua Kai Resort & Spa.";

	$copy['en']['majestic_heading'] = 'Majestic Mirage Punta Cana';
	$copy['en']['majestic_description'] = "An all-inclusive WestJet Vacations package for two that includes round-trip airfare to Punta Cana, Dominican Republic and a three night stay at the Majestic Mirage Punta Cana.";
	$copy['fr']['majestic_heading'] = 'Majestic Mirage Punta Cana';
	$copy['fr']['majestic_description'] = "Un forfait Vacances WestJet pour deux personnes comprenant le vol aller-retour à Punta Cana, en République dominicaine, et trois nuitées au Majestic Mirage Punta Cana.";

	$copy['en']['clubmarival_heading'] = 'Club Marival Resort & Suites';
	$copy['en']['clubmarival_description'] = "An all-inclusive WestJet Vacations package for two that includes round-trip airfare to Puerto Vallarta, Mexico and a three night stay at the Club Marival Resort & Suites.";
	$copy['fr']['clubmarival_heading'] = 'Club Marival Resort & Suites';
	$copy['fr']['clubmarival_description'] = "Un forfait Vacances WestJet tout inclus pour deux personnes comprenant le vol aller-retour à Puerto Vallarta, au Mexique, et trois nuitées au Club Marival Resort & Suites.";

	$copy['en']['grandpalladium_heading'] = 'Grand Palladium Punta Cana Resort & Spa';
	$copy['en']['grandpalladium_description'] = "An all-inclusive WestJet Vacations package for two that includes round-trip airfare to Punta Cana, Dominican Republic and a three night stay at the Grand Palladium Punta Cana Resort & Spa.";
	$copy['fr']['grandpalladium_heading'] = 'Grand Palladium Punta Cana Resort & Spa';
	$copy['fr']['grandpalladium_description'] = "Un forfait Vacances WestJet pour deux personnes comprenant le vol aller-retour à Punta Cana, en République dominicaine, et trois nuitées au Grand Palladium Punta Cana Resort & Spa.";

	$copy['en']['hyattziva_heading'] = 'Hyatt Ziva Cancun';
	$copy['en']['hyattziva_description'] = "An all-inclusive WestJet Vacations package for two that includes round-trip airfare to Cancun, Mexico and a three night stay at the Hyatt Ziva Cancun.";
	$copy['fr']['hyattziva_heading'] = 'Hyatt Ziva Cancun';
	$copy['fr']['hyattziva_description'] = "Un forfait Vacances WestJet tout inclus pour deux personnes comprenant le vol aller-retour à Cancun, au Mexique, et trois nuitées au Hyatt Ziva Cancun.";

	$copy['en']['sandos_heading'] = 'Sandos Playacar Beach Resort';
	$copy['en']['sandos_description'] = "An all-inclusive WestJet Vacations package for two that includes round-trip airfare to Cancun, Mexico and a three night stay at the Sandos Playacar Beach Resort in the Riviera Maya.";
	$copy['fr']['sandos_heading'] = 'Sandos Playacar Beach Resort';
	$copy['fr']['sandos_description'] = "Un forfait Vacances WestJet tout inclus pour deux personnes comprenant le vol aller-retour à Cancun, au Mexique, et trois nuitées au Sandos Playacar Beach Resort sur la Riviera Maya.";

	// PRIZE TITLES
	$copy['en']['loggedoutprize1_heading'] = 'WestJet Vacations packages';
	$copy['en']['loggedoutprize1_description'] = "Grab your sunscreen! Win one of 10 WestJet Vacations packages for two.";
	$copy['en']['loggedoutprize2_heading'] = 'Round-trip flights';
	$copy['en']['loggedoutprize2_description'] = "Pack your bags! Win one of 20 round-trip flights for two on WestJet.";

	$copy['fr']['loggedoutprize1_heading'] = 'Forfaits Vacances WestJet';
	$copy['fr']['loggedoutprize1_description'] = "Faites provision de crème solaire! Vous pourriez gagner l'un des 10 forfaits de Vacances WestJet pour deux personnes.";
	$copy['fr']['loggedoutprize2_heading'] = 'Vols aller-retour';
	$copy['fr']['loggedoutprize2_description'] = "Faites vos valises! Vous pourriez gagner l'un des 20 vols WestJet aller-retour pour deux personnes.";

	$copy['en']['mex_heading'] = 'WestJet Vacations packages';
	$copy['en']['mex_description'] = "One of ten round-trip flights for two to any scheduled destination in WestJet's network.";
	$copy['en']['plane_heading'] = 'Round-trip flights';
	$copy['en']['plane_description'] = 'One of ten round-trip flights for two to any scheduled WestJet destination in Canada or the U.S.';

	$copy['en']['hard_rock_heading'] = 'Hard Rock Riviera Maya';
	$copy['en']['hard_rock_description'] = 'All-inclusive WestJet Vacations package for two that includes round-trip airfare to Cancun, Mexico and a 7-night stay at the Hard Rock Riviera Maya.';
	$copy['en']['hilton_heading'] = 'Hilton Puerto Vallarta Resort';
	$copy['en']['hilton_description'] = 'All-inclusive WestJet Vacations package for two that includes round-trip airfare to Puerto Vallarta, Mexico and a 7-night stay at the Hilton Puerto Vallarta Resort.';
	$copy['en']['blanc_heading'] = 'Le Blanc Spa Resort';
	$copy['en']['blanc_description'] = 'All-inclusive WestJet Vacations package for two that includes round-trip airfare to Cancun, Mexico and a 7-night stay at Le Blanc Spa Resort.';
	$copy['en']['crane_heading'] = 'The Crane Resort';
	$copy['en']['crane_description'] = 'A WestJet Vacations package for two that includes round-trip airfare to Barbados and a 7-night stay at The Crane Resort.';
	$copy['en']['platinum_heading'] = 'Platinum Yucatan Princess All Suites & Spa Resort';
	$copy['en']['platinum_description'] = 'All-inclusive WestJet Vacations package for two that includes round-trip airfare to Cancun, Mexico and a 7-night stay at the Platinum Yucatan Princess All Suites & Spa Resort in Riviera Maya.';
	$copy['en']['melia_heading'] = 'Meliá Nassau Beach Resort';
	$copy['en']['melia_description'] = 'All-inclusive WestJet Vacations package for two that includes round-trip airfare to Nassau, Bahamas and a 4-night stay at the Meliá Nassau Beach Resort.';
	$copy['en']['flight_prize_1_description'] = 'One of four round-trip flights for two to any scheduled destination in WestJet’s network.';
	$copy['en']['flight_prize_2_description'] = 'One of ten round-trip flights for two to any scheduled WestJet destination in Canada or the U.S.';
	$copy['en']['flight_prize_3_description'] = 'One of ten round-trip flights for two to any scheduled WestJet destination in Canada.';
	$copy['en']['cash_prize_1_heading'] = 'One of twenty $100 WestJet dollar giveaways';
	$copy['en']['cash_prize_1_description'] = 'Expires one year from date of issue';
	$copy['en']['cash_prize_2_heading'] = 'One of ten $50 WestJet dollar giveaways';
	$copy['en']['cash_prize_2_description'] = 'Expires one year from date of issue';

	$copy['en']['leaderboard_heading'] = $copy['en']['leaderboard'];
	$copy['en']['leaderboard_description'] = 'Ballots give you more chances to win and enough ballots will give you a spot of our leaderboard too. The leaderboard features the top ten agents with the most number of ballots. Every ballot counts, so keep booking and playing the WestJet trivia game to get your name up on the board!';
	$copy['en']['position'] = 'Position';
	$copy['en']['username'] = 'Username';
	$copy['en']['total_ballots'] = 'Total ballots';
	$copy['en']['vacation_ballots'] = 'WestJet Vacations ballots';
	$copy['en']['flight_ballots'] = 'Flight ballots';
	$copy['en']['quizzes'] = 'Game ballots';

	$copy['en']['copyright'] = '&copy; 2016 WestJet, All Rights Reserved';
	$copy['en']['address'] = 'WestJet Airlines 22 Aerial Place NE Calgary, Alberta Canada T2E 3J1';


	$copy['en']['closed_subheader'] = 'This contest is now closed.';
	$copy['en']['closed_body'] = "Our Hooray for TAs contest is now closed. <br/><br/>Thanks to all those who participated! Be sure to come back on June 12, 2015 to find out if you’re a winner.";

	$copy['en']['winner_heading'] = "Winners";
	$copy['en']['winner_body'] = 'Congratulations to all of our winners and kudos to our <a href="javascript:open_popup(\'#leaderboard_popup\');">leaderboard finalists</a>. Thank you for participating.';
	$copy['en']['gp_header'] = "Grand Prizes";
	$copy['en']['rt_header'] = "Round-trip flights";
	$copy['en']['wj_header'] = "WestJet dollars giveaway";
	$copy['en']['name'] = "Name";
	$copy['en']['prize'] = "Prize";

	$copy['en']['404_header'] = "404";
	$copy['en']['404_copy_one'] = "Sorry, this page cannot be found.";
	$copy['en']['404_copy_two'] = "Please try one of the links above or below.";


	/***   FORM COPY   ***/
	/*********************/
	// register
	$copy['en']['register'] = 'Register';
	$copy['fr']['register_notice'] = '<strong>Remarque:</strong> Si vous avez participé au concours «Vive les agents» ou « Gagnez avec Plus »  ou « Vitrines pour WestJet »   de WestJet, vous serez automatiquement inscrit au présent concours. Il suffit d’entrer les données d’ouverture de session utilisées pour l’ancien concours (trouvées ci-dessous) pour vous lancer!';
	$copy['en']['first_name_label'] = 'Agent first name:';
	$copy['en']['last_name_label'] = 'Agent last name:';
	$copy['en']['username_label'] = 'Username:';
	$copy['en']['username_login_info'] = 'Enter the email or username you registered the contest with.';
	$copy['en']['username_register_info'] = 'Create a username that you can easily remember. <br>This username may appear on the leaderboard and be visible to others.';
	$copy['en']['phone_number_label'] = 'Agent phone number:';
	$copy['en']['agency_name_label'] = 'Agency name:';
	$copy['en']['iata_number_label'] = 'IATA number:';
	$copy['en']['email_label'] = 'Agent email:';
	$copy['en']['confirm_email_label'] = 'Confirm agent email:';
	$copy['en']['password_label'] = 'Password:';
	$copy['en']['confirm_password_label'] = 'Confirm password:';
	$copy['en']['rules_check_label'] = 'I have read & agree to the <a href="'.$copy['en']['rules_file'].'" target="_blank" class="rules_link">contest rules</a>';
	$copy['en']['comm_check_label'] = 'Please sign me up for WestJet’s TA JetMail email communications';
	$copy['en']['validation_label'] = 'Prove you’re a human...';
	$copy['en']['validation_text'] = 'Slide To The Right';
	$copy['en']['validation_thankyou'] = "Thank you for registering for the 20 years with WestJet contest. Good luck!";
	// login
	$copy['en']['login'] = 'Login';
	$copy['en']['username_email_label'] = 'Username or agent email address:';
	$copy['en']['not_registered'] = 'Not registered for the contest yet?';
	$copy['en']['register_now'] = 'Register now >';
	// forgot password	
	$copy['en']['forgot_password'] = 'Forgot Password?';
	$copy['en']['forgot_password_instructions'] = 'Type in the email address associated to your profile and we’ll send you an email to reset your password.';
	$copy['en']['submit'] = 'Submit';
	$copy['en']['register_button'] = 'Register';
	// reset password
	$copy['en']['reset_password'] = 'Reset your password';
	$copy['en']['new_password_label'] = 'New password';
	$copy['en']['confirm_new_password_label'] = 'Confirm new password';
	// bookings
	$copy['en']['booking_vacation_code_label'] = 'Enter your WestJet Vacations booking';
	$copy['en']['booking_vacation_code_label_alt'] = '(WestJet Vacations confirmation code)';
	$copy['en']['booking_code_label'] = 'Enter your WestJet flight booking';
	$copy['en']['booking_code_label_alt'] = '(WestJet ticket number)';
	$copy['en']['flight_code_name'] = 'WestJet ticket number';
	$copy['en']['vacation_code_name'] = 'WestJet Vacations package confirmation number';
	$copy['en']['booking_type_label_a'] = '<span>This flight is a:</span>';
	$copy['en']['booking_type_label_b'] = 'or';
	$copy['en']['booking_type_label_plus'] = '<b style="color:#203364">Plus fare</b>';
	$copy['en']['booking_type_label_advanced'] = '<b style="color:#203364">Includes advance seat selection</b>';
	// questions
	$copy['en']['correct'] = 'Correct!';
	$copy['en']['incorrect'] = "Sorry that's incorrect!";

	// register errors
	$copy['en']['first_name_required'] = 'First name is required.';
	$copy['en']['last_name_required'] = 'Last name is required.';
	$copy['en']['username_required'] = 'Username is required.';
	$copy['en']['username_unavailable'] = 'Username is unavailable.';
	$copy['en']['username_invalid_length'] = 'Username must be between 6 and 20 characters.';
	$copy['en']['phone_number_required'] = 'Phone number is required.';
	$copy['en']['phone_number_not_valid'] = 'Please enter a valid phone number.';
	$copy['en']['agency_name_required'] = 'Agency name is required.';
	$copy['en']['iata_required'] = 'Agency IATA is required.';
	$copy['en']['iata_not_valid'] = 'Please enter a valid agency IATA.';
	$copy['en']['email_required'] = 'Email address is required.';
	$copy['en']['email_not_valid'] = 'Please enter a valid email address.';
	$copy['en']['confirm_email_required'] = 'Please confirm the email address.';
	$copy['en']['confirm_email_not_match'] = 'The confirm email address does not match.';
	$copy['en']['password_required'] = 'Password is required.';
	$copy['en']['password_invalid_length'] = 'Password must be between 6 and 20 characters.';
	$copy['en']['confirm_password_required'] = 'Please confirm the password.';
	$copy['en']['confirm_password_not_match'] = 'The confirm password does not match.';
	$copy['en']['captcha_invalid'] = 'Please complete validation.';
	$copy['en']['captcha_valid'] = 'You are a human!';
	$copy['en']['rules_not_agree'] = 'You must agree to the contest rules.';
	$copy['en']['iata_already_registered'] = 'An account already exists with this IATA number.';
	$copy['en']['email_already_registered'] = 'An account already exists with this email.';
	$copy['en']['registration_note'] = '<p><strong>Note:</strong> Note: If you participated in past WestJet travel agent contests you will automatically be registered for this contest. Simply enter your login information from the previous contest below to get started!</p><br />';

	// login errors
	$copy['en']['username_email_required'] = 'Username or email address is required.';
	$copy['en']['username_password_not_valid'] = 'Username or Password is invalid.';
	// forgot password errors
	$copy['en']['email_has_no_account'] = 'Sorry, no account exists with that email.';
	// forgot password success
	$copy['en']['reset_password_email_sent'] = 'Thank you, you should receive an email shortly to reset your password.';
	// reset password errors
	$copy['en']['reset_token_invalid'] = 'Sorry, your reset token has expired, or may be invalid.<br><a href="javascript:void(0);" class="forgot_password_link">Request a new token</a>';
	// reset password success
	$copy['en']['reset_password_success'] = 'Your password has been reset.<br><a href="javascript:void(0);" class="login_link">Login here</a>.';
	// bookings errors
	$copy['en']['booking_code_required'] = 'Booking number is required.';
	$copy['en']['booking_code_invalid'] = 'Please enter a valid WestJet ticket number.';
	$copy['en']['booking_code_used'] = 'Sorry, booking code %s has already been submitted.';
	$copy['en']['vacation_code_invalid'] = 'Please enter a valid WestJet Vacations package confirmation number.';
	$copy['en']['booking_vacation_code_invalid'] = 'Please enter a valid WestJet Vacations package confirmation number.';
	
	// bookings success
	$copy['en']['booking_code_success'] = 'Thanks! Your %s %s has been added. You have earned 1 ballot.';
	$copy['en']['booking_plus_code_success'] = 'Thanks! Your %s %s has been added. You have earned one ballot for your flight bookings and four additional bonus ballots for booking Plus.';
	$copy['en']['booking_advanced_code_success'] = 'Thanks! Your WestJet ticket number %s has been added. You have earned one ballot for your flight bookings and two additional bonus ballot because your booking includes advance seat selection.';
	$copy['en']['vacation_code_success'] = 'Thanks! Your %s %s has been added. You have earned %d ballots for your WestJet Vacations bookings.';
	//$copy['en']['vacation_code_success'] = 'Thanks! Your WestJet Vacations confirmation ticket number %s has been added. You have earned %d ballots for your WestJet Vacations bookings.';
	// questions
	$copy['en']['congrats'] = 'Congrats';
	$copy['en']['questions_complete_message'] = 'Congrats! You have completed the trivia questions and have earned %d out of %d additional bonus ballots.';
	$copy['en']['complete_the_quiz'] = 'Complete the quiz!';	
	$copy['en']['questions_incomplete_message'] = 'You have %d unanswered questions for the WestJet trivia game. Answer them today to earn additional ballots to increase your chances of winning!';	
	// general
	$copy['en']['Error'] = 'Error';
	$copy['en']['submission_error'] = 'There was an error processing your submission. Please try again later!';
	

	/***   EMAIL COPY   ***/
	/**********************/
	$copy['en']['email_from'] = 'WestJet TA contest <do-not-reply@hoorayfortas.com>';
	$copy['en']['email_subject'] = 'Reset password';
	$copy['en']['email_heading'] = 'Hooray for TAs';
	$copy['en']['email_subheading'] = 'Forgot your password?';
	$copy['en']['email_copy_1'] = 'You recently requested to reset your password for the Hooray for TAs contest.';
	$copy['en']['email_copy_2'] = 'Click on the link below to create a new password.';
	$copy['en']['email_button'] = 'Reset password';
	$copy['en']['email_footer_1'] = 'You have received this email because you submitted a request for your username for your account with WestJet’s Hooray for TAs contest. Replies to this email will not be received. The privacy of your information is important to us. To learn more, visit our <a href="'.$copy['en']['privacy_policy_url'].'" style="color:inherit;text-decoration:underline;">privacy policy</a>';
	$copy['en']['email_footer_2'] = 'Quebec licensee. Ontario travel agents are covered by TICO. Mailing address: 6085 Midfield Road, Toronto, ON L5P 1A2. TICO registration number: 50018683.';
	$copy['en']['email_footer_3'] = $copy['en']['copyright'].'<br>'.$copy['en']['address'];

	$copy['en']['email_img_url'] = 'https://www.westjetagentcontests.com/images/logo.gif';
	$copy['fr']['email_img_url'] = 'https://www.westjetagentcontests.com/images/logo.gif';


	/***   NAV COPY   ***/
	/********************/
	// main
	$copy['fr']['home'] = 'Page d’accueil';
	$copy['fr']['prizes'] = 'Prix';
	$copy['fr']['how_to_enter'] = 'Comment participer';
	$copy['fr']['leaderboard'] = 'Tableau des meneurs';
	// user
	$copy['fr']['welcome'] = 'Salut, %s! Vous avez <a href="leaderboard.php" class="user_ballots">%d</a> bulletin(s) de participation.';
	$copy['fr']['logout'] = 'Déconnexion';
	// languages
	$copy['fr']['english'] = 'English';
	$copy['fr']['french'] = 'Français';
	// cta buttons
	$copy['fr']['enter_now'] = 'Participez dès maintenant';
	$copy['fr']['enter_bookings'] = 'Inscrivez vos réservations';
	$copy['fr']['play_the_game'] = 'Jouez au jeu-questionnaire';
	$copy['fr']['submit_bookings'] = 'Inscrivez vos réservations';
	
	// footer
	$copy['fr']['contest_rules'] = 'Règlement du concours';
	$copy['fr']['book_flights'] = 'Réserver un vol WestJet';
	$copy['fr']['book_flights_url'] = 'http://www.westjet.com/fr/agent/index.page';
	$copy['fr']['book_vacations'] = 'Réserver un forfait Vacances WestJet';
	$copy['fr']['book_vacations_url'] = 'http://www.westjet.com/fr/agent/index.page';
	$copy['fr']['privacy_policy'] = 'Politique de confidentialité';
	$copy['fr']['privacy_policy_url'] = 'http://www.westjet.com/fr/agent/privacy-policy.page';
	$copy['fr']['contact_us'] = 'Contactez-nous';
	$copy['fr']['contact_us_url'] = 'http://westjet.com/ressourcesagents';
	$copy['fr']['rules_file'] = 'pdf/contest_rules_fr.pdf';


	/***   BODY COPY   ***/
	/*********************/
	// home
	$copy['fr']['title'] = 'Vive les agents';
	$copy['fr']['heading'] = 'Vive les agents';
	$copy['fr']['description'] = 'Bon mois des agents de voyages!';
	$copy['fr']['description_2'] = "Bon mois des agents de voyages!"; 
	$copy['fr']['bookings_heading'] = 'Avez-vous réservé des vols WestJet ou des forfaits Vacances WestJet?';
	$copy['fr']['bookings_description'] = 'Inscrivez vos réservations effectuées entre le 2 mai et le 31 mai 2016 pour courir la chance de gagner. Vous obtiendrez un bulletin de participation pour chaque numéro de billet WestJet soumis. À ce bulletin s’ajoutent quatre bulletins pour une réservation au tarif Plus, et deux bulletins lorsque la réservation comprend une présélection de sièges. Cinq bulletins seront remis pour chaque numéro de confirmation de forfait Vacances WestJet soumis. Toutes les réservations doivent être soumises d’ici le 2 juin 2016.';
	$copy['fr']['bookings_cta_heading'] = 'Vous voulez d’autres chances de gagner?';
	$copy['fr']['book_flights_cta'] = 'Réservez plus des vols WestJet';
	$copy['fr']['book_vacations_cta'] = 'Réservez plus de forfaits Vacances WestJet';

	$copy['fr']['game_heading'] = 'Jouez au jeu-questionnaire de WestJet';
	$copy['fr']['game_description'] = 'Testez vos connaissances de WestJet avec notre jeu-questionnaire hebdomadaire. Pour chaque bonne réponse aux questions du jeu-questionnaire, vous obtiendrez un bulletin de participation additionnel au concours.';
	$copy['fr']['no_more_questions'] = 'Il n’y a aucun questions pour le moment.';
	$copy['fr']['come_back_message'] = 'Revisitez-nous le %s pour le prochain jeu-questionnaire!';

	$copy['fr']['how_to_enter_heading'] = 'Comment participer';
	$copy['fr']['how_to_enter_description'] = 'Soumettez vos réservations effectuées entre le 2 mai et le 31 mai 2016 et jouez au jeu-questionnaire hebdomadaire de WestJet pour courir la chance de gagner. Toutes les réservations doivent être soumises d’ici le 2 juin 2016.';
	$copy['fr']['submit_flights_heading'] = 'Inscrivez vos vols WestJet';
	$copy['fr']['submit_flights_description'] = "Vous obtiendrez un bulletin de participation au concours pour chaque numéro de billet de vol WestJet soumis. Vous pouvez également obtenir des bulletins de participation additionnels si vous réservez au tarif Plus et si vous choisissez des sièges à l’avance.";
	$copy['fr']['submit_vacations_heading'] = 'Inscrivez vos réservations de forfaits Vacances WestJet';
	$copy['fr']['submit_vacations_description'] = 'Vous obtiendrez cinq bulletins de participation au tirage du concours pour chaque numéro de confirmation de forfait Vacances WestJet soumis.';
	$copy['fr']['play_game_heading'] = 'Jouez au jeu-questionnaire de WestJet';
	$copy['fr']['play_game_description'] = 'Augmentez vos chances de gagner! Pour chaque bonne réponse que vous donnerez, vous recevrez un bulletin de participation supplémentaire au tirage.';

	$copy['fr']['prizes_heading'] = 'Ce que vous pouvez gagner';
	$copy['fr']['prizes_description'] = 'Chaque bulletin de participation obtenu vous donnera la chance de gagner l’un des 30 prix fantastiques. Assurez-vous de soumettre toutes vos réservations avec WestJet et Vacances WestJet et de jouer au jeu-questionnaire pour augmenter vos chances de gagner.';
	$copy['fr']['grand_prize_heading'] = 'Forfaits Vacances WestJet';
	$copy['fr']['grand_prize_description'] = 'Faites provision de crème solaire! Vous pourriez gagner l’un des 10 forfaits de Vacances WestJet pour deux personnes.';
	$copy['fr']['round_trip_flights_heading'] = 'Vols aller-retour';
	$copy['fr']['round_trip_flights_description'] = "Un de dix vols aller-retour pour deux personnes à la destination WestJet au Canada ou aux États-Unis de votre choix.";
	$copy['fr']['cash_giveaways_heading'] = 'Cadeaux en dollars WestJet';
	$copy['fr']['cash_giveaways_description'] = 'Vous pouvez gagner un des 30 prix en dollars WestJet échangeables pour des voyages futurs. Commencez à planifier!';

	$copy['fr']['mex_heading'] = 'Forfaits Vacances WestJet';
	$copy['fr']['mex_description'] = "Faites vos valises! Vous pourriez gagner l'un des 20 vols WestJet aller-retour pour deux personnes.";
	$copy['fr']['plane_heading'] = 'Forfaits Vacances WestJet';
	$copy['fr']['plane_description'] = 'Un de dix vols aller-retour pour deux personnes à la destination de votre choix au sein du réseau de WestJet.';

	$copy['fr']['hard_rock_heading'] = 'Hard Rock Riviera Maya';
	$copy['fr']['hard_rock_description'] = 'Forfait Vacances WestJet tout inclus pour deux personnes comprenant le vol aller-retour à Cancún, au Mexique et 7 nuitées au Hard Rock Riviera Maya.';
	$copy['fr']['hilton_heading'] = 'Hilton Puerto Vallarta Resort';
	$copy['fr']['hilton_description'] = 'Forfait Vacances WestJet tout inclus pour deux personnes comprenant le vol aller-retour à Puerto Vallarta, au Mexico et 7 nuitées au Hilton Puerto Vallarta Resort.';
	$copy['fr']['blanc_heading'] = 'Le Blanc Spa Resort';
	$copy['fr']['blanc_description'] = 'Forfait Vacances WestJet tout inclus pour deux personnes comprenant le vol aller-retour à Cancún, au Mexique et 7 nuitées au Le Blanc Spa Resort.';
	$copy['fr']['crane_heading'] = 'The Crane Resort';
	$copy['fr']['crane_description'] = 'Forfait Vacances WestJet pour deux personnes comprenant le vol aller-retour à la Barbade et 7 nuitées à The Crane Resort.';
	$copy['fr']['platinum_heading'] = 'Platinum Yucatan Princess All Suites & Spa Resort';
	$copy['fr']['platinum_description'] = 'Forfait Vacances WestJet tout inclus pour deux personnes comprenant le vol aller-retour à Cancún, au Mexique et 7 nuitées au Platinum Yucatan Princess All Suites & Spa Resort à la Riviera Maya.';
	$copy['fr']['melia_heading'] = 'Meliá Nassau Beach Resort';
	$copy['fr']['melia_description'] = 'Forfait Vacances WestJet tout inclus pour deux personnes comprenant le vol aller-retour à Nassau, dans les Bahamas et 4 nuitées au Meliá Nassau Beach Resort.';
	$copy['fr']['flight_prize_1_description'] = 'Un des quatre vols aller-retour pour deux personnes à la destination régulière de votre choix au sein du réseau de WestJet.';
	$copy['fr']['flight_prize_2_description'] = 'Un des dix vols aller-retour pour deux personnes à la destination WestJet au Canada ou aux États-Unis de votre choix.';
	$copy['fr']['flight_prize_3_description'] = 'Un des dix vols aller-retour pour deux personnes à la destination WestJet au Canada de votre choix.';
	$copy['fr']['cash_prize_1_heading'] = 'Un des vingt prix de 100 dollars WestJet';
	$copy['fr']['cash_prize_2_heading'] = 'Un des dix prix de 50 dollars WestJet';

	$copy['fr']['leaderboard_heading'] = $copy['fr']['leaderboard'];
	$copy['fr']['leaderboard_description'] = 'Les bulletins de participation vous donnent plus de chances de gagner; si vous avez suffisamment de bulletins, vous vous retrouverez sur notre tableau des meneurs. Le tableau des meneurs comprend les dix agents détenant le plus grand nombre de bulletins. Chaque bulletin de participation compte, alors continuez à réserver des vols et des forfaits Vacances et à jouer aux jeux-questionnaires de WestJet pour que votre nom apparaisse sur le tableau des meneurs!';
	$copy['fr']['position'] = 'Position';
	$copy['fr']['username'] = 'Nom d’utilisateur';
	$copy['fr']['total_ballots'] = 'Bulletins de participation totaux';
	$copy['fr']['vacation_ballots'] = 'Bulletin(s) de participation Vacances WestJet';
	$copy['fr']['flight_ballots'] = 'Bulletin(s) de participation de vols';
	$copy['fr']['quizzes'] = 'Bulletin(s) de participation de jeux-questionnaires';

	$copy['fr']['copyright'] = '&copy; 2016 WestJet, Tous droits réservés.';
	$copy['fr']['address'] = 'WestJet 22 Aerial Place NE Calgary, Alberta Canada T2E 3J1';


	$copy['fr']['closed_subheader'] = 'Ce concours est maintenant terminé.';
	$copy['fr']['closed_body'] = "Le concours « Vive les agents » est maintenant terminé.<br/><br/>Merci à tous les participants! Revisitez-nous le 12 juin 2015 pour voir si vous avez gagné.";

	$copy['fr']['winner_heading'] = "Gagnants";
	$copy['fr']['winner_body'] = 'Félicitations à tous nos gagnants ainsi qu’aux <a href="javascript:open_popup(\'#leaderboard_popup\');">finalistes du tableau des meneurs</a>. Merci d’avoir participé.';
	$copy['fr']['gp_header'] = "Grands prix";
	$copy['fr']['rt_header'] = "Vols aller-retour";
	$copy['fr']['wj_header'] = "Cadeaux en dollars WestJet";
	$copy['fr']['name'] = "Nom";
	$copy['fr']['prize'] = "Prix";

	$copy['fr']['404_header'] = "404";
	$copy['fr']['404_copy_one'] = "Désolé, cette page est introuvable.";
	$copy['fr']['404_copy_two'] = "Veuillez essayer un des liens ci-dessus ou ci-dessous.";



	/***   FORM COPY   ***/
	/*********************/
	// register

	$copy['fr']['register'] = 'Inscription';
	$copy['fr']['register_notice'] = '<strong>Remarque :</strong> Si vous avez participé au concours «Vive les agents» ou « Gagnez avec Plus »  ou « Vitrines pour WestJet »   de WestJet, vous serez automatiquement inscrit au présent concours. Il suffit d’entrer les données d’ouverture de session utilisées pour l’ancien concours (trouvées ci-dessous) pour vous lancer!';
	$copy['fr']['first_name_label'] = 'Prénom de l’agent:';
	$copy['fr']['last_name_label'] = 'Nom de famille de l’agent:';
	$copy['fr']['username_label'] = 'Nom d’utilisateur:';
	$copy['fr']['username_login_info'] = 'Veuillez saisir l’adresse de courriel ou le nom d’utilisateur dont vous vous êtes servi pour vous inscrire au concours.';
	$copy['fr']['username_register_info'] = 'Créez un nom d’utilisateur facile à retenir. Il se peut que ce nom d’utilisateur apparaisse sur le tableau des meneurs et soit visible à d’autres personnes.';
	$copy['fr']['phone_number_label'] = 'Numéro de téléphone de l’agent:';
	$copy['fr']['agency_name_label'] = 'Nom de l’agence:';
	$copy['fr']['iata_number_label'] = 'Numéro IATA de l’agent:';
	$copy['fr']['email_label'] = 'Adresse courriel de l’agent:';
	$copy['fr']['confirm_email_label'] = 'Confirmer adresse courriel de l’agent:';
	$copy['fr']['password_label'] = 'Mot de passe:';
	$copy['fr']['confirm_password_label'] = 'Confirmer mot de passe:';
	$copy['fr']['rules_check_label'] = 'J’ai lu et accepté le <a href="'.$copy['fr']['rules_file'].'" target="_blank" class="rules_link">règlement du concours</a>';
	$copy['fr']['comm_check_label'] = 'Veuillez m’inscrire aux communications par courriel JetMail pour agents de WestJet';
	$copy['fr']['validation_label'] = 'Prouvez que vous êtes humain...';
	$copy['fr']['validation_text'] = 'Glissez vers la droite';
	$copy['fr']['validation_thankyou'] = 'Merci de vous être inscrit au concours «Récoltez des prix» de WestJet. Bonne chance!';

	// login
	$copy['fr']['login'] = 'Connexion';
	$copy['fr']['username_email_label'] = 'Nom d’utilisateur ou adresse de courriel:';
	$copy['fr']['not_registered'] = 'Pas encore inscrit au concours?';
	$copy['fr']['register_now'] = 'Inscrivez-vous maintenant >';
	// forgot password	
	$copy['fr']['forgot_password'] = 'Mot de passe oublié?';
	$copy['fr']['forgot_password_instructions'] = 'Saisissez l’adresse courriel associé à votre profile et nous enverrons un courriel pour réinitialiser votre mot de passe';
	$copy['fr']['submit'] = 'Soumettre';
	// reset password
	$copy['fr']['reset_password'] = 'Réinitialisez votre mot de passe';
	$copy['fr']['new_password_label'] = 'Nouveau mot de passe';
	$copy['fr']['confirm_new_password_label'] = 'Confirmez le nouveau mot de passe';
	// vacance
	$copy['fr']['vacation_code_label'] = 'Entrez vos réservations<br>de Vacances WestJet';
	$copy['fr']['flight_code_name'] = 'numéro de billet WestJet';
	$copy['fr']['vacation_code_name'] = 'numéro de confirmation de forfait Vacances WestJet';
	// bookings
	$copy['fr']['booking_vacation_code_label'] = 'Entrez vos réservations<br>de Vacances WestJet';
	$copy['fr']['booking_vacation_code_label_alt'] = '(Code de confirmation de Vacances WestJet)';
	$copy['fr']['booking_code_label'] = 'Entrez votre réservation<br>de vol West Jet au tarif';
	$copy['fr']['booking_code_label_alt'] = '(Numéro de billet WestJet)';
	$copy['fr']['flight_code_name'] = 'numéro de billet WestJet';
	$copy['fr']['vacation_code_name'] = 'numéro de confirmation de forfait Vacances WestJet';
	$copy['fr']['booking_type_label_a'] = '<span>Ce vol est :</span>';
	$copy['fr']['booking_type_label_b'] = 'ou';
	$copy['fr']['booking_type_label_plus'] = '<b style="color:#203364">au tarif Plus </b>';
	$copy['fr']['booking_type_label_advanced'] = '<b style="color:#203364">comprend une sélection de sièges</b>';
	// bookings_plus
	$copy['fr']['booking_extra_code_label'] = 'Comprend une sélection de sièges';
	// questions
	$copy['fr']['correct'] = 'Exact!';
	$copy['fr']['incorrect'] = 'Dommage &#45; mauvaise r&eacute;ponse!';

	// register errors
	$copy['fr']['first_name_required'] = 'Prénom requis';
	$copy['fr']['last_name_required'] = 'Nom de famille requis';
	$copy['fr']['username_required'] = 'Nom d’utilisateur requis';
	$copy['fr']['username_unavailable'] = 'Nom d’utilisateur indisponible';
	$copy['fr']['username_invalid_length'] = 'Nom d’utilisateur doit être entre 6-20 charactères';
	$copy['fr']['phone_number_required'] = 'Numéro de téléphone requis';
	$copy['fr']['phone_number_not_valid'] = 'Veuillez entrer un numéro de téléphone valide';
	$copy['fr']['agency_name_required'] = 'Nom de l’agence requis';
	$copy['fr']['iata_required'] = 'Numéro IATA de l’agent requis';
	$copy['fr']['iata_not_valid'] = 'Veuillez entrer l’IATA valide de l’agence';
	$copy['fr']['email_required'] = 'Adresse courriel requise';
	$copy['fr']['email_not_valid'] = 'Veuillez entrer une adresse courriel valide';
	$copy['fr']['confirm_email_required'] = 'Veuillez confirmer l’adresse courriel';
	$copy['fr']['confirm_email_not_match'] = 'La confirmation d’adresse courriel ne correspond pas';
	$copy['fr']['password_required'] = "Nom d'utilisateur ou mot de passe est incorrect";
	$copy['fr']['password_invalid_length'] = 'Mot de passe doit être entre 6-20 charactères';
	$copy['fr']['confirm_password_required'] = 'Confirmez le mot de passe';
	$copy['fr']['confirm_password_not_match'] = 'La confirmation de mot de passe ne correspond pas';
	$copy['fr']['captcha_invalid'] = 'Complétez la validation SVP';
	$copy['fr']['captcha_valid'] = 'Vous êtes humain!';
	$copy['fr']['rules_not_agree'] = 'Vous devez accepter le règlement du concours';
	$copy['fr']['iata_already_registered'] = 'Un compte existe déjà avec ce numéro IATA';
	$copy['fr']['email_already_registered'] = 'Un compte existe déjà avec cette adresse de courriel';
	$copy['fr']['registration_note'] = '<p><strong>Remarque:</strong> Si vous avez participé aux précédents concours de WestJet pour les agents de voyages, vous serez automatiquement inscrit à ce concours. Il suffit d’entrer les données d’ouverture de session utilisées pour l’ancien concours (voir ci-dessous) pour vous lancer!</p><br />';

	// login errors
	$copy['fr']['username_email_required'] = 'Nom d’utilisateur ou adresse courriel requise';
	$copy['fr']['username_password_not_valid'] = "Nom d'utilisateur ou mot de passe est incorrect";
	// forgot password errors
	$copy['fr']['email_has_no_account'] = 'Désolé, aucun comp existe avec cette adresse de courriel';
	$copy['fr']['register_button'] = 'Soumettre';
	// forgot password success
	$copy['fr']['reset_password_email_sent'] = 'Merci, vous devriez recevoir un courriel pour remettre votre mot de passe.';
	// reset password errors
	$copy['fr']['reset_token_invalid'] = 'Désolé, votre code de réinitialisation est expiré ou invalide.<br><a href="javascript:void(0);" class="forgot_password_link">Demandez un nouveau code</a>';
	// reset password success
	$copy['fr']['reset_password_success'] = 'Votre mot de passe a été réinitialisé.<br><a href="javascript:void(0);" class="login_link">Connectez-ici</a>.';
	// bookings errors
	$copy['fr']['booking_code_required'] = 'Numéro de réservation requis';
	$copy['fr']['booking_code_invalid'] = 'Veuillez entrer un numéro de billet WestJet valide.';
	$copy['fr']['booking_code_used'] = 'Nous sommes désolés, mais ce code de réservation %s a déjà été soumis.';
	$copy['fr']['booking_vacation_code_invalid'] = 'Veuillez entrer un numéro de confirmation valide de forfait Vacances WestJet.';

	// bookings success
	$copy['fr']['booking_code_success'] = 'Merci! Votre %s %s a été ajouté. Vous avez obtenu %d bulletin de participation';
	$copy['fr']['vacation_code_success'] = 'Merci! Votre %s %s a été ajouté. Vous avez obtenu cinq bulletins de participation pour votre réservation de Vacances WestJet.';
	$copy['fr']['booking_plus_code_success'] = 'Merci! Votre %s %s a été ajouté. Vous avez obtenu un bulletin de participation pour votre réservation de vol et quatres bulletins additionnels pour avoir réservé au tarif Plus.';
	$copy['fr']['booking_advanced_code_success'] = "Merci! Votre numéro de billet WestJet %s %s a été ajouté. Vous avez obtenu un bulletin de participation pour votre réservation de vol et deux bulletin additionnel pour avoir effectué votre choix de siège à l'avance.";
	// questions
	$copy['fr']['congrats'] = 'F&eacute;licitations';
	$copy['fr']['questions_complete_message'] = 'F&eacute;licitations! Vous avez r&eacute;pondu aux questions. Vous recevrez %d bulletin(s) de participation additionnel(s) sur %d.';
	$copy['fr']['complete_the_quiz'] = 'Complétez le jeu-concours!';	
	$copy['fr']['questions_incomplete_message'] = 'Vous avez %d questions sans réponse au jeu-questionnaire de WestJet. Répondez à ces questions aujourd’hui et obtenez des bulletins de participation additionnels pour augmenter vos chances de gagner!';	
	// general
	$copy['fr']['Error'] = 'Erreur';
	$copy['fr']['submission_error'] = 'Une erreur s’est produite lors du traitement de votre demande. Veuillez réessayer plus tard.';


	/***   EMAIL COPY   ***/
	/**********************/
	$copy['fr']['email_from'] = 'Concours agent de vol WestJet <ne-repond-pas@vivelesagents.com>';
	$copy['fr']['email_subject'] = 'Reinitialiser mot de passe';
	$copy['fr']['email_heading'] = 'Vive les agents';
	$copy['fr']['email_subheading'] = 'Oubliez votre mot de passe?';
	$copy['fr']['email_copy_1'] = 'Vous nous avez demandé récemment de réinitialiser votre mot de passe dans le cadre du concours « Vive les agents ».';
	$copy['fr']['email_copy_2'] = 'Cliquez sur le lien ci-dessous pour créer un nouveau mot de passe.';
	$copy['fr']['email_button'] = 'Réinitialiser mot de passe';
	$copy['fr']['email_footer_1'] = 'Vous avez reçu ce courriel parce que vous avez demandé votre nom d’utilisateur dans le cadre du concours « Vive les agents ». WestJet ne recevra pas les réponses à ce courriel. La protection de vos renseignements personnels est importante pour nous. Pour plus de renseignements, veuillez consulter notre <a href="'.$copy['fr']['privacy_policy_url'].'" style="color:inherit;text-decoration:underline;">politique de confidentialité</a>';
	$copy['fr']['email_footer_2'] = 'Titulaire d’un permis du Québec. Les agents de voyages de l’Ontario sont inscrits auprès de TICO. Adresse postale : 6085 Midfield Road, Toronto ON L5P 1A2. Numéro d’enregistrement TICO : 50018683.';
	$copy['fr']['email_footer_3'] = $copy['fr']['copyright'].'<br>'.$copy['fr']['address'];
