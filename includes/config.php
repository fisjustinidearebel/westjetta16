<?php
	header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
	
	date_default_timezone_set('America/Denver');
	if(!isset($_SESSION)) session_start();
	$protocol = ( !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' )? 'https://' : 'http://';
	
	// LOCAL
	if ( $_SERVER['HTTP_HOST'] == 'wjta16.dev' || $_SERVER['HTTP_HOST'] == 'wjta16.dev') {
		define('BASE_URL', $protocol.$_SERVER['HTTP_HOST']);
		define('ENGLISH_URL', $protocol.'wjta16.dev?lang=en');
		define('FRENCH_URL', $protocol.'wjta16.dev?lang=fr');
		
		define('DB_HOST', '127.0.0.1');
		define('DB_USER', 'root');
		define('DB_PASS', 'root');
		define('DB_NAME', 'wjta162');
		
		if(isset($_GET['lang'])){
			define('LANG', (isset($_GET['lang']) && $_GET['lang'] == 'fr')? 'fr' : 'en');
			$_SESSION['LANG'] = LANG;
		}
		else if(isset($_SESSION['LANG'])){
			define('LANG', $_SESSION['LANG']);
		}
		else{
			define('LANG', (isset($_GET['lang']) && $_GET['lang'] == 'fr')? 'fr' : 'en');
			$_SESSION['LANG'] = LANG;
		}	
		
	}
	
	// STAGING
	if ( $_SERVER['HTTP_HOST'] == 'staging.idearebel.com') {
		define('BASE_URL', $protocol.$_SERVER['HTTP_HOST'].'/westjet_ta16/');
		define('ENGLISH_URL', $protocol.'staging.idearebel.com/westjet_ta16?lang=en');
		define('FRENCH_URL', $protocol.'staging.idearebel.com/westjet_ta16?lang=fr');
		
		if(!defined('DB_HOST')) define('DB_HOST', 'localhost');
		if(!defined('DB_USER')) define('DB_USER', 'ir_staging');
		if(!defined('DB_PASS')) define('DB_PASS', 'ir_staging');
		if(!defined('DB_NAME')) define('DB_NAME', 'wjta16');
	
		if(isset($_GET['lang'])){
			define('LANG', (isset($_GET['lang']) && $_GET['lang'] == 'fr')? 'fr' : 'en');
			$_SESSION['LANG'] = LANG;
		}
		else if(isset($_SESSION['LANG'])){
			define('LANG', $_SESSION['LANG']);
		}
		else{
			define('LANG', (isset($_GET['lang']) && $_GET['lang'] == 'fr')? 'fr' : 'en');
			$_SESSION['LANG'] = LANG;
		}	
	}
	
	// PRODUCTION
	if(!defined('BASE_URL')) define('BASE_URL', $protocol.$_SERVER['HTTP_HOST']);
	if(!defined('ENGLISH_URL')) define('ENGLISH_URL', $protocol.'www.westjetagentcontests.com');
	if(!defined('FRENCH_URL')) define('FRENCH_URL', $protocol.'www.concoursdewestjetpouragents.com');
	
	if(!defined('DB_HOST')) define('DB_HOST', '127.0.0.1');
	if(!defined('DB_USER')) define('DB_USER', 'wj_contest_user');
	if(!defined('DB_PASS')) define('DB_PASS', 'BfCJJeetk');
	if(!defined('DB_NAME')) define('DB_NAME', 'wjta16');

	if(!defined('LANG')) define('LANG', ($_SERVER['HTTP_HOST'] == 'concoursdewestjetpouragents.com' || $_SERVER['HTTP_HOST'] == 'www.concoursdewestjetpouragents.com')? 'fr' : 'en');

	
	// minimum number of answers a question must have
	if(!defined('MIN_ANSWERS')) define('MIN_ANSWERS', 2);
	

	$db = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	if ($db->connect_errno) {
    	die('Could not connect to MySQL: ('.$db->connect_errno.') '.$db->connect_error);
	}

	
	//Closed state check
	if (!defined('WINNERS') && time() > strtotime("2017-11-12 23:59:59")){
	    define('WINNERS', true);
        define('CLOSED', false);
        
    } else if (!defined('CLOSED') && time() > strtotime("2017-11-03 23:59:59")){
        define('WINNERS', false);
		define('CLOSED', true);
     
	} else {
	    define('WINNERS', false);
		define('CLOSED', false);
	}
	
	
	/*
	$analitics['en'] = "<script>
						  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
						  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
						  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
						  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
						  ga('create', 'UA-62258932-1', 'auto');
						  ga('send', 'pageview');
						</script>";
	$analitics['fr'] = "<script>
						  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
						  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
						  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
						  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
						  ga('create', 'UA-62258932-2', 'auto');
						  ga('send', 'pageview');
						</script>";
	*/
	$analitics['en'] = "<script>
							(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
							(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
							m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
							})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

							ga('create', 'UA-62258932-1', 'auto');
							ga('send', 'pageview');

						</script>";
	$analitics['fr'] = "<script>
							(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
							(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
							m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
							})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

							ga('create', 'UA-62258932-2', 'auto');
							ga('send', 'pageview');

						</script>";

	
	require_once 'copy.php';

	require_once 'functions.php';


?>
