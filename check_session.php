<?php
   if (WINNERS) {
       header('Location: winners.php');
       exit;
   } else if (CLOSED){
		header('Location: closed.php');
		exit;
	}

	if(!isset($_SESSION)) session_start();
	
	// check for user session
	if (isset($_SESSION['user'])){
		// check if last activity was over 1 hour ago
		if((time() - $_SESSION['last_activity'] > 3600)){
		    session_unset();
		    session_destroy();
		    if(strpos($_SERVER['PHP_SELF'], 'index.php') === false){
		    	header('Location: index.php');
		    	exit;
		    }
		// user session still active
		}else{
			$_SESSION['user'] = get_user_by_id($_SESSION['user']['user_id']);
			$_SESSION['last_activity'] = time();
			if(strpos($_SERVER['PHP_SELF'], 'index.php') !== false){
				
				if(LANG=='en'): 
					header('Location: home.php');
				else: 
					header('Location: home.php');
				endif; 

				exit;
			}
		}
	//no user
	}else{
		session_unset();
		session_destroy();
		if(strpos($_SERVER['PHP_SELF'], 'index.php') === false){
			header('Location: index.php');
			exit;
		}
	}
?>