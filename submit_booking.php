<?php
	require_once 'includes/config.php';

	$valid = true;
	$errors = array();

	$user_id = $_SESSION['user']['user_id'];
	$booking_code = (isset($_POST['booking_code']))? escape($_POST['booking_code']) : null;
	
	// validate
	if(empty($booking_code)){
		$errors['booking_code'] = $copy[LANG]['booking_code_required'];
		$return = array('status'=>'error', 'errors'=>$errors);
		echo json_encode($return);
		exit;
	}
	// validate flight booking code
	elseif(preg_match('/^\d+$/',$booking_code) && strlen($booking_code) == 13){
		$booking_type_id = 1;
		if(isset($_POST['booking_cb_plus']))//Flight Plus checked
			$booking_type_id =3;
		elseif(isset($_POST['booking_cb_advanced']))//Flight Advanced checked
			$booking_type_id =4;

		$booking_type = $copy[LANG]['flight_code_name'];
	}
	else{
		$errors['booking_code'] = $copy[LANG]['booking_code_invalid'];
		$return = array('status'=>'error', 'errors'=>$errors);
		echo json_encode($return);
		exit;
	}
	
	

	//check if booking code has already been used		
	$sql = "SELECT * FROM user_booking WHERE booking_code = '$booking_code'";		
	$result = $db->query($sql);

	if($result->num_rows > 0){
		$errors = sprintf($copy[LANG]['booking_code_used'], $booking_code);
		$return = array('status'=>'error', 'errors'=>$errors);
		echo json_encode($return);
		exit;
	}

	// insert booking
	$sql = "INSERT INTO user_booking (user_id, booking_type_id, booking_code) VALUES ($user_id, $booking_type_id, '$booking_code')";		
	if(!$result = $db->query($sql)){
		$return = array('status'=>'error', 'errors'=>$copy[LANG]['submission_error']);
		echo json_encode($return);
		exit;
	}

	// get point value for given booking type
	$point_value = get_booking_type_point_value($booking_type_id);
	
	// update user points
 	update_user_points($user_id, $point_value);
	if(isset($_POST['booking_cb_plus']))
		$message = sprintf($copy[LANG]['booking_plus_code_success'], $booking_type, $booking_code, ($point_value? $point_value : ''));
	elseif(isset($_POST['booking_cb_advanced']))
		$message = sprintf($copy[LANG]['booking_advanced_code_success'], $booking_type, $booking_code, ($point_value? $point_value : ''));
	else
		$message = sprintf($copy[LANG]['booking_code_success'], $booking_type, $booking_code, ($point_value? $point_value : ''));

	$return = array('status'=>'ok', 'message'=>$message);
	echo json_encode($return);
	exit;

?>