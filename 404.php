<?php
	require_once('includes/config.php');
	require_once('check_session.php');
		
	$reset_token = (isset($_GET['token']))? $_GET['token'] : null;

	include('header-closed.php');
?>
	<div id="main">
		
		<div id="how_to_enter" class="section">
			<div class="container">
				<div class="col_wrapper">
					<div class="col col_1"></div>
					<div class="col col_10">
						<h2 class="section_heading" style="text-align: left;"><?php echo $copy[LANG]['404_header']; ?></h2>
						<p class="section_description" style="text-align: left;"><?php echo $copy[LANG]['404_copy_one']; ?><br/><?php echo $copy[LANG]['404_copy_two']; ?></p>
					</div>
					<div class="col col_1"></div>
				</div>
			</div>
		</div>
	</div>

<?php
	include('footer.php');
?>