<?php
	require_once('includes/config.php');
	require_once('check_session.php');
		
	include('header.php');
?>
	<div id="main">
		<div id="how_to_enter" class="section">
			<div class="container">
				<div class="col_wrapper">
					<div class="col col_1"></div>
					<div class="col col_10">
						<h1 class="section_heading"><?php echo $copy[LANG]['leaderboard_heading']; ?></h1>
						<p class="section_description"><?php echo $copy[LANG]['leaderboard_description']; ?></p>
					</div>
					<div class="col col_1"></div>
				</div>
			</div>
		</div>
		<div id="leaderboard" class="section">
			<div class="container">
				<?php $leaderboard = get_full_leaderboard(); ?>
				<?php $user_row = get_user_leaderboard_row($_SESSION['user']['user_id']); ?>
				<table>
					<thead>
						<tr>
							<th><?php echo $copy[LANG]['position']; ?></th>
							<th><?php echo $copy[LANG]['username']; ?></th>
							<th><?php echo $copy[LANG]['total_ballots']; ?></th>
						</tr>
						<tr>
							<td><?php echo $user_row['rank']; ?></td>
							<td><?php echo $user_row['username']; ?></td>
							<td>
								<div class="ballots_wrapper">
									<span class="ballots"><?php echo $user_row['total_points']; ?></span>
									<div class="ballot_breakdown">
										<?php echo $copy[LANG]['vacation_ballots'].' : <strong>'.$user_row['v_booking_points'].'</strong><br>'; ?>
										<?php echo $copy[LANG]['flight_ballots'].' : <strong>'.$user_row['f_booking_points'].'</strong><br>'; ?>
										<?php echo $copy[LANG]['quizzes'].'  : <strong>'.$user_row['question_points'].'</strong>'; ?>
									</div>
								</div>
							</td>
						</tr>
					</thead>
					<tbody>
						<?php foreach($leaderboard as $user_id=>$user): ?>
							<tr>
								<td><?php echo $user['rank']; ?></td>
								<td><?php echo $user['username']; ?></td>
								<td>
									<div class="ballots_wrapper">
										<span class="ballots"><?php echo $user['total_points']; ?></span>
										<div class="ballot_breakdown">
											<?php echo $copy[LANG]['vacation_ballots'].' : <strong>'.$user['v_booking_points'].'</strong><br>'; ?>
											<?php echo $copy[LANG]['flight_ballots'].'  : <strong>'.$user['f_booking_points'].'</strong><br>'; ?>
											<?php echo $copy[LANG]['quizzes'].' : <strong>'.$user['question_points'].'</strong>'; ?>
										</div>
									</div>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

<?php
	include('footer.php');
?>