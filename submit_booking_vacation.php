<?php
	require_once 'includes/config.php';

	$valid = true;
	$errors = array();

	$user_id = $_SESSION['user']['user_id'];
	
	$booking_code_vacation = (isset($_POST['booking_code_vacation']))? escape($_POST['booking_code_vacation']) : null;
	
	// validate
	if(empty($booking_code_vacation)){
		$errors['booking_code_vacation'] = $copy[LANG]['booking_code_required'];
		$return = array('status'=>'error', 'errors'=>$errors);
		echo json_encode($return);
		exit;
	}
	// validate vacation booking code
	elseif(preg_match('/^\d+$/',$booking_code_vacation) && strlen($booking_code_vacation) == 7){
		$booking_type_id = 2;
		$booking_type = $copy[LANG]['vacation_code_name'];
	}
	else{
		$errors['booking_code_vacation'] = $copy[LANG]['booking_vacation_code_invalid'];
		$return = array('status'=>'error', 'errors'=>$errors);
		echo json_encode($return);
		exit;
	}
	
	
	//check if booking code has already been used		
	$sql = "SELECT * FROM user_booking WHERE booking_code = '$booking_code_vacation'";		
	$result = $db->query($sql);

	if($result->num_rows > 0){
		$errors = sprintf($copy[LANG]['booking_code_used'], $booking_code_vacation);
		$return = array('status'=>'error', 'errors'=>$errors);
		echo json_encode($return);
		exit;
	}

	// insert booking
	$sql = "INSERT INTO user_booking (user_id, booking_type_id, booking_code) VALUES ($user_id, $booking_type_id, '$booking_code_vacation')";		
	if(!$result = $db->query($sql)){
		$return = array('status'=>'error', 'errors'=>$copy[LANG]['submission_error']);
		echo json_encode($return);
		exit;
	}

	// get point value for given booking type
	$point_value = get_booking_type_point_value($booking_type_id);
	
	// update user points
 	update_user_points($user_id, $point_value);

	$message = sprintf($copy[LANG]['vacation_code_success'], $booking_type, $booking_code_vacation, ($point_value? $point_value : ''));
	$return = array('status'=>'ok', 'message'=>$message);
	echo json_encode($return);
	exit;

function debug_to_console( $data ) {

    if ( is_array( $data ) )
        $output = "<script>console.log( 'Debug Objects: " . implode( ',', $data) . "' );</script>";
    else
        $output = "<script>console.log( 'Debug Objects: " . $data . "' );</script>";

    echo $output;
}

?>