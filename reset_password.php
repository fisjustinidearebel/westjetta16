<?php
	require_once 'includes/config.php';

	$now = date('Y-m-d H:i:s');
	$valid = true;
	$errors = array();

	$password = (isset($_POST['password']))? $_POST['password'] : null;
	$confirm_password = (isset($_POST['confirm_password']))? $_POST['confirm_password'] : null;
	$reset_password_token = (isset($_POST['reset_password_token']))? escape($_POST['reset_password_token']) : '';
	
	// validate
	if(empty($password)){
		$valid = false;
		$errors['password'] = $copy[LANG]['password_required'];
	}elseif(strlen($password) < 6 || strlen($password) > 20){
		$valid = false;
		$errors['password'] = $copy[LANG]['password_invalid_length'];
	}
	if(empty($confirm_password)){
		$valid = false;
		$errors['confirm_password'] = $copy[LANG]['confirm_password_required'];
	}elseif($confirm_password != $password){
		$valid = false;
		$errors['confirm_password'] = $copy[LANG]['confirm_password_not_match'];
	}

	if(!$valid){
		$return = array('status'=>'error', 'errors'=>$errors);
		echo json_encode($return);
		exit;
	}
	
	// validate reset token
	$sql = "SELECT user_id FROM user WHERE reset_password_token = '$reset_password_token' AND reset_password_expire > '$now'";		
	$result = $db->query($sql);
	if($result->num_rows == 0){
		$return = array('status'=>'error', 'errors'=>$copy[LANG]['reset_token_invalid']);
		echo json_encode($return);
		exit;
	}

	$row = $result->fetch_assoc();
	$user_id = $row['user_id'];
	$password = md5($password);
	
	$sql = "UPDATE user SET
				password = '$password',
				reset_password_token = NULL,
				reset_password_expire = NULL
			WHERE user_id = $user_id";		
	$result = $db->query($sql);


	$return = array('status'=>'ok', 'message'=>$copy[LANG]['reset_password_success']);
	echo json_encode($return);
	exit;
?>