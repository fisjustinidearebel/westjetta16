<?php
        require_once('includes/config.php');
        require_once('check_session.php');

        include('header.php');
?>
        <div id="main">

                <div id="banner" class="home__hero">
                        <div class="container">
                                <div class="col_wrapper">
                                        <div class="col col_6 bg_overlay">
                                            <h1 class="section_heading"><?php echo $copy[LANG]['heading']; ?></h1>
                                            <p class="section_description"><?php echo $copy[LANG]['description_2']; ?></p>
                                        </div>
                                        <div class="col col_8"></div>
                                </div>
                        </div>
                </div>

<!--
<div class="hero flexslider" >
                <ul class="slides">
                <li>
                <div id="hero_1">
                        <div class="container">
                                <div class="col_wrapper">
                                        <?php /*if(LANG=='en'): ?>
                                        <div class="col col_4 bg_overlay">
                                        <?php else: ?>
                                        <div class="col col_6 bg_overlay_fr">
                                        <?php endif; ?>
                                                <h1 class="section_heading"><?php echo $copy[LANG]['heading']; ?></h1>
                                                <p class="section_description"><?php echo $copy[LANG]['description']; ?></p>
                                                <a href="javascript:void(0);" class="btn btn-default login_link"><?php echo $copy[LANG]['enter_now']; ?></a>
                                                </div>
                                                <div class="col col_8"></div>
                                        </div>
                                </div>
                        </div>
                </li>
                <li>
 <li>
                <div id="hero_2">
                        <div class="container">
                                <div class="col_wrapper">
                                        <?php if(LANG=='en'): ?>
                                        <div class="col col_4 bg_overlay">
                                        <?php else: ?>
                                        <div class="col col_6 bg_overlay_fr">
                                        <?php endif; ?>
                                                <h1 class="section_heading"><?php echo $copy[LANG]['heading']; ?></h1>
                                                <p class="section_description"><?php echo $copy[LANG]['description']; ?></p>
                                                <a href="javascript:void(0);" class="btn btn-default login_link"><?php echo $copy[LANG]['enter_now']; ?></a>
                                        </div>
                                        <div class="col col_8"></div>
                                </div>
                        </div>
                </div>
                </li>
                <li>
                <div id="hero_3">
                        <div class="container">
                                <div class="col_wrapper">
                                        <?php if(LANG=='en'): ?>
                                        <div class="col col_4 bg_overlay">
                                        <?php else: ?>
                                        <div class="col col_6 bg_overlay_fr">
                                        <?php endif; ?>
                                                <h1 class="section_heading"><?php echo $copy[LANG]['heading']; ?></h1>
                                                <p class="section_description"><?php echo $copy[LANG]['description']; ?></p>
                                                <a href="javascript:void(0);" class="btn btn-default login_link"><?php echo $copy[LANG]['enter_now']; */?></a>
                                        </div>
                                        <div class="col col_8"></div>
                                </div>
                        </div>
                </div>
                </li>
                </ul>
        </div>
        </div>
-->

        <div id="bookings" class="section">
                <div class="container">
                        <div class="col_wrapper">
                            <div class="col col_2"></div>
                            <div class="col col_8">
                                    <h2 class="section_heading"><?php echo $copy[LANG]['bookings_heading']; ?></h2>
                                    <p class="section_description"><?php echo $copy[LANG]['bookings_description']; ?></p>
                            </div>
                            <div class="col col_2"></div>
                        </div>
                            <div id="vacations_submissions" class="col_wrapper">
                                <!-- Vacation booking form -->
                                <div class="col col_1"></div>
                                <div class="col col_10">
                                    <div id="booking_alertmsg"></div>
                                    <form class="booking_form-row" id="booking_form_vacation" action="" method="post">
                                        <div class="form_row">
                                            <div class="booking_col-left">
                                                <label for="booking_code_vacation" id="vac_code-t1"><?php echo $copy[LANG]['booking_vacation_code_label']; ?></label>
                                                <label for="booking_code_vacation" id="vac_code-t2" style="margin-top:-18px;"><?php echo $copy[LANG]['booking_vacation_code_label_alt']; ?></label>
                                            </div>
                                            <div class="booking_col-right">
                                                <div class="field_group">
                                                <span class="error_msg"></span>
                                                    <input type="submit" id="booking_submit" class="btn btn-default" value="<?php echo $copy[LANG]['submit']; ?>">
                                                    <input type="text" name="booking_code_vacation" id="booking_code_vacation" class="textfield" value="">
                                                    <span class="error_msg"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col col_1"></div>
                            </div>

                            <!--Plus Fare Flight or Advanced Seat Purchase-->
                            <div id="booking_submissions" class="col_wrapper">
                                <div class="col col_1">
                                </div>
                                <div class="col col_10">
                                    <form id="booking_form" action="" method="post">
                                        <div class="form_row">
                                            <div class="booking_col-left">
                                                <label for="booking_code" id="booking_code-t1"><?php echo $copy[LANG]['booking_code_label']; ?></label>
                                                <label for="booking_code" id="booking_code-t2" style="margin-top:-18px;"><?php echo $copy[LANG]['booking_code_label_alt']; ?></label>
                                            </div>
                                            <div class="booking_col-right">
                                                <div class="field_group">
                                                    <input type="submit" id="booking_submit" class="btn btn-default" value="<?php echo $copy[LANG]['submit']; ?>">
                                                    <input type="text" name="booking_code" id="booking_code" class="textfield" value="">
                                                    <span class="error_msg"></span>
                                                    <div class="flight_checkbox">
                                                        <label><?php echo $copy[LANG]['booking_type_label_a']; ?></label>                                                        
                                                        <div class="checkbox">
                                                            <input for="booking_type_selection" class="checkbox" type="checkbox" id="booking_cb_plus"  name="booking_cb_plus" value="plus" >
                                                            <label for="booking_cb_plus"><?php echo $copy[LANG]['booking_type_label_plus']; ?></label>
                                                        </div>
                                                        <div class="booking_or"><?php echo $copy[LANG]['booking_type_label_b']; ?></div>
                                                        <div class="checkbox">
                                                            <input for="booking_type_selection" class="checkbox" type="checkbox" id="booking_cb_advanced" name="booking_cb_advanced" value="advanced" >
                                                            <label for="booking_cb_advanced"><?php echo $copy[LANG]['booking_type_label_advanced']; ?></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form_row" style="height:23px;">
                                            <div class="col col_2"></div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col col_1">
                                </div>
                            </div>

<!--
                            <div id="bookings_cta">
                                    <h2 class="section_heading"><?php echo $copy[LANG]['bookings_cta_heading']; ?></h2>
                                    <a href="<?php echo $copy[LANG]['book_flights_url']; ?>" target="_blank" class="btn btn-white"><?php echo $copy[LANG]['book_flights_cta']; ?></a>
                                    &nbsp;&nbsp;&nbsp;
                                    <a href="<?php echo $copy[LANG]['book_vacations_url']; ?>" target="_blank" class="btn btn-white"><?php echo $copy[LANG]['book_vacations_cta']; ?></a>
                            </div>
                            -->
                        </div>
                </div>
                <div id="game" class="section">
                        <div class="container">
                                <div class="col_wrapper">
                                        <div class="col col_2"></div>
                                        <div class="col col_8">
                                                <h2 class="section_heading"><?php echo $copy[LANG]['game_heading']; ?></h2>
                                                <p class="section_description"><?php echo $copy[LANG]['game_description']; ?></p>
                                        </div>
                                        <div class="col col_2"></div>

                                        <div class="col col_12">
                                            <div class="col col_1"></div>
                                            <div class="col col_10">
                                                <?php $event = get_active_event(); ?>
                                                <input type="hidden" id="current_event" value="<?php if($event) echo $event['event_id']; ?>">
                                                <div id="questions">
                                                    
                                                </div>
                                            <div class="col col_1"></div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>

<!--- HERO SLIDER START -->
<script type="text/javascript">

/** Code for mutually exclusive checkboxes **/

jQuery(document).ready(function () {
    
    // Checkboxes like radio btn
    jQuery("input:checkbox[class='checkbox']").click(function () { // Input type is checkbox having class checkbox.
        if (jQuery(this).is(":checked")) { // We have clicked this ceckbox.
            var radioCehckoBoxes = "input:checkbox[class='checkbox']"; // All checkboxes which have checkbox class.
            jQuery(radioCehckoBoxes).prop("checked", false); // Uncheck every checkboxes found in above statement.
            jQuery(this).prop("checked", true); // Now check the current clicked one.
        } else {
            jQuery(this).prop("checked", false);
        }
    });

});


</script>
<!--- HERO SLIDER END -->

<?php
        include('footer.php');
?>