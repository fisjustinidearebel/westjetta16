<?php 
	require_once 'includes/config.php';

	$now = date('Y-m-d H:i:s');
	$valid = true;
	$errors = array();
	
	$first_name = (isset($_POST['first_name']))? escape($_POST['first_name']) : null;
	$last_name = (isset($_POST['last_name']))? escape($_POST['last_name']) : null;
	$username = (isset($_POST['username']))? escape($_POST['username']) : null;
	$phone_number = (isset($_POST['phone_number']))? escape($_POST['phone_number']) : null;
	$agency_name = (isset($_POST['agency_name']))? escape($_POST['agency_name']) : null;
	$iata_number = (isset($_POST['iata_number']))? escape($_POST['iata_number']) : null;
	$email = (isset($_POST['email']))? escape($_POST['email']) : null;
	$confirm_email = (isset($_POST['confirm_email']))? escape($_POST['confirm_email']) : null;
	$password = (isset($_POST['password']))? $_POST['password'] : null;
	$confirm_password = (isset($_POST['confirm_password']))? $_POST['confirm_password'] : null;
	$rules_check = (isset($_POST['rules_check']))? escape($_POST['rules_check']) : '0';
	$comm_check = (isset($_POST['comm_check']))? escape($_POST['comm_check']) : '0';
	$captcha_key = (isset($_SESSION['qaptcha_key']))? $_SESSION['qaptcha_key'] : null;
	
	if(empty($first_name)){
		$valid = false;
		$errors['first_name'] = $copy[LANG]['first_name_required'];
	}
	if(empty($last_name)){
		$valid = false;
		$errors['last_name'] = $copy[LANG]['last_name_required'];
	}
	if(empty($username)){
		$valid = false;
		$errors['username'] = $copy[LANG]['username_required'];
	}elseif(strlen($username) < 6 || strlen($username) > 20){
		$valid = false;
		$errors['username'] = $copy[LANG]['username_invalid_length'];
	}
	if(empty($phone_number)){
		$valid = false;
		$errors['phone_number'] = $copy[LANG]['phone_number_required'];
	}elseif(!preg_match('/^[\d-+ ]+$/', $phone_number) || strlen($phone_number) < 10){
		$valid = false;
		$errors['phone_number'] = $copy[LANG]['phone_number_not_valid'];
	}
	if(empty($agency_name)){
		$valid = false;
		$errors['agency_name'] = $copy[LANG]['agency_name_required'];
	}
	if(empty($iata_number)){
		$valid = false;
		$errors['iata_number'] = $copy[LANG]['iata_required'];
	}elseif(!preg_match('/^\d+$/',$iata_number) || strlen($iata_number) != 8){
		$valid = false;
		$errors['iata_number'] = $copy[LANG]['iata_not_valid'];
	}
	if(empty($email)){
		$valid = false;
		$errors['email'] = $copy[LANG]['email_required'];
	}elseif(!valid_email($email)){
		$valid = false;
		$errors['email'] = $copy[LANG]['email_not_valid'];
	}
	if(empty($confirm_email)){
		$valid = false;
		$errors['confirm_email'] = $copy[LANG]['confirm_email_required'];
	}elseif($confirm_email != $email){
		$valid = false;
		$errors['confirm_email'] = $copy[LANG]['confirm_email_not_match'];
	}
	if(empty($password)){
		$valid = false;
		$errors['password'] = $copy[LANG]['password_required'];
	}elseif(strlen($password) < 6 || strlen($password) > 20){
		$valid = false;
		$errors['password'] = $copy[LANG]['password_invalid_length'];
	}
	if(empty($confirm_password)){
		$valid = false;
		$errors['confirm_password'] = $copy[LANG]['confirm_password_required'];
	}elseif($confirm_password != $password){
		$valid = false;
		$errors['confirm_password'] = $copy[LANG]['confirm_password_not_match'];
	}
	if($rules_check == '0'){
		$valid = false;
		$errors['rules_check'] = $copy[LANG]['rules_not_agree'];
	}
	if(empty($captcha_key)){
		$valid = false;
		$errors['captcha'] = $copy[LANG]['captcha_invalid'];
	}else{
		$captcha = (isset($_POST[$captcha_key]))? $_POST[$captcha_key] : null;
		if(!empty($captcha)){
			$valid = false;
			$errors['captcha'] = $copy[LANG]['captcha_invalid'];
		}
	}
	
	
	if(!$valid){
		$return = array('status'=>'error', 'errors'=>$errors);
		echo json_encode($return);
		exit;
	}
	
/*
	// check if iata number is already registered		
	$sql = "SELECT * FROM user WHERE iata_number = '$iata_number'";
	$result = $db->query($sql);
	// already registered
	if($result->num_rows){
		$return = array('status'=>'error', 'errors'=>$copy[LANG]['iata_already_registered']);
		echo json_encode($return);
		exit;
	}
*/
	// check if email is already registered		
	$sql = "SELECT * FROM user WHERE email = '$email'";
	$result = $db->query($sql);
	// already registered
	if($result->num_rows){
		$return = array('status'=>'error', 'errors'=>$copy[LANG]['email_already_registered']);
		echo json_encode($return);
		exit;
	}
	// check if username is available		
	$sql = "SELECT * FROM user WHERE username = '$username'";
	$result = $db->query($sql);
	if($result->num_rows){
		$return = array('status'=>'error', 'errors'=>$copy[LANG]['username_unavailable']);
		echo json_encode($return);
		exit;
	}
	
	$sql = "INSERT INTO user (
				first_name,
				last_name,
				username,
				phone_number,
				agency_name,
				iata_number,
				email,
				password,
				rules_check,
				comm_check,
				date_register
			) VALUES (
				'$first_name',
				'$last_name',
				'$username',
				'$phone_number',
				'$agency_name',
				'$iata_number',
				'$email',
				'".md5($password)."',
				'$rules_check',
				'$comm_check',
				'$now'
			)";
	if(!$result = $db->query($sql)){
		$return = array('status'=>'error', 'errors'=>$copy[LANG]['submission_error']);
		echo json_encode($return);
		exit;
	}
		
	$_SESSION['user'] = get_user_by_id($db->insert_id);
	$_SESSION['last_activity'] = time();	
	
	// reset captcha only if there are no errors, and registration is successfull
	unset($_SESSION['qaptcha_key']);

	$return = array('status'=>'ok');
	echo json_encode($return);
	exit;
?>