<?php
	require_once 'includes/config.php';

	$user_id = $_SESSION['user']['user_id'];
	$user = get_user_by_id($user_id);
	
	$_SESSION['user'] = $user;
	
	$return = array('status'=>'ok', 'user'=>$user);
	echo json_encode($return);
	exit;
?>