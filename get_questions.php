<?php
	require_once 'includes/config.php';

	$user_id = (isset($_SESSION['user']))? $_SESSION['user']['user_id'] : null;
	$event = get_active_event();
	$questions_html = get_event_questions_html($event['event_id']);
	$user_results = get_user_question_results_for_event($event['event_id'], $user_id);
	$next_event = get_next_event();
	
	if(empty($user_id)){
		$return = array('status'=>'error', 'error'=>'no user session');
		echo json_encode($return);
		exit;
	}
		
	if(!$event || !$questions_html){
		if (LANG == 'fr'){
			setlocale(LC_TIME, 'fr_FR');
			$date = strtolower(strftime('%A %e %B', strtotime($next_event['start_date'])));
			setlocale(LC_TIME, NULL);
		}else{
			$date = date('l F jS', strtotime($next_event['start_date']));
		}
		$questions_html = ($next_event)? '<p>'.sprintf($copy[LANG]['come_back_message'], $date).'</p>' : '<p>'.$copy[LANG]['no_more_questions'].'</p>';
		$return = array('status'=>'ok', 'questions_html'=>$questions_html);
		echo json_encode($return);
		exit;
	}
	
	$return = array('status'=>'ok', 'questions_html'=>$questions_html);
	
	if($user_results['num_answered'] > 0 && $user_results['num_answered'] < $event['num_questions'] && (!isset($_SESSION['user_notified']) || $_SESSION['user_notified'] != 1)){
		$return['message_title'] = $copy[LANG]['complete_the_quiz'];
		$return['message'] = sprintf($copy[LANG]['questions_incomplete_message'], ($event['num_questions'] - $user_results['num_answered']));
		$_SESSION['user_notified'] = 1;
	}

	echo json_encode($return);
	exit;
?>