<?php
	require_once 'includes/config.php';

	$valid = true;
	$errors = array();

	$user_id = $_SESSION['user']['user_id'];
	$vacation_code = (isset($_POST['vacation_code']))? escape($_POST['vacation_code']) : null;
	
	// validate
	if(empty($vacation_code)){
		$errors['vacation_code'] = $copy[LANG]['vacation_code_required'];
		$return = array('status'=>'error', 'errors'=>$errors);
		echo json_encode($return);
		exit;
	}
	
	// validate flight booking code
	if(preg_match('/^\d+$/',$vacation_code) && strlen($vacation_code) == 13){
		$vacation_type_id = 1;
		$vacation_type = $copy[LANG]['flight_code_name'];
	}
	// validate vacation booking code
	elseif(preg_match('/^\d+$/',$vacation_code) && strlen($vacation_code) == 7){
		$vacation_type_id = 2;
		$vacation_type = $copy[LANG]['vacation_code_name'];
	}else{
		$errors['vacation_code'] = $copy[LANG]['vacation_code_invalid'];
		$return = array('status'=>'error', 'errors'=>$errors);
		echo json_encode($return);
		exit;
		$errors['booking_code'] = $copy[LANG]['booking_code_invalid'];
		$return = array('status'=>'error', 'errors'=>$errors);
		echo json_encode($return);
		exit;
	}


	//check if vacation code has already been used		
	$sql = "SELECT * FROM user_booking WHERE vacation_code = '$vacation_code'";		
	$result = $db->query($sql);

	if($result->num_rows > 0){
		$errors = sprintf($copy[LANG]['vacation_code_used'], $vacation_code);
		$return = array('status'=>'error', 'errors'=>$errors);
		echo json_encode($return);
		exit;
	}

	// insert vacation
	$sql = "INSERT INTO user_booking (user_id, booking_type_id, vacation_code) VALUES ($user_id, $vacation_type_id, '$vacation_code')";		
	if(!$result = $db->query($sql)){
		$return = array('status'=>'error', 'errors'=>$copy[LANG]['submission_error']);
		echo json_encode($return);
		exit;
	}

	// get point value for given vacation type
	$point_value = get_vacation_type_point_value($vacation_type_id);
	
	// update user points
 	update_user_points($user_id, $point_value);

	$message = sprintf($copy[LANG]['vacation_code_success'], $vacation_type, $vacation_code, ($point_value? $point_value : ''));
	$return = array('status'=>'ok', 'message'=>$message);
	echo json_encode($return);
	exit;



?>