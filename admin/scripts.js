$(function(){
	
	if($('#start_date').length && $('#start_date').length){
	
		var startDateTextBox = $('#start_date');
		var endDateTextBox = $('#end_date');

		$.timepicker.datetimeRange(
			startDateTextBox,
			endDateTextBox,
			{
				minInterval: (1000*60*60), // 1hr
				dateFormat: 'yy-mm-dd', 
				timeFormat: 'HH:mm',
				start: {}, 
				end: {}					
			}
		); 
	}

	$('#add_event').click(function() {
		$('#new_event').slideDown('fast');		
	});
	$('#cancel_event').click(function() {
		$('#edit_event_form .error_msg').html('').hide();
		$('#new_event').slideUp('fast');		
	});
	$('#edit_event_form').submit(function() {
		var form = this;
		$(form).find('.error').removeClass('error');
		$(form).find('.error_msg').html('').hide();
		$(form).find('.error_messages').html('').hide();
		
		$.ajax({
	        type:'POST',
	        url: 'manage.php',
	        dataType: 'json',
	        data: $('#edit_event_form').serialize(),
	        success: function(data) {
		        if(data.status == 'ok'){
		        	location.href = 'event.php?e='+data.event_id;
		        }else{
					if(data.errors instanceof Object){
						$.each(data.errors, function(index, value){
							var input = $(form).find('[name='+index+']');
							if(input.prop('type') == 'checkbox')
								input = input.parent('.checkbox');
							input.addClass('error').find('+ .error_msg').html(value).css({'display':'block'});
						});
					}else{
						$(form).find('.error_messages').html(data.errors).css({'display':'block'});
					}
		        }     
	        }
	    });
		return false;
	});
	$('.delete_event_form').submit(function() {
		var form = $(this);
		var event_id = form.find('.event_id').val();
		if(confirm('Are you sure you want to permanently delete this event?')){
			$.ajax({
		        type:'POST',
		        url: 'manage.php',
		        dataType: 'json',
		        data: form.serialize(),
		        success: function(data) {
			        if(data.status == 'ok')
			        	$('#event_'+event_id).animate({'opacity':'0'}, 500, function(){
				        	$(this).slideUp(500, function(){
					        	$(this).remove();
				        	});
			        	});
			        else{
				        alert(data.errors);
			        }     
		        }
		    });
		}
	    return false;
	});
	$('.tab').click(function(){
		var pane = $(this).attr('id').replace('_tab', '_pane');
		$('.tab, .pane').removeClass('active');
		$(this).addClass('active');
		$('#'+pane).addClass('active');
	});





	$('#add_question').click(function() {
		$('#new_question').slideDown('fast');		
	});
	$('#cancel_question').click(function() {
		$('#edit_question_form').removeClass('true-false');
		$('#edit_question_form .error_msg').html('').hide();
		$('#edit_question_form .correct_check').prop('type','radio');
		$('#new_question').slideUp('fast');		
	});
	$('#question_type').change(function() {
		var value = $('#question_type').val();
		if(value == 3){
			$('#edit_question_form .correct_check').each(function(){
				$(this).prop('type','checkbox').attr('name', $(this).attr('name')+'['+$(this).val()+']');
			});
			if($('#edit_question_form').hasClass('true-false')){
				$('#edit_question_form').removeClass('true-false');
				$('#answers .answer_text_en').val('');
				$('#answers .answer_text_fr').val('');
			}
		}else{
			if(value == 2){
				while($('#answers .answer').length < 2){
					add_answer();
				}
				$('#answers .answer').each(function(index) { 
					if(index == 0){
						$(this).find('.answer_text_en').val('True');
						$(this).find('.answer_text_fr').val('Vrai');
					}else if(index == 1){
						$(this).find('.answer_text_en').val('False');
						$(this).find('.answer_text_fr').val('Faux');
					}else{
						$(this).remove();
					}
				});
				$('#edit_question_form').addClass('true-false');
			}else{
				if($('#edit_question_form').hasClass('true-false')){
					$('#edit_question_form').removeClass('true-false');
					$('#answers .answer_text_en').val('');
					$('#answers .answer_text_fr').val('');
				}
			}
			$('#edit_question_form .correct_check').each(function(){
				$(this).prop('type','radio').attr('name', $(this).attr('name').replace(/\[\d+\]$/, ''));
			});
		}
	});	
	$('#add_answer').click(function() {
		add_answer();
	});
	$('#answers').on('click', '.delete_answer', function() {
		var answer = $(this).parents('.answer')
		var question = answer.parents('#answers');
		var id_to_delete = (answer.find('.answer_id').val() != '')? answer.find('.answer_id').val()+',' : '';
		$('#answers_to_delete').val($('#answers_to_delete').val()+id_to_delete);
		answer.remove();
	    question.find('.answer').each(function(index) {
			if($('#question_type').val() == 3)
				$(this).find('.correct_check').attr('name', $(this).find('.correct_check').attr('name').replace(/\[\d+\]$/, '['+index+']'));
			$(this).find('.correct_check').val(index);
			$(this).find('.answer_text_en').attr('name', $(this).find('.answer_text_en').attr('name').replace(/\[\d+\]$/, '['+index+']'));
			$(this).find('.answer_text_fr').attr('name', $(this).find('.answer_text_fr').attr('name').replace(/\[\d+\]$/, '['+index+']'));
			$(this).find('.answer_id').attr('name', $(this).find('.answer_id').attr('name').replace(/\[\d+\]$/, '['+index+']'));
		});
	});
	$('#edit_question_form').submit(function() {
		var form = this;
		$(form).find('.error').removeClass('error');
		$(form).find('.error_msg').html('').hide();
		$(form).find('.error_messages').html('').hide();
		
		$.ajax({
	        type:'POST',
	        url: 'manage.php',
	        dataType: 'json',
	        data: $('#edit_question_form').serialize(),
	        success: function(data) {
		        if(data.status == 'ok')
		        	location.href = 'event.php?e='+$('#event_id').val();
		        else{
					if(data.errors instanceof Object){
						$.each(data.errors, function(index, value){
							if(index == 'answers'){
								var input = $(form).find('#answers');
							}else{
								var input = $(form).find('[name='+index+']');
								if(input.prop('type') == 'checkbox')
									input = input.parent('.checkbox');
							}
							if(value instanceof Array)
								input.addClass('error').find('+ .error_msg').html(value.join('<br>')).css({'display':'block'});
							else
								input.addClass('error').find('+ .error_msg').html(value).css({'display':'block'});
						});
					}else{
						$(form).find('.error_messages').html(data.errors).css({'display':'block'});
					}
		        }     
	        }
	    });
		return false;
	});
	$('.delete_question_form').submit(function() {
		var form = $(this);
		var question_id = form.find('.question_id').val();
		if(confirm('Are you sure you want to permanently delete this question?')){
			$.ajax({
		        type:'POST',
		        url: 'manage.php',
		        dataType: 'json',
		        data: form.serialize(),
		        success: function(data) {
			        if(data.status == 'ok')
			        	$('#question_'+question_id).animate({'opacity':'0'}, 500, function(){
				        	$(this).slideUp(500, function(){
					        	$(this).remove();
				        	});
			        	});
			        else{
				        alert(data.errors);
			        }     
		        }
		    });
		}
	    return false;
	});

});

function add_answer(){
	var index = $('#edit_question_form').find('.answer').length;
	var html = '<div class="answer col-wrapper">'+
					'<div class="col col-10 text-center">'+
						(($('#question_type').val() == 3)
							?'<input type="checkbox" name="answer_correct['+index+']" class="correct_check" value="'+index+'">'
							:'<input type="radio" name="answer_correct" class="correct_check" value="'+index+'">')+
					'</div>'+
					'<div class="col col-40">'+
						'<input type="text" name="answer_text_en['+index+']" class="answer_text_en" value="">'+
					'</div>'+
					'<div class="col col-40">'+
						'<input type="text" name="answer_text_fr['+index+']" class="answer_text_fr" value="">'+
					'</div>'+
					'<div class="col col-10 text-center">'+
						'<input type="hidden" name="answer_id['+index+']" class="answer_id" value="">'+
						'<a href="javascript:void(0);" class="delete_answer"><i class="fa fa-minus-circle"></i></a>'+
					'</div>'+
				'</div>';
	$('#answers').append(html);	
}
