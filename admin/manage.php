<?php
require_once('../includes/config.php');

$action = isset($_POST['action'])? $_POST['action'] : null;
$now = date('Y-m-d H:i:s');

// EDIT EVENT
/***********************************************************/
if($action == 'edit_event') {
	$valid = true;
	$errors = array();
	
	$event_id = (isset($_POST['event_id']))? escape($_POST['event_id']) : 'NULL';
	$event_name = (isset($_POST['event_name']))? escape($_POST['event_name']) : null;
	$start_date = (isset($_POST['start_date']))? escape($_POST['start_date']) : null;	
	$end_date = (isset($_POST['end_date']))? escape($_POST['end_date']) : null;
	
	// validation
	if(empty($event_name)){
		$valid = false;
		$errors['event_name'] = 'Please enter an event name';
	}
	if(empty($start_date)){
		$valid = false;
		$errors['start_date'] = 'Please enter a start date';
	}elseif($start_date < $now){
		$valid = false;
		$errors['start_date'] = 'Please select a start date in the future';
	}
	if(empty($end_date)){
		$valid = false;
		$errors['end_date'] = 'Please enter an end date';
	}
	
	if(!$valid){
		$return = array('status'=>'error', 'errors'=>$errors);
		echo json_encode($return);
		exit;
	}


	$sql = "SELECT * FROM event WHERE event_id != ".($event_id == 'NULL'?"''":$event_id)." AND '$start_date' BETWEEN start_date AND end_date";
	if(!$result = $db->query($sql)){
		$return = array('status'=>'error', 'errors'=>'Error saving event. Please try again later.', 'mysql_error'=>'('.$db->errno.') '.$db->error);
		echo json_encode($return);
		exit;
	}
	if($result->num_rows > 0){
		$errors['start_date'] = 'Start date must not be during another event';
		$return = array('status'=>'error', 'errors'=>$errors);
		echo json_encode($return);
		exit;
	}
	$sql = "SELECT * FROM event WHERE event_id != ".($event_id == 'NULL'?"''":$event_id)." AND '$end_date' BETWEEN start_date AND end_date";
	if(!$result = $db->query($sql)){
		$return = array('status'=>'error', 'errors'=>'Error saving event. Please try again later.', 'mysql_error'=>'('.$db->errno.') '.$db->error);
		echo json_encode($return);
		exit;
	}
	if($result->num_rows > 0){
		$errors['start_date'] = 'End date must not be during another event';
		$return = array('status'=>'error', 'errors'=>$errors);
		echo json_encode($return);
		exit;
	}


	$sql = "INSERT INTO event (
				event_id,
				name, 
				start_date, 
				end_date
			) VALUES (
				$event_id,
				'$event_name', 
				'$start_date', 
				'$end_date'
			) ON DUPLICATE KEY UPDATE
				event_id = LAST_INSERT_ID(event_id),
				name = VALUES(name),
				start_date = VALUES(start_date),
				end_date = VALUES(end_date)";
	if(!$db->query($sql)){
		$return = array('status'=>'error', 'errors'=>'Error saving event. Please try again later.', 'mysql_error'=>'('.$db->errno.') '.$db->error);
		echo json_encode($return);
		exit;
	}

	$return = array('status'=>'ok', 'event_id'=>$db->insert_id);
	echo json_encode($return);
	exit;	
}


// DELETE EVENT
/***********************************************************/
if($action == 'delete_event') {
	
	$event_id = (isset($_POST['event_id']))? escape($_POST['event_id']) : 'NULL';

	$sql = "DELETE FROM event WHERE event_id = $event_id";
	if(!$db->query($sql)){
		$return = array('status'=>'error', 'errors'=>'Error deleting event. Please try again later.', 'mysql_error'=>'('.$db->errno.') '.$db->error);
		echo json_encode($return);
		exit;
	}

	$return = array('status'=>'ok');
	echo json_encode($return);
	exit;
}


// EDIT QUESTION
/***********************************************************/
if($action == 'edit_question') {
	$valid = true;
	$errors = array();
	$errors['answers'] = array();
	
	$question_id = (isset($_POST['question_id']))? escape($_POST['question_id']) : 'NULL';
	$event_id = (isset($_POST['event_id']))? escape($_POST['event_id']) : null;
	$question_type = (isset($_POST['question_type']))? escape($_POST['question_type']) : null;
	$point_value = (isset($_POST['point_value']))? escape($_POST['point_value']) : null;	
	$question_text_en = (isset($_POST['question_text_en']))? escape($_POST['question_text_en']) : null;
	$question_text_fr = (isset($_POST['question_text_fr']))? escape($_POST['question_text_fr']) : null;
	$correct_response_en = (isset($_POST['correct_response_en']))? escape($_POST['correct_response_en']) : null;
	$correct_response_fr = (isset($_POST['correct_response_fr']))? escape($_POST['correct_response_fr']) : null;
	$incorrect_response_en = (isset($_POST['incorrect_response_en']))? escape($_POST['incorrect_response_en']) : null;
	$incorrect_response_fr = (isset($_POST['incorrect_response_fr']))? escape($_POST['incorrect_response_fr']) : null;
	$answers_to_delete = (isset($_POST['answers_to_delete']))? escape($_POST['answers_to_delete']) : null;

	$answer_id = (isset($_POST['answer_id']))? escape($_POST['answer_id']) : null;
	$answer_text_en = (isset($_POST['answer_text_en']))? escape($_POST['answer_text_en']) : null;
	$answer_text_fr = (isset($_POST['answer_text_fr']))? escape($_POST['answer_text_fr']) : null;
	$answer_correct = (isset($_POST['answer_correct']))? escape($_POST['answer_correct']) : null;

	// validation
	if(empty($question_type)){
		$valid = false;
		$errors['question_type'] = 'Please select a question type';
	}
	if(empty($point_value) || is_nan($point_value)){
		$valid = false;
		$errors['point_value'] = 'Point value must be a valid integer greater than 0';
	}
	if(empty($question_text_en)){
		$valid = false;
		$errors['question_text_en'] = 'Please enter English question text';
	}
	if(empty($question_text_fr)){
		$valid = false;
		$errors['question_text_fr'] = 'Please enter French question text';
	}	
	if(empty($correct_response_en)){
		$valid = false;
		$errors['correct_response_en'] = 'Please enter an English correct answer response';
	}
	if(empty($correct_response_fr)){
		$valid = false;
		$errors['correct_response_fr'] = 'Please enter a French correct answer response';
	}
	if(empty($incorrect_response_en)){
		$valid = false;
		$errors['incorrect_response_en'] = 'Please enter an English incorrect answer response';
	}
	if(empty($incorrect_response_fr)){
		$valid = false;
		$errors['incorrect_response_fr'] = 'Please enter a French incorrect answer response';
	}
	if(empty($answer_id) || count($answer_id) < 2){
		$valid = false;
		$errors['answers'][] = 'At least 2 possible answers are required';
	}
	foreach($answer_id as $key=>$id){
		if(empty($answer_text_en[$key])){
			$msg = 'All answers require English text';
			if(!in_array($msg, $errors['answers']))
				$errors['answers'][] = $msg;
		}
		if(empty($answer_text_fr[$key])){
			$msg = 'All answers require French text';
			if(!in_array($msg, $errors['answers']))
				$errors['answers'][] = $msg;
		}
	}
	if(is_null($answer_correct)){
		$valid = false;
		$errors['answers'][] = 'Please select a correct answer';
	}
		
	
	if(!$valid){
		$return = array('status'=>'error', 'errors'=>$errors);
		echo json_encode($return);
		exit;
	}
	
	if(is_array($answer_correct))
		$num_correct_answers = count($answer_correct);
	else
		$num_correct_answers = 1;
	
	
	$sql = "INSERT INTO question (
				question_id,
				event_id, 
				question_type_id, 
				point_value,
				num_correct_answers, 
				question_text_en, 
				question_text_fr, 
				correct_response_en, 
				correct_response_fr, 
				incorrect_response_en, 
				incorrect_response_fr
			) VALUES (
				$question_id,
				$event_id, 
				$question_type, 
				$point_value,
				$num_correct_answers,
				'$question_text_en', 
				'$question_text_fr', 
				'$correct_response_en', 
				'$correct_response_fr', 
				'$incorrect_response_en', 
				'$incorrect_response_fr'
			) ON DUPLICATE KEY UPDATE
				question_id = LAST_INSERT_ID(question_id),
				question_type_id = VALUES(question_type_id),
				point_value = VALUES(point_value),
				num_correct_answers = VALUES(num_correct_answers),
				question_text_en = VALUES(question_text_en),
				question_text_fr = VALUES(question_text_fr),
				correct_response_en = VALUES(correct_response_en),
				correct_response_fr = VALUES(correct_response_fr),
				incorrect_response_en = VALUES(incorrect_response_en),
				incorrect_response_fr = VALUES(incorrect_response_fr)";
	if(!$db->query($sql)){
		$return = array('status'=>'error', 'errors'=>'Error saving question. Please try again later.', 'mysql_error'=>'('.$db->errno.') '.$db->error);
		echo json_encode($return);
		exit;
	}
	$question_id = $db->insert_id;
	
	$answers = array();
	foreach($answer_id as $key=>$id){
		$id = (!empty($answer_id[$key]))? $answer_id[$key] : 'NULL';
		$text_en = $answer_text_en[$key];
		$text_fr = $answer_text_fr[$key];
		if(is_array($answer_correct) && in_array($key, $answer_correct)){
			$correct = '1';
		}elseif($key == $answer_correct){
			$correct = '1';
		}else{
			$correct = '0';
		}
		$sql = "INSERT INTO answer (
					answer_id,
					question_id,
					text_en,
					text_fr,
					correct
				) VALUES (
					$id,
					$question_id,
					'$text_en',
					'$text_fr',
					'$correct'
				)
				ON DUPLICATE KEY UPDATE
					question_id = VALUES(question_id),
					text_en = VALUES(text_en),
					text_fr = VALUES(text_fr),
					correct = VALUES(correct)";
		if(!$db->query($sql)){
			$return = array('status'=>'error', 'errors'=>'Error saving question answers. Please try again later.', 'mysql_error'=>'('.$db->errno.') '.$db->error);
			echo json_encode($return);
			exit;
		}
	}
	
	if(!empty($answers_to_delete)){
		$answers_to_delete = rtrim($answers_to_delete, ',');
		$sql = "DELETE FROM answer WHERE answer_id IN ($answers_to_delete)";
		if(!$db->query($sql)){
			$return = array('status'=>'error', 'errors'=>'Error saving question answers. Please try again later.', 'mysql_error'=>'('.$db->errno.') '.$db->error);
			echo json_encode($return);
			exit;
		}
	}
	

	$return = array('status'=>'ok');
	echo json_encode($return);
	exit;	
}


// DELETE QUESTION
/***********************************************************/
if($action == 'delete_question') {
	
	$question_id = (isset($_POST['question_id']))? escape($_POST['question_id']) : 'NULL';

	$sql = "DELETE FROM question WHERE question_id = $question_id";
	if(!$db->query($sql)){
		$return = array('status'=>'error', 'errors'=>'Error deleting question. Please try again later.', 'mysql_error'=>'('.$db->errno.') '.$db->error);
		echo json_encode($return);
		exit;
	}

	$return = array('status'=>'ok');
	echo json_encode($return);
	exit;
}

exit;
?>