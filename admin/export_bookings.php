<?php
	require_once '../includes/config.php';

	$user = (isset($_GET['user']) && !empty($_GET['user']))? $_GET['user'] : null;
	$user_condition = (!empty($user))? "WHERE user_id = $user" : '';
	
	$sql = "SELECT 
				user_id,
				first_name,
				last_name,
				username,
				email,
				booking_code
			FROM user_booking
			JOIN user USING(user_id)
			$user_condition
			ORDER BY booking_type_id ASC, user_booking_id DESC";	
	$bookings = $db->query($sql);
	
	$out = '';

	$out .= '"Username",';
	$out .= '"First name",';
	$out .= '"Last name",';
	$out .= '"Email",';
	$out .= '"Booking code"';
	$out .= "\n";

	while ( $booking = $bookings->fetch_assoc() ) { 
		$out .= '"'. $booking['username'].'",';
		$out .= '"'. $booking['first_name'].'",';
		$out .= '"'. $booking['last_name'].'",';
		$out .= '"'. $booking['email'].'",';
		$out .= '"'. $booking['booking_code'].'"';
		$out .= "\n";
	} 

	if(!empty($user)){
		$sql = "SELECT 
					username
				FROM user
				WHERE user_id = $user";	
		$result = $db->query($sql);
		$user_info = $result->fetch_assoc();
	}




	//Output to browser with appropriate mime type, you choose ;)
	header("Content-type: text/x-csv");
	//header("Content-type: text/csv");
	//header("Content-type: application/csv");
	header("Content-Disposition: attachment; filename=Hooray_for_TAs_Bookings_".(!empty($user)?$user_info['username'].'_':'').date('Y-m-d').".csv");
	echo $out;
	exit;
?>