<!DOCTYPE html>
<html>
<head>
	<title>20 years with WestJet</title>
	<!-- jquery -->
	<script src="../assets/jquery-1.11.2.min.js"></script>
	<!-- jquery ui -->
	<link rel="stylesheet" type="text/css" href="../assets/jquery-ui-1.11.4.custom/jquery-ui.min.css"/ >
	<script src="../assets/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
	<!-- jquery timepicker addon -->
	<link rel="stylesheet" type="text/css" href="../assets/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.min.css"/ >
	<script src="../assets/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.min.js"></script>

	<!-- font awesome -->
	<link type="text/css" href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet">

	<link type="text/css" href="styles.css" rel="stylesheet">
	<script type="text/javascript" src="scripts.js"></script>
</head>
<body>
<div id="wrapper">
	<div id="topbar">
		<div class="inner">
			<img id="logo" src="images/logo.gif">
		</div>
	</div>
	<div id="navbar">
		<div class="inner">
			<ul class="nav">
				<li><a href="index.php">Dashboard</a></li>
				<li><a href="entrants.php">Entrants</a></li>
				<li><a href="bookings.php">Bookings</a></li>
			</ul>
		</div>
	</div>
