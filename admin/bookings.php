<?php
	require_once '../includes/config.php';
	require_once 'head.php';

	$type = (isset($_GET['type']) && !empty($_GET['type']))? $_GET['type'] : 1;
	if($type != 1 && $type != 2) $type = 1;
	$type_name = ($type == 1)? 'Flight' : 'Vacation';
	$user = (isset($_GET['user']) && !empty($_GET['user']))? $_GET['user'] : null;
	
	if(!empty($user)){
		$sql = "SELECT 
					username
				FROM user
				WHERE user_id = $user";	
		$result = $db->query($sql);
		$user_info = $result->fetch_assoc();
	}
?>


<div id="page">
	<h1 class="page_title">
		<?php echo $type_name; ?> Bookings :
		<?php if(!empty($user)): ?>
			<span class="highlight"><?php echo $user_info['username']; ?></span>
			<a href="bookings.php?type=<?php echo $type; ?>" class="btn btn-default btn-sm" style="margin-top:-5px;">Remove User Filter</a>
		<?php else: ?>
			<span class="highlight">All Users</span>
		<?php endif; ?>
	</h1>
	
	<a href="bookings.php?type=2<?php if(!empty($user)) echo '&user='.$user; ?>" class="btn btn-default">Vacation Bookings</a>
	<a href="bookings.php?type=1<?php if(!empty($user)) echo '&user='.$user; ?>" class="btn btn-default">Flight Bookings</a>

	<a href="export_bookings.php
			<?php if(!empty($user)) echo '?user='.$user; ?>" id="export" class="btn btn-default">Export <?php echo (!empty($user))? $user_info['username'] : 'All'; ?> Bookings</a>
</div>
<div id="full_width">
	<div id="bookings" class="clear">
		<table cellspacing="0" cellpadding="0">
			<tr>
				<th>Username</th>
				<th>First name</th>
				<th>Last name</th>
				<th>Email</th>
				<th>Booking code</th>
			</tr>

			<?php //*
			$user_condition = (!empty($user))? "AND user_id = $user" : '';
			
			$sql = "SELECT 
						user_id,
						first_name,
						last_name,
						username,
						email,
						booking_code
					FROM user_booking
					JOIN user USING(user_id)
					WHERE booking_type_id = $type 
					$user_condition
					ORDER BY user_booking_id DESC";	
			$bookings = $db->query($sql);
			?>
			<?php while ( $booking = $bookings->fetch_assoc() ): ?>		
				<tr>
					<td><a href="bookings.php?type=<?php echo $type; ?>&user=<?php echo $booking['user_id']; ?>"><?php echo $booking['username']; ?></td>
					<td><?php echo $booking['first_name']; ?></td>
					<td><?php echo $booking['last_name']; ?></td>
					<td><?php echo $booking['email']; ?></td>
					<td><?php echo $booking['booking_code']; ?></td>
				</tr>
			<?php endwhile;  //*/?>
		</table>
	</div>
</div>
<?php
	require_once 'foot.php';
?>