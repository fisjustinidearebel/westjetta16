<?php
	require_once '../includes/config.php';

	$out = '';

	$sql = "SELECT 
				*,
				IFNULL(question_points, 0) AS question_points,
				f_booking_points,
				v_booking_points,
				(IFNULL(question_points, 0) + f_booking_points + v_booking_points) AS total_points
			FROM user u
			LEFT JOIN (
				SELECT 
					user_id,
					SUM(points) AS question_points
				FROM user_question_points
				GROUP BY user_id
			) user_question_points_sum USING(user_id)
			LEFT JOIN (
				SELECT
					user_id,
					IFNULL(SUM(bt1.point_value), 0) as f_booking_points,
					IFNULL(SUM(bt2.point_value), 0) as v_booking_points
				FROM user u
				LEFT JOIN user_booking ub USING(user_id)
				LEFT JOIN booking_type bt1 ON ub.booking_type_id = bt1.booking_type_id AND bt1.booking_type_id = 1
				LEFT JOIN booking_type bt2 ON ub.booking_type_id = bt2.booking_type_id AND bt2.booking_type_id = 2
				GROUP BY u.user_id
			) user_booking_points USING(user_id)
			GROUP BY user_id
			ORDER BY total_points DESC";	
	$users = $db->query($sql);

	$out .= '"First name",';
	$out .= '"Last name",';
	$out .= '"Username",';
	$out .= '"Email",';
	$out .= '"Phone",';
	$out .= '"Agency name",';
	$out .= '"IATA",';
	$out .= '"JetMail",';
	$out .= '"Date registered",';
	$out .= '"Total ballots",';
	$out .= '"Vacation ballots",';
	$out .= '"Flight ballots",';
	$out .= '"Game ballots"';
	$out .= "\n";

	while ( $user = $users->fetch_assoc() ) { 
		$out .= '"'. $user['first_name'].'",';
		$out .= '"'. $user['last_name'].'",';
		$out .= '"'. $user['username'].'",';
		$out .= '"'. $user['email'].'",';
		$out .= '"'. $user['phone_number'].'",';
		$out .= '"'. $user['agency_name'].'",';
		$out .= '"'. $user['iata_number'].'",';
		$out .= '"'. ($user['comm_check'] == 1?'yes':'no').'",';
		$out .= '"'. $user['date_register'].'",';
		$out .= '"'. $user['total_points'].'",';
		$out .= '"'. $user['v_booking_points'].'",';
		$out .= '"'. $user['f_booking_points'].'",';
		$out .= '"'. $user['question_points'].'"';
		$out .= "\n";
	} 

	//Output to browser with appropriate mime type, you choose ;)
	header("Content-type: text/x-csv");
	//header("Content-type: text/csv");
	//header("Content-type: application/csv");
	header("Content-Disposition: attachment; filename=Hooray_for_TAs_Users_".date('Y-m-d').".csv");
	echo $out;
	exit;
?>