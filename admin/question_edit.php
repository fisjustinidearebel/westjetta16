<?php
	require_once '../includes/config.php';

	$question_id = (isset($_GET['q']) && !empty($_GET['q']))? $_GET['q'] : null;
	if(empty($question_id)){
		$event_id = (isset($_GET['e']) && !empty($_GET['e']))? $_GET['e'] : null;
		if(empty($event_id)){
			header('Location: index.php');
			exit;
		}
		header('Location: event.php?e='.$event_id);
		exit;
	}
	
	require_once 'head.php';
	
	$num_answers = 0;
	
	$sql = "SELECT * FROM question q
			WHERE question_id = $question_id";	
	$result = $db->query($sql);
	if($result->num_rows > 0)
		$question = $result->fetch_assoc();			
	
	$sql = "SELECT * FROM answer a
			WHERE question_id = $question_id";	
	$answers = $db->query($sql);
	$num_answers = $answers->num_rows;
?>
<div id="page">
	<h1 class="page_title">Edit Question</h1>

	<div class="question">
		<form action="" method="post" id="edit_question_form" class="form-style">
			<div class="form-row">
				<label for="question_type">Question type:</label>
				<select name="question_type" id="question_type">
					<option value="">Please Select...</option>
					<?php $question_types = $db->query("SELECT * FROM question_type"); ?>
					<?php while ( $type = $question_types->fetch_assoc() ): ?>
					<option value="<?php echo $type['question_type_id']; ?>"<?php if(isset($question) && $question['question_type_id'] == $type['question_type_id']) echo 'selected'; ?>>
						<?php echo $type['type_name']; ?>
					</option>
					<?php endwhile; ?>		
				</select>
				<span class="error_msg"></span>
			</div>
			<div class="form-row">
				<label for="point_value">Point value: </label>
				<input type="text" name="point_value" id="point_value" value="<?php echo (isset($question))? $question['point_value'] : '1'; ?>">
				<span class="error_msg"></span>
			</div>
			<div class="form-row">
				<div class="col-wrapper">
					<div class="col col-50">
						<label for="question_text_en">Question <span class="not_bold">(english)</span>:</label>
						<textarea name="question_text_en" id="question_text_en" rows="4"><?php if (isset($question)) echo $question['question_text_en']; ?></textarea>
						<span class="error_msg"></span>
					</div>
					<div class="col col-50">
						<label for="question_text_fr"><span class="not_bold">(french)</span>:</label>
						<textarea name="question_text_fr" id="question_text_fr" rows="4"><?php if (isset($question)) echo $question['question_text_fr']; ?></textarea>
						<span class="error_msg"></span>
					</div>
				</div>
			</div>
			<div class="form-row">
				<label for="question_text_en">Answers:</label>
				<div id="answers">
					<div class=" col-wrapper">
						<div class="col col-10 text-center">
							<span for="">Correct</span><br>
						</div>
						<div class="col col-40">
							<label class="not_bold">(english):</label>
						</div>
						<div class="col col-40">
							<label class="not_bold">(french):</label>
						</div>
						<div class="col col-10">
	
						</div>
					</div>
					<?php if(isset($answers)): $index = 0; ?>
						<?php while($answer = $answers->fetch_assoc()): ?>
						<div id="answer_<?php echo $answer['answer_id']; ?>" class="answer col-wrapper">
							<div class="col col-10 text-center">
								<?php if($question['question_type_id'] == 3): ?>
									<input type="checkbox" name="answer_correct[<?php echo $index; ?>]" class="correct_check" value="<?php echo $index; ?>" <?php if($answer['correct']) echo 'checked'; ?>>
								<?php else: ?>
									<input type="radio" name="answer_correct" class="correct_check" value="<?php echo $index; ?>" <?php if($answer['correct']) echo 'checked'; ?>>
								<?php endif; ?>
							</div>
							<div class="col col-40">
								<input type="text" name="answer_text_en[<?php echo $index; ?>]" class="answer_text_en" value="<?php echo $answer['text_en']; ?>">
							</div>
							<div class="col col-40">
								<input type="text" name="answer_text_fr[<?php echo $index; ?>]" class="answer_text_fr" value="<?php echo $answer['text_fr']; ?>">
							</div>
							<div class="col col-10 text-center">
								<input type="hidden" name="answer_id[<?php echo $index; ?>]" class="answer_id" value="<?php echo $answer['answer_id']; ?>">
								<a href="javascript:void(0);" class="delete_answer"><i class="fa fa-minus-circle"></i></a>
							</div>
						</div>
						<?php $index++; ?>
						<?php endwhile; ?>
					<?php endif; ?>
					<?php if($num_answers < MIN_ANSWERS): ?>
						<?php for($i=0; $i<MIN_ANSWERS-$num_answers; $i++): ?>
							<div class="answer col-wrapper">
								<div class="col col-10 text-center">
									<input type="radio" name="answer_correct" class="correct_check" value="<?php echo $index + $i; ?>">
								</div>
								<div class="col col-40">
									<input type="text" name="answer_text_en[<?php echo $index + $i; ?>]" class="answer_text_en" value="">
								</div>
								<div class="col col-40">
									<input type="text" name="answer_text_fr[<?php echo $index + $i; ?>]" class="answer_text_fr" value="">
								</div>
								<div class="col col-10 text-center">
									<input type="hidden" name="answer_id[<?php echo $index + $i; ?>]" class="answer_id" value="">
									<a href="javascript:void(0);" class="delete_answer"><i class="fa fa-minus-circle"></i></a>
								</div>
							</div>
						<?php endfor; ?> 
					<?php endif; ?>
				</div>
				<span class="error_msg"></span>
			</div>
			<div class="form-row">
				<button type="button" id="add_answer" class="btn btn-default btn-sm">Add Answer</button>
				<input type="hidden" id="answers_to_delete" name="answers_to_delete" value="">
			</div>
			<div class="form-row">
				<div class="col-wrapper">
					<div class="col col-50">
						<label for="correct_response_en">Correct Answer Response <span class="not_bold">(english)</span>:</label>
						<textarea name="correct_response_en" id="correct_response_en" rows="4"><?php if (isset($question)) echo $question['correct_response_en']; ?></textarea>
						<span class="error_msg"></span>
					</div>
					<div class="col col-50">
						<label for="correct_response_fr"><span class="not_bold">(french)</span>:</label>
						<textarea name="correct_response_fr" id="correct_response_fr" rows="4"><?php if (isset($question)) echo $question['correct_response_fr']; ?></textarea>
						<span class="error_msg"></span>
					</div>
				</div>
			</div>
			<div class="form-row">
				<div class="col-wrapper">
					<div class="col col-50">
						<label for="incorrect_response_en">Incorrect Answer Response <span class="not_bold">(english)</span>:</label>
						<textarea name="incorrect_response_en" id="incorrect_response_en" rows="4"><?php if (isset($question)) echo $question['incorrect_response_en']; ?></textarea>
						<span class="error_msg"></span>
					</div>
					<div class="col col-50">
						<label for="incorrect_response_fr"><span class="not_bold">(french)</span>:</label>
						<textarea name="incorrect_response_fr" id="incorrect_response_fr" rows="4"><?php if (isset($question)) echo $question['incorrect_response_fr']; ?></textarea>
						<span class="error_msg"></span>
					</div>
				</div>
			</div>
			<div class="form-row text-right">
				<input type="hidden" name="action" value="edit_question">
				<input type="hidden" name="event_id" id="event_id" value="<?php echo $question['event_id']; ?>">
				<input type="hidden" name="question_id" value="<?php echo $question_id; ?>">
				<input type="submit" id="save_question" class="btn btn-default" value="submit">
			</div>
			<div class="form_row">
				<div class="error_messages"></div>
			</div>
		</form>
	</div>
</div>
<?php
	require_once 'foot.php';
?>