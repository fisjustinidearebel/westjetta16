<?php
	require_once '../includes/config.php';
	require_once 'head.php';
?>


<div id="page">
	<h1 class="page_title">Entrants</h1>

	<a href="export_entrants.php" id="export" class="btn btn-default">Export Entrants</a>
</div>
<div id="full_width">
	<div id="entrants" class="clear">
		<table cellspacing="0" cellpadding="0">
			<tr>
				<th>First name</th>
				<th>Last name</th>
				<th>Username</th>
				<th>Email</th>
				<th>Phone</th>
				<th>Agency name</th>
				<th>IATA</th>
				<th>JetMail</th>
				<th>Date registered</th>
				<th>Points</th>
			</tr>

			<?php
			$sql = "SELECT 
						*,
						IFNULL(question_points, 0) AS question_points,
						f_booking_points,
						v_booking_points,
						(IFNULL(question_points, 0) + f_booking_points + v_booking_points) AS total_points
					FROM user u
					LEFT JOIN (
						SELECT 
							user_id,
							SUM(points) AS question_points
						FROM user_question_points
						GROUP BY user_id
					) user_question_points_sum USING(user_id)
					LEFT JOIN (
						SELECT
							user_id,
							IFNULL(SUM(bt1.point_value), 0) as f_booking_points,
							IFNULL(SUM(bt2.point_value), 0) as v_booking_points
						FROM user u
						LEFT JOIN user_booking ub USING(user_id)
						LEFT JOIN booking_type bt1 ON ub.booking_type_id = bt1.booking_type_id AND bt1.booking_type_id = 1
						LEFT JOIN booking_type bt2 ON ub.booking_type_id = bt2.booking_type_id AND bt2.booking_type_id = 2
						GROUP BY u.user_id
					) user_booking_points USING(user_id)
					GROUP BY user_id
					ORDER BY total_points DESC";	
			$users = $db->query($sql);
			?>
			<?php while ( $user = $users->fetch_assoc() ): ?>		
				<tr>
					<td><?php echo $user['first_name']; ?></td>
					<td><?php echo $user['last_name']; ?></td>
					<td><?php echo $user['username']; ?></td>
					<td><?php echo $user['email']; ?></td>
					<td><?php echo $user['phone_number']; ?></td>
					<td><?php echo $user['agency_name']; ?></td>
					<td><?php echo $user['iata_number']; ?></td>
					<td><?php echo ($user['comm_check'] == 1)? 'yes': 'no'; ?></td>
					<td><?php echo $user['date_register']; ?></td>
					<td>
						<div class="points">
							<?php echo $user['total_points']; ?>
							<div class="points_breakdown">
								<a href="bookings.php?type=2&user=<?php echo $user['user_id']; ?>">Vacation ballots</a> : <strong><?php echo $user['v_booking_points']; ?></strong><br>
								<a href="bookings.php?type=1&user=<?php echo $user['user_id']; ?>">Flight ballots</a> : <strong><?php echo $user['f_booking_points']; ?></strong><br>
								Game ballots : <strong><?php echo $user['question_points']; ?></strong>
							</div>
						</div>
					</td>
				</tr>
			<?php endwhile; ?>
		</table>
	</div>
</div>
<?php
	require_once 'foot.php';
?>