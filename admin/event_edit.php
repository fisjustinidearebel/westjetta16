<?php
	require_once '../includes/config.php';
	
	$event_id = (isset($_GET['e']) && !empty($_GET['e']))? $_GET['e'] : null;
	if(empty($event_id)){
		header('Location: index.php');
		exit;
	}
	
	$sql = "SELECT * FROM event e
			WHERE event_id = $event_id";	
	if($result = $db->query($sql)){
		if($result->num_rows > 0)
			$event = $result->fetch_assoc();
	}		

	$now = date('Y-m-d H:i:s');

	require_once 'head.php';
?>


<div id="page">
	<h1 class="page_title">Edit Event</h1>

	<div id="edit_event" class="clear">
		<form action="" method="" id="edit_event_form" class="form-style">
			<div class="form-row">
				<label for="event_name">Event Name: </label>
				<input type="text" name="event_name" id="event_name" value="<?php echo $event['name']; ?>">
				<span class="error_msg"></span>
			</div>
			<div class="form-row">
				<div class="col-wrapper">
					<div class="col col-50">
						<label for="start_date">Start Date:</label>
						<input type="text" name="start_date" id="start_date" class="datepicker" value="<?php echo $event['start_date']; ?>">
						<span class="error_msg"></span>
					</div>
					<div class="col col-50">
						<label for="end_date">End Date:</label>
						<input type="text" name="end_date" id="end_date" class="datepicker" value="<?php echo $event['end_date']; ?>">
						<span class="error_msg"></span>
					</div>
				</div>
			</div>
			<div class="form-row text-right">
				<input type="hidden" name="action" value="edit_event">
				<input type="hidden" name="event_id" value="<?php echo $event['event_id']; ?>">
				<input type="submit" id="save_event" class="btn btn-default" value="submit">
			</div>
			<div class="form_row">
				<div class="error_messages"></div>
			</div>
		</form>
	</div>
</div>
<?php
	require_once 'foot.php';
?>