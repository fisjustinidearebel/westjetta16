<?php
	require_once '../includes/config.php';

	$event_id = (isset($_GET['e']) && !empty($_GET['e']))? $_GET['e'] : null;
	if(empty($event_id)){
		header('Location: index.php');
		exit;
	}
	
	$sql = "SELECT * FROM event e
			WHERE event_id = $event_id";	
	if($result = $db->query($sql)){
		if($result->num_rows > 0)
			$event = $result->fetch_assoc();
	}		

	$now = date('Y-m-d H:i:s');

	require_once 'head.php';
?>
<div id="page">
	<?php if(!isset($event)): ?>
	<h1 class="page_title">Event: Not Found</h1>
	<?php else: ?>
	<?php 
		$is_active = ($event['start_date'] < $now && $event['end_date'] > $now); 
		$is_past = ($event['end_date'] < $now); 
	?>
	<h1 class="page_title">
		Event: <?php echo $event['name']; ?>
		<?php if(!$is_active && !$is_past): ?>
			<a href="event_edit.php?e=<?php echo $event_id; ?>" class="btn btn-default btn-sm" style="margin-top:-5px;">Edit Event</a>
		<?php endif; ?>
	</h1>
	<h2 class="page_subtitle"><?php echo date('Y-m-d H:i:s', strtotime($event['start_date'])); ?> - <?php echo date('Y-m-d H:i:s', strtotime($event['end_date'])); ?></h2>
	<?php if($is_active): ?>
		<div class="alert">
			<p>This event is currently active and cannot be edited.</p>
		</div>
	<?php elseif($is_past): ?>
		<div class="alert">
			<p>This event has past and can no longer be edited.</p>
		</div>
	<?php endif; ?>
	<?php if(!$is_active && !$is_past): ?>
		<button id="add_question" class="btn btn-default">Create New Question</button>
	<?php endif; ?>
	<div id="questions" class="clear">
		<div id="new_question">
			<form action="" method="post" id="edit_question_form" class="form-style">
				<div class="form-row">
					<label for="question_type">Question type:</label>
					<select name="question_type" id="question_type">
						<option value="">Please Select...</option>
						<?php $question_types = $db->query("SELECT * FROM question_type"); ?>
						<?php while ( $type = $question_types->fetch_assoc() ): ?>
						<option value="<?php echo $type['question_type_id']; ?>">
							<?php echo $type['type_name']; ?>
						</option>
						<?php endwhile; ?>		
					</select>
					<span class="error_msg"></span>
				</div>
				<div class="form-row">
					<label for="point_value">Point value: </label>
					<input type="text" name="point_value" id="point_value" value="1">
					<span class="error_msg"></span>
				</div>
				<div class="form-row">
					<div class="col-wrapper">
						<div class="col col-50">
							<label for="question_text_en">Question <span class="not_bold">(english)</span>:</label>
							<textarea name="question_text_en" id="question_text_en" rows="4"></textarea>
							<span class="error_msg"></span>
						</div>
						<div class="col col-50">
							<label for="question_text_fr"><span class="not_bold">(french)</span>:</label>
							<textarea name="question_text_fr" id="question_text_fr" rows="4"></textarea>
							<span class="error_msg"></span>
						</div>
					</div>
				</div>
				<div class="form-row">
					<label for="question_text_en">Answers:</label>
					<div id="answers">
						<div class=" col-wrapper">
							<div class="col col-10 text-center">
								<span for="">Correct</span><br>
							</div>
							<div class="col col-40">
								<label class="not_bold">(english):</label>
							</div>
							<div class="col col-40">
								<label class="not_bold">(french):</label>
							</div>
							<div class="col col-10">
		
							</div>
						</div>
						<?php for($i=0; $i<MIN_ANSWERS; $i++): ?>
							<div class="answer col-wrapper">
								<div class="col col-10 text-center">
									<input type="radio" name="answer_correct" class="correct_check" value="<?php echo $i; ?>">
								</div>
								<div class="col col-40">
									<input type="text" name="answer_text_en[<?php echo $i; ?>]" class="answer_text_en" value="">
								</div>
								<div class="col col-40">
									<input type="text" name="answer_text_fr[<?php echo $i; ?>]" class="answer_text_fr" value="">
								</div>
								<div class="col col-10 text-center">
									<input type="hidden" name="answer_id[<?php echo $i; ?>]" class="answer_id" value="">
									<a href="javascript:void(0);" class="delete_answer"><i class="fa fa-minus-circle"></i></a>
								</div>
							</div>
						<?php endfor; ?> 
					</div>
					<span class="error_msg"></span>
				</div>
				<div class="form-row">
					<button type="button" id="add_answer" class="btn btn-default btn-sm">Add Answer</button>
				</div>
				<div class="form-row">
					<div class="col-wrapper">
						<div class="col col-50">
							<label for="correct_response_en">Correct Answer Response <span class="not_bold">(english)</span>:</label>
							<textarea name="correct_response_en" id="correct_response_en" rows="4"></textarea>
							<span class="error_msg"></span>
						</div>
						<div class="col col-50">
							<label for="correct_response_fr"><span class="not_bold">(french)</span>:</label>
							<textarea name="correct_response_fr" id="correct_response_fr" rows="4"></textarea>
							<span class="error_msg"></span>
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="col-wrapper">
						<div class="col col-50">
							<label for="incorrect_response_en">Incorrect Answer Response <span class="not_bold">(english)</span>:</label>
							<textarea name="incorrect_response_en" id="incorrect_response_en" rows="4"><?php if (isset($question)) echo $question['incorrect_response_en']; ?></textarea>
							<span class="error_msg"></span>
						</div>
						<div class="col col-50">
							<label for="incorrect_response_fr"><span class="not_bold">(french)</span>:</label>
							<textarea name="incorrect_response_fr" id="incorrect_response_fr" rows="4"><?php if (isset($question)) echo $question['incorrect_response_fr']; ?></textarea>
							<span class="error_msg"></span>
						</div>
					</div>
				</div>
				<div class="form-row text-right">
					<input type="hidden" name="action" value="edit_question">
					<input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id; ?>">
					<input type="reset" id="cancel_question" class="btn btn-default" value="Cancel">
					<input type="submit" id="save_question" class="btn btn-default" value="submit">
				</div>
				<div class="form_row">
					<div class="error_messages"></div>
				</div>
			</form>
		</div>
		<?php
		$sql = "SELECT * FROM question q
				LEFT JOIN question_type qt USING(question_type_id)
				WHERE event_id = $event_id";	
		$questions = $db->query($sql);
		?>
		<?php while ( $question = $questions->fetch_assoc() ): ?>		
			<div id="question_<?php echo $question['question_id']; ?>" class="question">
				<div class="question_header">
					<p><?php echo $question['type_name']; ?><span class="pull-right">Point Value: <?php echo $question['point_value']; ?></span></p>
				</div>
				<div class="question_text">
					<div class="col-wrapper">
						<div class="col col-50">
							<label>Question (english):</label>
							<p><?php echo $question['question_text_en']; ?></p>
						</div>
						<div class="col col-50">
							<label>(french):</label>
							<p><?php echo $question['question_text_fr']; ?></p>
						</div>
					</div>
				</div>
				<?php
				$sub_sql = "SELECT * FROM answer WHERE question_id = ".$question['question_id'];
				$answers = $db->query($sub_sql);
				?>
				<div class="answers">
					<div class=" col-wrapper">
						<div class="col col-50">
							<label>Answers (english):</label>
						</div>
						<div class="col col-50">
							<label>(french):</label>
						</div>
					</div>
					<?php while ( $answer = $answers->fetch_assoc() ): ?>
						<div class="answer col-wrapper <?php if($answer['correct']) echo 'correct'; ?>">
							<div class="col col-50">
								<p><?php echo $answer['text_en'] ?></p>
							</div>
							<div class="col col-50">
								<p><?php echo $answer['text_fr'] ?></p>
							</div>
						</div>
					<?php endwhile; ?>
				</div>
				<div class="question_response">
					<div class="col-wrapper">
						<div class="col col-50">
							<label>Correct Answer Response (english):</label>
							<p><?php echo $question['correct_response_en']; ?></p>
						</div>
						<div class="col col-50">
							<label>(french):</label>
							<p><?php echo $question['correct_response_fr']; ?></p>
						</div>
					</div>
				</div>
				<div class="question_response">
					<div class="col-wrapper">
						<div class="col col-50">
							<label>Incorrect Answer Response (english):</label>
							<p><?php echo $question['incorrect_response_en']; ?></p>
						</div>
						<div class="col col-50">
							<label>(french):</label>
							<p><?php echo $question['incorrect_response_fr']; ?></p>
						</div>
					</div>
				</div>
				<?php if(!$is_active & !$is_past): ?>	
					<div class="question_actions">
						<a href="question_edit.php?q=<?php echo $question['question_id']; ?>" class="btn btn-default">Edit</a>
						<form action="" method="post" class="delete_question_form">
							<input type="hidden" name="action" value="delete_question">
							<input type="hidden" name="question_id" class="question_id" value="<?php echo $question['question_id']; ?>">
							<button type="submit" class="btn btn-default">Delete</button>
						</form>
					</div>
				<?php endif; ?>		
			</div>
		<?php endwhile; ?>
	</div>
	<?php endif; ?>
</div>
<?php
	require_once 'foot.php';
?>