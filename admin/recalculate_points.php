<?php
	require_once '../includes/config.php';
	
	// lock users table
	$sql = "LOCK TABLES 
				user WRITE, 
				user user_points WRITE, 
				user u WRITE, 
				user_question_points WRITE, 
				user_booking ub WRITE, 
				booking_type bt1 WRITE, 
				booking_type bt2 WRITE;";
	if(!$db->query($sql)){
		die('Error: '.$db->error);
	}
		
	// re calculate points column in user table
	$sql = "UPDATE user
			JOIN (
				SELECT
					user_id,
					(IFNULL(question_points, 0) + f_booking_points + v_booking_points) AS total_points
				FROM user user_points
				LEFT JOIN (
					SELECT 
						user_id,
						SUM(points) AS question_points
					FROM user_question_points
					GROUP BY user_id
				) user_question_points_sum USING(user_id)
				LEFT JOIN (
					SELECT
						user_id,
						IFNULL(SUM(bt1.point_value), 0) as f_booking_points,
						IFNULL(SUM(bt2.point_value), 0) as v_booking_points
					FROM user u
					LEFT JOIN user_booking ub USING(user_id)
					LEFT JOIN booking_type bt1 ON ub.booking_type_id = bt1.booking_type_id AND bt1.booking_type_id = 1
					LEFT JOIN booking_type bt2 ON ub.booking_type_id = bt2.booking_type_id AND bt2.booking_type_id = 2
					GROUP BY u.user_id
				) user_booking_points USING(user_id)
			) user_points_table
			SET points = user_points_table.total_points 
			WHERE user.user_id = user_points_table.user_id;";
	if(!$db->query($sql)){
		$error = $db->error;
		$db->query("UNLOCK TABLES");
		die('Error: '.$error);
	}	
	
	$affected_rows = $db->affected_rows;
	
	// unlock tables
	if(!$db->query("UNLOCK TABLES")){
		die('Error: '.$db->error);
	}
	
	die('Done. '.$affected_rows.' entrants needed updating.');
?>