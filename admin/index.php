<?php
	require_once '../includes/config.php';
	require_once 'head.php';

	$now = date('Y-m-d H:i:s');
?>


<div id="page">
	<h1 class="page_title">Dashboard</h1>

	<button id="add_event" class="btn btn-default">Create New Event</button>
	<div id="new_event" class="clear">
		<form action="" method="" id="edit_event_form" class="form-style">
			<div class="form-row">
				<label for="event_name">Event Name: </label>
				<input type="text" name="event_name" id="event_name" value="">
				<span class="error_msg"></span>
			</div>
			<div class="form-row">
				<div class="col-wrapper">
					<div class="col col-50">
						<label for="start_date">Start Date:</label>
						<input type="text" name="start_date" id="start_date" class="datepicker" value="">
						<span class="error_msg"></span>
					</div>
					<div class="col col-50">
						<label for="end_date">End Date:</label>
						<input type="text" name="end_date" id="end_date" class="datepicker" value="">
						<span class="error_msg"></span>
					</div>
				</div>
			</div>
			<div class="form-row text-right">
				<input type="hidden" name="action" value="edit_event">
				<input type="button" id="cancel_event" class="btn btn-default" value="Cancel">
				<input type="submit" id="save_event" class="btn btn-default" value="submit">
			</div>
			<div class="form_row">
				<div class="error_messages"></div>
			</div>
		</form>
	</div>
	<div id="events" class="clear">
		<ul class="event_tabs">
			<li id="current_tab" class="tab active">Current and Upcoming Events</li>
			<li id="past_tab" class="tab">Past Events</li>
		</ul>
		<div class="event_panes">
			<div id="current_pane" class="pane active">
			<?php
			$sql = "SELECT 
						e.*,
						COUNT(question_id) as num_questions
					FROM event e
					LEFT JOIN question q USING(event_id)
					WHERE end_date > '$now' 
					GROUP BY event_id
					ORDER BY end_date ASC";	
			$events = $db->query($sql);
			$active = false;
			?>
			<?php while ( $event = $events->fetch_assoc() ): ?>		
				<?php $active = ($event['start_date'] < $now && !$active);?>
				<div id="event_<?php echo $event['event_id']; ?>" class="event <?php if($active) echo 'active'; ?>">
					<h3 class="event_name"><a href="event.php?e=<?php echo $event['event_id']; ?>"><?php echo $event['name']; ?></a><?php if($active) echo ' - active'; ?></h3>
					<p class="event_dates"><?php echo date('Y-m-d H:i:s', strtotime($event['start_date'])); ?> - <?php echo date('Y-m-d H:i:s', strtotime($event['end_date'])); ?></p>
					<p class="event_questions"><strong><?php echo $event['num_questions']; ?></strong> questions created</p>
					<div class="event_actions">
						<a href="event.php?e=<?php echo $event['event_id']; ?>" class="btn btn-default btn-sm">View</a>
						<?php if(!$active): ?>
							<a href="event_edit.php?e=<?php echo $event['event_id']; ?>" class="btn btn-default btn-sm">Edit</a>
							<form action="" method="post" class="delete_event_form">
								<input type="hidden" name="action" value="delete_event">
								<input type="hidden" name="event_id" class="event_id" value="<?php echo $event['event_id']; ?>">
								<button type="submit" class="btn btn-default btn-sm">Delete</button>
							</form>
						<?php endif; ?>
					</div>
				</div>
			<?php endwhile; ?>
			</div>
			<div id="past_pane" class="pane">
			<?php
			$sql = "SELECT 
						e.*,
						COUNT(question_id) as num_questions
					FROM event e
					LEFT JOIN question q USING(event_id)
					WHERE end_date <= '$now' 
					GROUP BY event_id
					ORDER BY end_date ASC";	
			$events = $db->query($sql);
			?>
			<?php while ( $event = $events->fetch_assoc() ): ?>		
				<div id="event_<?php echo $event['event_id']; ?>" class="event">
					<h3 class="event_name"><a href="event.php?e=<?php echo $event['event_id']; ?>"><?php echo $event['name']; ?></a></h3>
					<p class="event_dates"><?php echo date('Y-m-d H:i:s', strtotime($event['start_date'])); ?> - <?php echo date('Y-m-d H:i:s', strtotime($event['end_date'])); ?></p>
					<p class="event_questions"><strong><?php echo $event['num_questions']; ?></strong> questions created</p>
					<div class="event_actions">
						<a href="event.php?e=<?php echo $event['event_id']; ?>" class="btn btn-default btn-sm">View</a>
					</div>
				</div>
			<?php endwhile; ?>
			</div>
		</div>
	</div>
</div>
<?php
	require_once 'foot.php';
?>