<?php
	require_once 'includes/config.php';

	$valid = true;
	$errors = array();

	$username_email = (isset($_POST['username_email']))? escape($_POST['username_email']) : null;
	$password = (isset($_POST['password']))? $_POST['password'] : null;
	
	// validate
	if(empty($username_email)){
		$valid = false;
		$errors['username_email'] = $copy[LANG]['username_email_required'];
	}
	if(empty($password)){
		$valid = false;
		$errors['password'] = $copy[LANG]['password_required'];
	}
	if(!$valid){
		$return = array('status'=>'error', 'errors'=>$errors);
		echo json_encode($return);
		exit;
	}else{
		$password = md5($password);
		
		//check if user exists		
		$sql = "SELECT
					user_id
				FROM user 
				WHERE (username = '$username_email' OR email = '$username_email') 
					AND password = '$password'";		
		$result = $db->query($sql);

		// user exists
		if($result->num_rows){
			$row = $result->fetch_assoc();
			
			$_SESSION['user'] = get_user_by_id($row['user_id']);
			$_SESSION['last_activity'] = time();

			$return = array('status'=>'ok');
			echo json_encode($return);
			exit;
		}else{
			$return = array('status'=>'error', 'errors'=>$copy[LANG]['username_password_not_valid']);
			echo json_encode($return);
			exit;
		}
	}
?>