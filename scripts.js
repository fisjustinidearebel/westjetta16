var run_update = false;

function create_popup(title, content){
	var overlay = $('.overlay').get(0);
	title = (typeof title !== 'undefined')? title : '';
	content = (typeof content !== 'undefined')? content : '';
	
	var popup = $('<div class="popup">'+
					'<div class="popup_header">'+
						'<h3>'+title+
							'<span class="popup_close fa-stack fa-lg">'+
								'<i class="fa fa-circle fa-stack-2x"></i>'+
								'<i class="fa fa-times fa-stack-1x fa-inverse"></i>'+
							'</span>'+
						'</h3>'+
					'</div>'+
					'<div class="popup_content"><p class="body_copy">'+content+'</p></div>'+
				'</div>');
	$(overlay).append(popup);
	return popup;
}

function open_popup(popup){
	if(!$(popup).parents('.overlay').is(':visible')){
		$(popup).parents('.overlay').fadeIn(300, function(){
			$('body').addClass('popup-open');
			$(popup).fadeIn(300);
		});
	}else{
		$('.popup:visible').fadeOut(300, function(){
			$(popup).fadeIn(300);
		});
	}
}

function close_popup(popup){
	$(popup).fadeOut(300, function(){
		var form = $(popup).find('form');
		$(form).find('.error').removeClass('error');
		$(form).find('.error_msg').html('').hide();
		$(form).find('.error_messages').html('').hide();
		form.each(function(){
			this.reset();
		});
		$('.overlay').fadeOut(300);
		$('body').removeClass('popup-open');
	});
}

function get_questions(){
	$.ajax({
		type: 'POST',
		url: 'get_questions.php',
		data: {},
		dataType:'json',
		success: function(data){
			if(data.status == 'ok'){
				$('#questions').append(data.questions_html);
				if(data.hasOwnProperty('message') && data.hasOwnProperty('message_title')){
					var popup = create_popup(data.message_title, data.message);
					open_popup(popup);
				}
			}
		}
	});
}

function update(){
	//if(run_update){
		$.ajax({
			type: 'POST',
			url: 'update.php',
			data: {
				event_id:$('#current_event').val()
			},
			dataType:'json',
			success: function(data){
				run_update = false;
				if(data.status == 'ok'){
					$('#site_nav .welcome .user_ballots').text(data.user.total_points);
				}
			}
		});
	//}
}

$(function(){	
		
	if($('#questions').length){
		get_questions();
	}

/*
	if($('#current_event').length == 1){
		var interval = self.setInterval(function(){
			update();
		},5000)
	}
*/
		
	// close popup
	$('.overlay').on('click', '.popup_close', function(){
		close_popup($(this).parents('.popup'));
	});
	// open login popup
	$('body').on('click', '.login_link', function(){
		open_popup('#login_popup');
	});
	// open register popup
	$('.register_link').click(function(){
		open_popup('#register_popup');
	});
	// open forgot password popup
	$('.forgot_password_link').click(function(){
		open_popup('#forgot_password_popup');
	});

	// submit registration form
	$('#register_form').submit(function(){
		var form = this;
		$(form).find('.error').removeClass('error');
		$(form).find('.error_msg').html('').hide();
		$(form).find('.error_messages').html('').hide();
		
		$.ajax({
			type: 'POST',
			url: 'register.php',
			data: $(form).serialize(),
			dataType:'json',
			success: function(data){
				if(data.status == 'ok'){
					document.location.href = "home.php";
					//window.location.assign("home.php");
				}else{
					if(data.errors instanceof Object){
						$.each(data.errors, function(index, value){
							if(index == 'captcha'){
								var input = $(form).find('.captcha');
							}else{
								var input = $(form).find('[name='+index+']');
								if(input.prop('type') == 'checkbox')
									input = input.parent('.checkbox');
							}
							input.addClass('error').find('+ .error_msg').html(value).css({'display':'block'});
						});
					}else{
						$(form).find('.error_messages').html(data.errors).css({'display':'block'});
					}
				}
			}
		});
		
		return false;	
	});

	// submit login form
	$('#login_form').submit(function(){
		var form = this;
		$(form).find('.error').removeClass('error');
		$(form).find('.error_msg').html('').hide();
		$(form).find('.error_messages').html('').hide();
		
		$.ajax({
			type: 'POST',
			url: 'login.php',
			data: $(form).serialize(),
			dataType:'json',
			success: function(data){
				if(data.status == 'ok'){
					window.location.assign("home.php");
				}else{
					if(data.errors instanceof Object){
						$.each(data.errors, function(index, value){
							var input = $(form).find('[name='+index+']');
							input.addClass('error').find('+ .error_msg').html(value).css({'display':'block'});
						});
					}else{
						$(form).find('.error_messages').html(data.errors).css({'display':'block'});
					}
				}
			}
		});
		return false;	
	});

	// submit forgot password form
	$('#forgot_password_form').submit(function(){
		var form = this;
		$(form).find('.error').removeClass('error');
		$(form).find('.error_msg').html('').hide();
		$(form).find('.error_messages').html('').hide();
		
		$.ajax({
			type: 'POST',
			url: 'forgot_password.php',
			data: $(form).serialize(),
			dataType:'json',
			success: function(data){
				if(data.status == 'ok'){
					$(form).find('.success_messages').html(data.message).css({'display':'block'});
					form.reset();
				}else{
					if(data.errors instanceof Object){
						$.each(data.errors, function(index, value){
							var input = $(form).find('[name='+index+']');
							input.addClass('error').find('+ .error_msg').html(value).css({'display':'block'});
						});
					}else{
						$(form).find('.error_messages').html(data.errors).css({'display':'block'});
					}
				}
			}
		});
		return false;	
	});

	// submit reset password form
	$('#reset_password_form').submit(function(){
		var form = this;
		$(form).find('.error').removeClass('error');
		$(form).find('.error_msg').html('').hide();
		$(form).find('.error_messages').html('').hide();
		
		$.ajax({
			type: 'POST',
			url: 'reset_password.php',
			data: $(form).serialize(),
			dataType:'json',
			success: function(data){
				if(data.status == 'ok'){
					$(form).find('.success_messages').html(data.message).css({'display':'block'});
					form.reset();
				}else{
					if(data.errors instanceof Object){
						$.each(data.errors, function(index, value){
							var input = $(form).find('[name='+index+']');
							input.addClass('error').find('+ .error_msg').html(value).css({'display':'block'});
						});
					}else{
						$(form).find('.error_messages').html(data.errors).css({'display':'block'});
					}
				}
			}
		});
		return false;	
	});

	// submit booking form
	$('#booking_form').submit(function(){
		var form = this;
		$(form).find('.error').removeClass('error');
		$(form).find('.error_msg').html('').hide();
		//form.find('.error_messages').html('').hide();
		
		if ($('.submission').length) {
			$('.submission').remove();
		}
		
		$.ajax({
			type: 'POST',
			url: 'submit_booking.php',
			data: $(form).serialize(),
			dataType:'json',
			success: function(data){
				update();
				$('#booking_submissions .submission')
					.css({'position:relative;display:inline-block;margin-bottom':'0px','padding-top':'21px','padding-bottom':'21px'})
					.animate({'opacity':'0','height':'0px','padding-top':'0px','padding-bottom':'0px','margin-top':'0px'},300, function(){
						$(this).remove();
					});
				if(data.status == 'ok'){
					$('#booking_alertmsg').prepend('<div class="submission valid col_12">'+data.message+'</div>');
					$('#booking_alertmsg .submission:last-child').slideDown(300, function(){
/*
						var submission = $(this);
						setTimeout(function(){
							submission.animate({'opacity':'0'}, 1000, function(){
								submission.slideUp(300);
							});
						}, 5000);
*/
					});
					form.reset();
				}else{
					if(data.errors instanceof Object){
						$.each(data.errors, function(index, value){
							var input = $(form).find('[name='+index+']');
							input.addClass('error').find('+ .error_msg').html(value).css({'display':'block'});
						});
					}else{
						$('#booking_alertmsg').prepend('<div class="submission invalid col_12">'+data.errors+'</div>');
						$('#booking_alertmsg .submission:last-child').slideDown(300, function(){
/*
							var submission = $(this);
							setTimeout(function(){
								submission.animate({'opacity':'0'}, 1000, function(){
									submission.slideUp(300);
								});
							}, 5000);
*/
						});
						form.reset();
					}
				}
			}
		});
		return false;	
	});
// submit booking form vacation
	$('#booking_form_vacation').submit(function(){
		var form = this;
		$(form).find('.error').removeClass('error');
		$(form).find('.error_msg').html('').hide();
		//form.find('.error_messages').html('').hide();
		
		if ($('.submission').length) {
			$('.submission').remove();
		}

		$.ajax({
			type: 'POST',
			url: 'submit_booking_vacation.php',
			data: $(form).serialize(),
			dataType:'json',
			success: function(data){
				update();
				$('#booking_form_vacation .submission')
					.css({'margin-bottom':'0px','padding-top':'21px','padding-bottom':'21px'})
					.animate({'opacity':'0','height':'0px','padding-top':'0px','padding-bottom':'0px','margin-top':'0px'},300, function(){
						$(this).remove();
					});
				if(data.status == 'ok'){
					$('#booking_alertmsg').prepend('<div class="submission valid col_12">'+data.message+'</div>');
					$('#booking_alertmsg .submission:last-child').slideDown(300, function(){
/*
						var submission = $(this);
						setTimeout(function(){
							submission.animate({'opacity':'0'}, 1000, function(){
								submission.slideUp(300);
							});
						}, 5000);
*/
					});
					form.reset();
				}else{
					if(data.errors instanceof Object){
						$.each(data.errors, function(index, value){
							var input = $(form).find('[name='+index+']');
							input.addClass('error').find('+ .error_msg').html(value).css({'display':'block'});
						});
					}else{
						$('#booking_alertmsg').prepend('<div class="submission invalid col_12">'+data.errors+'</div>');
						$('#booking_alertmsg .submission:last-child').slideDown(300, function(){
/*
							var submission = $(this);
							setTimeout(function(){
								submission.animate({'opacity':'0'}, 1000, function(){
									submission.slideUp(300);
								});
							}, 5000);
*/
						});
						form.reset();
					}
				}
			}
		});
		return false;	
	});

	// open / close question boxes
	$('#questions').on('click', '.question_text', function(){
		$(this).parents('.question').toggleClass('open');
	});

	// submit question
	$('#questions').on('submit', '.question_form', function(){
		var form = this;
		// only submit if user has selected
		if($(form).find('input[type=radio]:checked, input[type=checkbox]:checked').length > 0){
			$.ajax({
				type: 'POST',
				url: 'submit_question.php',
				data: $(form).serialize(),
				dataType:'json',
				success: function(data){
					update();
					if(data.status == 'ok'){
						$(form).parents('.question').find('+ .question').addClass('open');
						$(form).parents('.question').html($(data.question_html).html()).attr('class', $(data.question_html).attr('class'));
						if(data.hasOwnProperty('message') && data.hasOwnProperty('message_title')){
							var popup = create_popup(data.message_title, data.message);
							open_popup(popup);
						}
					}else{
						var popup = create_popup(data.message_title, data.errors);
						open_popup(popup);
					}
				}
			});
		}
		return false;
	});
	
	// fade scroll link on scroll
	$(window).scroll( function(){
		var scroll_top = $(window).scrollTop() * 1.1;
		var window_height = $(window).height();
		var position = 1 - (scroll_top / window_height);
		$('.scroll_link').css('opacity', position);
	});
	$('.scroll_link').click(function(){
		$('body,html').animate({scrollTop:710}, 500);
	});

	

});