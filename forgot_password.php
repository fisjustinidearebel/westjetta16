<?php
	require_once 'includes/config.php';
	require_once "Mail.php";
	
	$valid = true;
	$errors = array();

	$email = (isset($_POST['email']))? escape($_POST['email']) : null;
	
	// validate
	if(empty($email)){
		$valid = false;
		$errors['email'] = $copy[LANG]['email_required'];
	}elseif(!valid_email($email)){
		$valid = false;
		$errors['email'] = $copy[LANG]['email_not_valid'];
	}

	if(!$valid){
		$return = array('status'=>'error', 'errors'=>$errors);
		echo json_encode($return);
		exit;
	}
	
	$sql = "SELECT user_id FROM user WHERE email = '$email'";		
	$result = $db->query($sql);

	if($result->num_rows == 0){
		$errors['email'] = $copy[LANG]['email_has_no_account'];
		$return = array('status'=>'error', 'errors'=>$errors);
		echo json_encode($return);
		exit;
	}

	$row = $result->fetch_assoc();
	$user_id = $row['user_id'];
	$reset_password_token = md5($email.time());
	$reset_password_expire = date('Y-m-d H:i:s', strtotime('+1 hour'));
	
	$sql = "UPDATE user SET
				reset_password_token = '$reset_password_token',
				reset_password_expire = '$reset_password_expire'
			WHERE user_id = $user_id";		
	if(!$result = $db->query($sql)){
 		$errors['email'] = $copy[LANG]['submission_error'];
		$return = array('status'=>'error', 'errors'=>$errors);
		echo json_encode($return);
		exit;
	}


	$reset_url = BASE_URL.'?token='.$reset_password_token;

	// set up params for mail object
	$params['host'] = 'email-smtp.us-east-1.amazonaws.com';
	$params['port'] = '25'; 
	$params['auth'] = true; 
	$params['username'] = 'AKIAIENCKNXOPUH4IIAA';
	$params['password'] = 'Ai2+16uOk59d2ITr80cxfRL/bgUI44QMt5o7LCVfOnC7';
	
	$mail_object =& Mail::factory("smtp", $params);


	// build email
	$template = file_get_contents('forgot_password_email_template.html', true);
	$placeholders = array( 
		'{logo_image}',
		'{email_heading}',
		'{email_subheading}',
		'{email_copy_1}',
		'{email_copy_2}',
		'{email_button}',
		'{reset_url}',
		'{email_footer_1}',
		'{email_footer_2}',
		'{email_footer_3}',
		'{email_footer_4}',
		'{email_img_url}'
	);
	$replacements = array( 
		BASE_URL.'/images/logo.gif',
		$copy[LANG]['email_heading'],
		$copy[LANG]['email_subheading'],
		$copy[LANG]['email_copy_1'],
		$copy[LANG]['email_copy_2'],
		$copy[LANG]['email_button'],
		$reset_url,
		$copy[LANG]['email_footer_1'],
		$copy[LANG]['email_footer_2'],
		$copy[LANG]['email_footer_3'],
		$copy[LANG]['email_footer_4'],
		$copy[LANG]['email_img_url']
	);
	
	$content = str_replace( $placeholders, $replacements, $template );
    
	$headers['From'] = $copy[LANG]['email_from'];
	$headers['To'] = $email;
	$headers['Subject'] = $copy[LANG]['email_subject'];
    $headers['MIME-Version'] = '1.0';
	$headers['Content-Type'] = 'text/html; charset="UTF-8"';
    
    // send email, returns True if successful, PEAR Error Object if it fails, therefore strict comparison is needed here
    if($mail_object->send($email, $headers, $content) !== true ){
 		$errors['email'] = $copy[LANG]['submission_error'];
		$return = array('status'=>'error', 'errors'=>$errors);
		echo json_encode($return);
		exit;
	}


	$return = array('status'=>'ok', 'message'=>$copy[LANG]['reset_password_email_sent']);
	echo json_encode($return);
	exit;
?>