<?php
	require_once('includes/config.php');
	//require_once('check_session.php');
		
	$reset_token = (isset($_GET['token']))? $_GET['token'] : null;

	include('header-closed.php');
	
	$prize_lang = "prize_" . LANG;
	
	//Grand Prize
	$gp_winner = array();
	
	//$gp_winner[0] = array("name"=>"", "prize"=>"");
	$gp_winner[0] = array("name"=>"Sandy Lush (GB Travel)", "prize_en"=>"WestJet Vacations package for two<br>La Brisas (Huatulco)"
												, "prize_fr"=>"Forfait Vacances WestJet pour deux<br>La Brisas (Huatulco)");
	
	$gp_winner[1] = array("name"=>"Karen Gonzalez (Marlin Travel)", "prize_en"=>"WestJet Vacations package for two<br>Grand Riviera Princess All Suites Resort &amp; Spa (Riviera Maya)"
												, "prize_fr"=>"Forfait Vacances WestJet pour deux<br>Grand Riviera Princess All Suites Resort & Spa (Riviera Maya)");
	
	$gp_winner[2] = array("name"=>"Caroline Connelly (Marlin Travel)", "prize_en"=>"WestJet Vacations package for two<br>Crown Paradise Golden Resort (Puerto Vallarta)"
												, "prize_fr"=>"Forfait Vacances WestJet pour deux<br>Crown Paradise Golden Resort (Puerto Vallarta)");
	
	$gp_winner[3] = array("name"=>"Jared Cochlin (L &amp; L Travel Management)", "prize_en"=>"WestJet Vacations package for two<br>Croc’s Casino Resort (Costa Rica)"
													, "prize_fr"=>"Forfait Vacances WestJet pour deux<br>Croc’s Casino Resort (Costa Rica)");
	
	$gp_winner[4] = array("name"=>"Robyn Arnold (Travel Nation Canada)", "prize_en"=>"WestJet Vacations package for two<br>Royal Lahaina Resort (Maui)"
												, "prize_fr"=>"Forfait Vacances WestJet pour deux<br>Royal Lahaina Resort (Maui)");
	
	$gp_winner[5] = array("name"=>"Conrad Fernandes (World View Travel)", "prize_en"=>"WestJet Vacations package for two<br>Mandalay Bay (Las Vegas)"
												, "prize_fr"=>"Forfait Vacances WestJet pour deux<br>Mandalay Bay (Las Vegas)");
	
	$gp_winner[6] = array("name"=>"Diana Di Taddeo (HRG North America)", "prize_en"=>"WestJet Vacations package for two<br>SpringHill Suites Marriot (Anaheim)"
												, "prize_fr"=>"Forfait Vacances WestJet pour deux<br>SpringHill Suites Marriot (Anaheim)");
												
	//Round Trip Flights
	//$rt_winner[0] = array("name"=>"", "prize"=>"");
	$rt_winner[0] = array("name"=>"Kathy Fitzpatrick (Travel Resources)", "prize_en"=>"<span style='white-space:nowrap'>Round-trip flights for two to any WestJet destination</span>"
													, "prize_fr"=>"<span style='white-space:nowrap'>Vols aller-retour pour deux à la destination WestJet de votre choix</span>");
													
	$rt_winner[1] = array("name"=>"Amanda Eros (Marlin Travel Spruce Grove)", "prize_en"=>"<span style='white-space:nowrap'>Round-trip flights for two to any WestJet destination</span>"
													, "prize_fr"=>"<span style='white-space:nowrap'>Vols aller-retour pour deux à la destination WestJet de votre choix</span>");
													
	$rt_winner[2] = array("name"=>"Kelly Shea (5 10 Travel)", "prize_en"=>"<span style='white-space:nowrap'>Round-trip flights for two to any WestJet destination</span>"
													, "prize_fr"=>"<span style='white-space:nowrap'>Vols aller-retour pour deux à la destination WestJet de votre choix</span>");
													
	$rt_winner[3] = array("name"=>"Anja Chow (Uniglobe Sunburst Travel)", "prize_en"=>"<span style='white-space:nowrap'>Round-trip flights for two to any WestJet destination</span>"
													, "prize_fr"=>"<span style='white-space:nowrap'>Vols aller-retour pour deux à la destination WestJet de votre choix</span>");
													
	$rt_winner[4] = array("name"=>"Kory Chenard (HRG North America)", "prize_en"=>"<span style='white-space:nowrap'>Round-trip flights for two to any WestJet destination</span>"
													, "prize_fr"=>"<span style='white-space:nowrap'>Vols aller-retour pour deux à la destination WestJet de votre choix</span>");
	
	//End of Anywhere Winners
	$rt_winner[5] = array("name"=>"Kellie Horler (Merit Travel)", "prize_en"=>"<span style='white-space:nowrap'>Round-trip flights for two to any WestJet destination in Canada or the U.S.</span>"
													, "prize_fr"=>"<span style='white-space:nowrap'>Vols aller-retour pour deux à la destination WestJet canadienne ou américaine de votre choix</span>");
													
	$rt_winner[6] = array("name"=>"Jodi Sanderson (Carlson Wagonlit)", "prize_en"=>"<span style='white-space:nowrap'>Round-trip flights for two to any WestJet destination in Canada or the U.S.</span>"
													, "prize_fr"=>"<span style='white-space:nowrap'>Vols aller-retour pour deux à la destination WestJet canadienne ou américaine de votre choix</span>");
													
	$rt_winner[7] = array("name"=>"Sharon Boissonneault (House of Travel)", "prize_en"=>"<span style='white-space:nowrap'>Round-trip flights for two to any WestJet destination in Canada or the U.S.</span>"
													, "prize_fr"=>"<span style='white-space:nowrap'>Vols aller-retour pour deux à la destination WestJet canadienne ou américaine de votre choix</span>");
													
	$rt_winner[8] = array("name"=>"Gordana Stevovic (Merit Travel)", "prize_en"=>"<span style='white-space:nowrap'>Round-trip flights for two to any WestJet destination in Canada or the U.S.</span>"
													, "prize_fr"=>"<span style='white-space:nowrap'>Vols aller-retour pour deux à la destination WestJet canadienne ou américaine de votre choix</span>");
													
	$rt_winner[9] = array("name"=>"Victoria Brownlee (Merit Travel)", "prize_en"=>"<span style='white-space:nowrap'>Round-trip flights for two to any WestJet destination in Canada or the U.S.</span>"
													, "prize_fr"=>"<span style='white-space:nowrap'>Vols aller-retour pour deux à la destination WestJet canadienne ou américaine de votre choix</span>");
	
	//End of US/CA Winners											
	$rt_winner[10] = array("name"=>"Tara Ashe (Flight Centre Associates)", "prize_en"=>"<span style='white-space:nowrap'>Round-trip flights for two to any WestJet destination in Canada</span>"
													, "prize_fr"=>"<span style='white-space:nowrap'>Vols aller-retour pour deux à la destination WestJet canadienne de votre choix</span>");
													
	$rt_winner[11] = array("name"=>"Teresa Bortoluzzi (Marlin Travel)", "prize_en"=>"<span style='white-space:nowrap'>Round-trip flights for two to any WestJet destination in Canada</span>"
													, "prize_fr"=>"<span style='white-space:nowrap'>Vols aller-retour pour deux à la destination WestJet canadienne de votre choix</span>");
													
	$rt_winner[12] = array("name"=>"Sarah Phillips (Merit Loyalty)", "prize_en"=>"<span style='white-space:nowrap'>Round-trip flights for two to any WestJet destination in Canada</span>"
													, "prize_fr"=>"<span style='white-space:nowrap'>Vols aller-retour pour deux à la destination WestJet canadienne de votre choix</span>");
													
	$rt_winner[13] = array("name"=>"Lori Coelho (Vision Travel)", "prize_en"=>"<span style='white-space:nowrap'>Round-trip flights for two to any WestJet destination in Canada</span>"
													, "prize_fr"=>"<span style='white-space:nowrap'>Vols aller-retour pour deux à la destination WestJet canadienne de votre choix</span>");
													
	$rt_winner[14] = array("name"=>"Ally Guidi (Marlin Travel)", "prize_en"=>"<span style='white-space:nowrap'>Round-trip flights for two to any WestJet destination in Canada</span>"
													, "prize_fr"=>"<span style='white-space:nowrap'>Vols aller-retour pour deux à la destination WestJet canadienne de votre choix</span>");
													
	$rt_winner[15] = array("name"=>"Lourdo Fernandes (Merit Travel)", "prize_en"=>"<span style='white-space:nowrap'>Round-trip flights for two to any WestJet destination in Canada</span>"
													, "prize_fr"=>"<span style='white-space:nowrap'>Vols aller-retour pour deux à la destination WestJet canadienne de votre choix</span>");
													
	$rt_winner[16] = array("name"=>"Paul Ammerlaan (Uniglobe One Travel)", "prize_en"=>"<span style='white-space:nowrap'>Round-trip flights for two to any WestJet destination in Canada</span>"
													, "prize_fr"=>"<span style='white-space:nowrap'>Vols aller-retour pour deux à la destination WestJet canadienne de votre choix</span>");
													
	$rt_winner[17] = array("name"=>"Sabrina Meroli (HRG North America)", "prize_en"=>"<span style='white-space:nowrap'>Round-trip flights for two to any WestJet destination in Canada</span>"
													, "prize_fr"=>"<span style='white-space:nowrap'>Vols aller-retour pour deux à la destination WestJet canadienne de votre choix</span>");
													
	$rt_winner[18] = array("name"=>"Chris Walton (Uniglobe One Travel)", "prize_en"=>"<span style='white-space:nowrap'>Round-trip flights for two to any WestJet destination in Canada</span>"
													, "prize_fr"=>"<span style='white-space:nowrap'>Vols aller-retour pour deux à la destination WestJet canadienne de votre choix</span>");
													
	$rt_winner[19] = array("name"=>"Jasmine Win (Corporate Traveller)", "prize_en"=>"<span style='white-space:nowrap'>Round-trip flights for two to any WestJet destination in Canada</span>"
													, "prize_fr"=>"<span style='white-space:nowrap'>Vols aller-retour pour deux à la destination WestJet canadienne de votre choix</span>");
	//End of Standard Winners
	
	//WestJet Dollars
	//$wj_winner[0] = array("name"=>"", "prize"=>"");
	// $wj_winner[0] = array("name"=>"Pamela Alderdice", "prize_en"=>"100 WestJet Dollars", "prize_fr"=>"100 dollars WestJet");
	// $wj_winner[1] = array("name"=>"Dolores Brack", "prize_en"=>"100 WestJet Dollars", "prize_fr"=>"100 dollars WestJet");
	// $wj_winner[2] = array("name"=>"Cindy Cloke", "prize_en"=>"100 WestJet Dollars", "prize_fr"=>"100 dollars WestJet");
	// $wj_winner[3] = array("name"=>"Lesley Dodge", "prize_en"=>"100 WestJet Dollars", "prize_fr"=>"100 dollars WestJet");
	// $wj_winner[4] = array("name"=>"Kara Doucette", "prize_en"=>"100 WestJet Dollars", "prize_fr"=>"100 dollars WestJet");
	// $wj_winner[5] = array("name"=>"Kevin Fitzgerald", "prize_en"=>"100 WestJet Dollars", "prize_fr"=>"100 dollars WestJet");
	// $wj_winner[6] = array("name"=>"Celia Goagara", "prize_en"=>"100 WestJet Dollars", "prize_fr"=>"100 dollars WestJet");
	// $wj_winner[7] = array("name"=>"Nicole Hansen", "prize_en"=>"100 WestJet Dollars", "prize_fr"=>"100 dollars WestJet");
	// $wj_winner[8] = array("name"=>"Heidi Hodgkinson", "prize_en"=>"100 WestJet Dollars", "prize_fr"=>"100 dollars WestJet");
	// $wj_winner[9] = array("name"=>"Kassie Karelse", "prize_en"=>"100 WestJet Dollars", "prize_fr"=>"100 dollars WestJet");
	// $wj_winner[10] = array("name"=>"Shelley Letourneau", "prize_en"=>"100 WestJet Dollars", "prize_fr"=>"100 dollars WestJet");
	// $wj_winner[11] = array("name"=>"Pam Macey", "prize_en"=>"100 WestJet Dollars", "prize_fr"=>"100 dollars WestJet");
	// $wj_winner[12] = array("name"=>"Susan Maxwell", "prize_en"=>"100 WestJet Dollars", "prize_fr"=>"100 dollars WestJet");
	// $wj_winner[13] = array("name"=>"Deb McCallum", "prize_en"=>"100 WestJet Dollars", "prize_fr"=>"100 dollars WestJet");
	// $wj_winner[14] = array("name"=>"Marcellina Pottinger", "prize_en"=>"100 WestJet Dollars", "prize_fr"=>"100 dollars WestJet");
	// $wj_winner[15] = array("name"=>"Kahla Rakai", "prize_en"=>"100 WestJet Dollars", "prize_fr"=>"100 dollars WestJet");
	// $wj_winner[16] = array("name"=>"Cherylan Rocha", "prize_en"=>"100 WestJet Dollars", "prize_fr"=>"100 dollars WestJet");
	// $wj_winner[17] = array("name"=>"Sean Rollinson", "prize_en"=>"100 WestJet Dollars", "prize_fr"=>"100 dollars WestJet");
	// $wj_winner[18] = array("name"=>"Dave Starrie", "prize_en"=>"100 WestJet Dollars", "prize_fr"=>"100 dollars WestJet");
	// $wj_winner[19] = array("name"=>"Sophia Wilson", "prize_en"=>"100 WestJet Dollars", "prize_fr"=>"100 dollars WestJet");
//
	// $wj_winner[20] = array("name"=>"Flo Azulay", "prize_en"=>"50 WestJet Dollars", "prize_fr"=>"50 dollars WestJet");
	// $wj_winner[21] = array("name"=>"Wendy Blake", "prize_en"=>"50 WestJet Dollars", "prize_fr"=>"50 dollars WestJet");
	// $wj_winner[22] = array("name"=>"Catriona Charles", "prize_en"=>"50 WestJet Dollars", "prize_fr"=>"50 dollars WestJet");
	// $wj_winner[23] = array("name"=>"Melanie Loewen", "prize_en"=>"50 WestJet Dollars", "prize_fr"=>"50 dollars WestJet");
	// $wj_winner[24] = array("name"=>"Susan Richard", "prize_en"=>"50 WestJet Dollars", "prize_fr"=>"50 dollars WestJet");
	// $wj_winner[25] = array("name"=>"Ashley Stamp", "prize_en"=>"50 WestJet Dollars", "prize_fr"=>"50 dollars WestJet");
	// $wj_winner[26] = array("name"=>"Monica Taylor", "prize_en"=>"50 WestJet Dollars", "prize_fr"=>"50 dollars WestJet");
	// $wj_winner[27] = array("name"=>"Leslie Wilson", "prize_en"=>"50 WestJet Dollars", "prize_fr"=>"50 dollars WestJet");
	// $wj_winner[28] = array("name"=>"Nicola Wilson", "prize_en"=>"50 WestJet Dollars", "prize_fr"=>"50 dollars WestJet");
	// $wj_winner[29] = array("name"=>"Cheryl Woods", "prize_en"=>"50 WestJet Dollars", "prize_fr"=>"50 dollars WestJet");
?>
	<div id="main">
		<div id="banner">
			<div class="container">
				<div class="col_wrapper">
					<?php if(LANG == 'en'): ?>
					<div class="col col_4 bg_overlay">
					<?php elseif(LANG == 'fr'): ?>
					<div class="col col_6 bg_overlay">
					<?php endif; ?>
						<h1 class="section_heading"><?php echo $copy[LANG]['heading']; ?></h1>
						<p class="section_description"><?php echo $copy[LANG]['winner_header_body']; ?></p>
						<a href="javascript:void(0);" class="btn btn-default scroll_link"><i class="fa fa-chevron-down"></i></a>
					</div>
					<div class="col col_8"></div>
				</div>
			</div>
		</div>
		<div id="how_to_enter" class="section">
			<div class="container">
				<div class="col_wrapper">
					<div class="col col_1"></div>
					<div class="col col_10">
						<h2 class="section_heading"><?php echo $copy[LANG]['winner_heading']; ?></h2>
						<p class="section_description"><?php echo $copy[LANG]['winner_body']; ?></p>
						<hr class="page-hr">
					</div>
					<div class="col col_1"></div>
					
				</div>
				
				<!-- Grand Prizes -->
				<div class="col_wrapper">
					<div class="col col_12">
						<h3 class="prize_heading"><?php echo $copy[LANG]['gp_header']; ?></h3>
						<table class="prize_table">
							<tr class="header_row">
								<th><?php echo $copy[LANG]['name']; ?></th>
								<th><?php echo $copy[LANG]['prize']; ?></th>
							</tr>
						<?php
						foreach ($gp_winner as $winner){ 
							echo "<tr><td>{$winner['name']}</td><td>{$winner[$prize_lang]}</td></tr>";	
						} ?>
						</table>
					</div>
				</div><!-- End of Grand Prizes -->
				
				<!-- Round Trip -->
				<div class="col_wrapper">
					<div class="col col_12">
						<h3 class="prize_heading"><?php echo $copy[LANG]['rt_header']; ?></h3>
						<table class="prize_table">
							<tr class="header_row">
								<th><?php echo $copy[LANG]['name']; ?></th>
								<th><?php echo $copy[LANG]['prize']; ?></th>
							</tr>
						<?php
						foreach ($rt_winner as $winner){
							echo "<tr><td>{$winner['name']}</td><td>{$winner[$prize_lang]}</td></tr>";	
						} ?>
						</table>
					</div>
				</div><!-- End of Round Trip -->
				
				<!-- Westjet Dollars -->
				<?php if($wj_winner != null) { ?>
				<div class="col_wrapper">
					<div class="col col_12">
						<h3 class="prize_heading"><?php echo $copy[LANG]['wj_header']; ?></h3>
						<table class="prize_table">
							<tr class="header_row">
								<th><?php echo $copy[LANG]['name']; ?></th>
								<th><?php echo $copy[LANG]['prize']; ?></th>
							</tr>
						<?php
						foreach ($wj_winner as $winner){ 
							echo "<tr><td>{$winner['name']}</td><td>{$winner[$prize_lang]}</td></tr>";	
						} ?>
						</table>
					</div>
				</div><!-- End of Westjet Dollars -->
				<?php } ?>
				
				
			</div>
		</div>
	</div>

<?php
	include('footer.php');
?>

<div class="overlay">
	<div id="leaderboard_popup" class="popup">
		<div class="popup_header">
			<h3>
				<?php echo $copy[LANG]['leaderboard_heading']; ?>
				<span class="popup_close fa-stack fa-lg">
					<i class="fa fa-circle fa-stack-2x"></i>
					<i class="fa fa-times fa-stack-1x fa-inverse"></i>
				</span>
			</h3>
		</div>
		<div class="popup_content">
			<div id="leaderboard">
				<table>
					<thead>
						<tr>
							<th><?php echo $copy[LANG]['position']; ?></th>
							<th><?php echo $copy[LANG]['username']; ?></th>
							<th><?php echo $copy[LANG]['total_ballots']; ?></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</td>
							<td>Mahnoush </td>
							<td>
								<div class="ballots_wrapper">
									<span class="ballots">707</span>
									<div class="ballot_breakdown">
										<?php echo $copy[LANG]['vacation_ballots']; ?> : <strong>0</strong><br>
										<?php echo $copy[LANG]['flight_ballots']; ?>  : <strong>704</strong><br>
										<?php echo $copy[LANG]['quizzes']; ?> : <strong>3</strong>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>2</td>
							<td>skylink</td>
							<td>
								<div class="ballots_wrapper">
									<span class="ballots">510</span>
									<div class="ballot_breakdown">
										<?php echo $copy[LANG]['vacation_ballots']; ?> : <strong>0</strong><br>
										<?php echo $copy[LANG]['flight_ballots']; ?>  : <strong>492</strong><br>
										<?php echo $copy[LANG]['quizzes']; ?> : <strong>18</strong>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>3</td>
							<td>Katarinas</td>
							<td>
								<div class="ballots_wrapper">
									<span class="ballots">437</span>
									<div class="ballot_breakdown">
										<?php echo $copy[LANG]['vacation_ballots']; ?> : <strong>70</strong><br>
										<?php echo $copy[LANG]['flight_ballots']; ?>  : <strong>349</strong><br>
										<?php echo $copy[LANG]['quizzes']; ?> : <strong>18</strong>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>4</td>
							<td>SavvyAdam</td>
							<td>
								<div class="ballots_wrapper">
									<span class="ballots">365</span>
									<div class="ballot_breakdown">
										<?php echo $copy[LANG]['vacation_ballots']; ?> : <strong>0</strong><br>
										<?php echo $copy[LANG]['flight_ballots']; ?>  : <strong>353</strong><br>
										<?php echo $copy[LANG]['quizzes']; ?> : <strong>12</strong>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>5</td>
							<td>lorico</td>
							<td>
								<div class="ballots_wrapper">
									<span class="ballots">336</span>
									<div class="ballot_breakdown">
										<?php echo $copy[LANG]['vacation_ballots']; ?> : <strong>0</strong><br>
										<?php echo $copy[LANG]['flight_ballots']; ?>  : <strong>316</strong><br>
										<?php echo $copy[LANG]['quizzes']; ?> : <strong>20</strong>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>6</td>
							<td>ctmark</td>
							<td>
								<div class="ballots_wrapper">
									<span class="ballots">289</span>
									<div class="ballot_breakdown">
										<?php echo $copy[LANG]['vacation_ballots']; ?> : <strong>0</strong><br>
										<?php echo $copy[LANG]['flight_ballots']; ?>  : <strong>288</strong><br>
										<?php echo $copy[LANG]['quizzes']; ?> : <strong>1</strong>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>7</td>
							<td>searol</td>
							<td>
								<div class="ballots_wrapper">
									<span class="ballots">250</span>
									<div class="ballot_breakdown">
										<?php echo $copy[LANG]['vacation_ballots']; ?> : <strong>235</strong><br>
										<?php echo $copy[LANG]['flight_ballots']; ?>  : <strong>0</strong><br>
										<?php echo $copy[LANG]['quizzes']; ?> : <strong>15</strong>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>8</td>
							<td>leslie90</td>
							<td>
								<div class="ballots_wrapper">
									<span class="ballots">232</span>
									<div class="ballot_breakdown">
										<?php echo $copy[LANG]['vacation_ballots']; ?> : <strong>55</strong><br>
										<?php echo $copy[LANG]['flight_ballots']; ?>  : <strong>161</strong><br>
										<?php echo $copy[LANG]['quizzes']; ?> : <strong>16</strong>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>9</td>
							<td>bearjohnson</td>
							<td>
								<div class="ballots_wrapper">
									<span class="ballots">229</span>
									<div class="ballot_breakdown">
										<?php echo $copy[LANG]['vacation_ballots']; ?> : <strong>5</strong><br>
										<?php echo $copy[LANG]['flight_ballots']; ?>  : <strong>205</strong><br>
										<?php echo $copy[LANG]['quizzes']; ?> : <strong>19</strong>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>10</td>
							<td>MarieFrrance</td>
							<td>
								<div class="ballots_wrapper">
									<span class="ballots">213</span>
									<div class="ballot_breakdown">
										<?php echo $copy[LANG]['vacation_ballots']; ?> : <strong>0</strong><br>
										<?php echo $copy[LANG]['flight_ballots']; ?>  : <strong>194</strong><br>
										<?php echo $copy[LANG]['quizzes']; ?> : <strong>19</strong>
									</div>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>