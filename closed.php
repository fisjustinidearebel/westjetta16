<?php
	require_once('includes/config.php');
	require_once('check_session.php');
		
	$reset_token = (isset($_GET['token']))? $_GET['token'] : null;

	include('header-closed.php');
?>
	<div id="main">
		<div id="banner">
			<div class="container">
				<div class="col_wrapper">
					<?php if(LANG == 'en'): ?>
					<div class="col col_4 bg_overlay">
					<?php elseif(LANG == 'fr'): ?>
					<div class="col col_6 bg_overlay">
					<?php endif; ?>
						<h1 class="section_heading"><?php echo $copy[LANG]['heading']; ?></h1>
						<p class="section_description"><?php echo $copy[LANG]['closed_subheader']; ?></p>
						<!-- <a href="javascript:void(0);" class="btn btn-default scroll_link"><i class="fa fa-chevron-down"></i></a> -->
					</div>
					<div class="col col_8"></div>
				</div>
			</div>
		</div>
		<div id="how_to_enter" class="section">
			<div class="container">
				<div class="col_wrapper">
					<div class="col col_1"></div>
					<div class="col col_10">
						<h2 class="section_heading" style="display: none;"><?php echo $copy[LANG]['how_to_enter_heading']; ?></h2>
						<p class="section_description"><?php echo $copy[LANG]['closed_body']; ?></p>
					</div>
					<div class="col col_1"></div>
				</div>
			</div>
		</div>

		<div id="prizes_banner" class="section lo">
			<div class="container">
				<div class="col_wrapper">
						<div class="col col_2"></div>
						<div class="col col_8">
							<h2 class="section_heading"><?php echo $copy[LANG]['prizes_heading']; ?></h2>
							<p class="section_description"><?php echo $copy[LANG]['prizes_description']; ?></p>
						</div>
						<div class="col col_2"></div>
				</div>
				<div class="col_wrapper extra_padding">
						
						<div class="col col_6 prize_box">
							<img src="images/wj20-front-page__prizes-plane.png">
							<?php ?>
							<h3 class="section_subheading"><?php echo $copy[LANG]['loggedoutprize2_heading']; ?></h3>
							<?php ?>
							<p class="body_copy"><?php echo $copy[LANG]['loggedoutprize2_description']; ?></p>
						</div>
						<div class="col col_6 prize_box">
							<img src="images/wj20-front-page__prizes-mex.png">
							<?php ?>
							<h3 class="section_subheading"><?php echo $copy[LANG]['loggedoutprize1_heading']; ?></h3>
							<?php ?>
							<p class="body_copy"><?php echo $copy[LANG]['loggedoutprize1_description']; ?></p>
						</div>
				</div>
			</div>
		</div>

	</div>

<?php
	include('footer.php');
?>