<?php
	require_once('includes/config.php');
//	require_once('check_session.php');
		
	$reset_token = (isset($_GET['token']))? $_GET['token'] : null;

	include('header.php');
?>
	<div id="main">
	
	
		<div id="banner">
			<div class="container">
				<div class="col_wrapper hero__banner-over">
					<?php if(LANG == 'en'): ?>
					<div class="col col_6 bg_overlay">
					<?php elseif(LANG == 'fr'): ?>
					<div class="col col_6 bg_overlay">
					<?php endif; ?>
						<h1 class="section_heading"><?php echo $copy[LANG]['heading']; ?></h1>
						<p class="section_description"><?php echo $copy[LANG]['description']; ?></p>
						<p><a href="javascript:void(0);" class="btn btn-default login_link"><?php echo $copy[LANG]['enter_now']; ?></a></p>
					</div>
					<div class="col col_6"></div>
					</div>
				</div>
			</div>
		</div>
		
	<div id="how_to_enter" class="section">
		<div class="container">
			<div class="col_wrapper">
				<div class="col col_2"></div>
				<div class="col col_8">
					<h2 class="section_heading"><?php echo $copy[LANG]['how_to_enter_heading']; ?></h2>
					<p class="section_description"><?php echo $copy[LANG]['how_to_enter_description']; ?></p>
				</div>
				<div class="col col_2"></div>
			</div>
			<div class="col col_wrapper">
					<div class="col col_2">
					</div>
					<div class="col col_8">
							<div class="col col_4">
								<img src="images/palm.gif">
								<h3 class="section_subheading"><?php echo $copy[LANG]['submit_vacations_heading']; ?></h3>
								<p class="body_copy"><?php echo $copy[LANG]['submit_vacations_description']; ?></p>
							</div>
						<div class="col col_4">
							<img src="images/plane.gif">
							<h3 class="section_subheading"><?php echo $copy[LANG]['submit_flights_heading']; ?></h3>
							<p class="body_copy"><?php echo $copy[LANG]['submit_flights_description']; ?></p>
						</div>
							
						<div class="col col_4">
							<img src="images/trophy.gif">
							<h3 class="section_subheading"><?php echo $copy[LANG]['play_game_heading']; ?></h3>
							<p class="body_copy"><?php echo $copy[LANG]['play_game_description']; ?></p>
						</div>
					</div>
					<div class="col col_2">
					</div>
			</div>
			<div class="col_wrapper cta_buttons home__prizes_enter-btn">
				<div class="col col_12">
					<a href="javascript:void(0);" class="btn btn-default login_link"><?php echo $copy[LANG]['enter_now']; ?></a>
				</div>
			</div>
		</div>
	</div>

	<div id="prizes_banner" class="section lo">
		<div class="container">
			<div class="col_wrapper">
					<div class="col col_2"></div>
					<div class="col col_8">
						<h2 class="section_heading"><?php echo $copy[LANG]['prizes_heading']; ?></h2>
						<p class="section_description"><?php echo $copy[LANG]['prizes_description']; ?></p>
					</div>
					<div class="col col_2"></div>
			</div>
			<div class="col_wrapper extra_padding">
					
					<div class="col col_6 prize_box">
						<img src="images/wj20-front-page__prizes-plane.png">
						<?php ?>
						<h3 class="section_subheading"><?php echo $copy[LANG]['loggedoutprize2_heading']; ?></h3>
						<?php ?>
						<p class="body_copy"><?php echo $copy[LANG]['loggedoutprize2_description']; ?></p>
					</div>
					<div class="col col_6 prize_box">
						<img src="images/wj20-front-page__prizes-mex.png">
						<?php ?>
						<h3 class="section_subheading"><?php echo $copy[LANG]['loggedoutprize1_heading']; ?></h3>
						<?php ?>
						<p class="body_copy"><?php echo $copy[LANG]['loggedoutprize1_description']; ?></p>
					</div>
			</div>
		</div>
	</div>

<?php
	include('footer.php');
?>