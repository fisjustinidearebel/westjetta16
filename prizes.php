<?php
	require_once('includes/config.php');
//	require_once('check_session.php');
		
	include('header.php');
?>
	<div id="main" class="prizes_banner">
		<div id="prizes" class="section">
			<div class="container">
				<div class="col_wrapper">
					<div class="col col_1"></div>
					<div class="col col_10">
						<h1 class="section_heading"><?php echo $copy[LANG]['prizes_heading']; ?></h1>
						<p class="section_description"><?php echo $copy[LANG]['prizes_description']; ?></p>
					</div>
					<div class="col col_1"></div>
				</div>
			</div>
		</div>
		<div id="gen_prize_table" class="section">
			<div class="container">
				<div class="col_wrapper">
					<h1 class="section_heading"><?php echo $copy[LANG]['mex_heading']; ?></h1>
					<div class="col col_3 prize_box">
						<div class="prize_itembox" style="background-image:url('images/Grand-Bahia-Principe-Jamaica-and-San-Juan.png');">
						</div>
						<!-- <img src=""> -->
						<?php ?>
						<h3 class="section_subheading"><?php echo $copy[LANG]['grandbahia_heading']; ?></h3>
						<?php ?>
						<p class="body_copy"><?php echo $copy[LANG]['grandbahia_description']; ?></p>
					</div>
					<div class="col col_3 prize_box">
						<div class="prize_itembox" style="background-image:url('images/Grand-Bahia-Principe-Jamaica-and-San-Juan.png');">
						</div>
						<!-- <img src=""> -->
						<?php ?>
						<h3 class="section_subheading"><?php echo $copy[LANG]['grandbahiasj_heading']; ?></h3>
						<?php ?>
						<p class="body_copy"><?php echo $copy[LANG]['grandbahiasj_description']; ?></p>
					</div>
					<div class="col col_3 prize_box">
						<div class="prize_itembox" style="background-image:url('images/BuenaventuraGrandHotelSpa.png');">
						</div>
						<!-- <img src=""> -->
						<?php ?>
						<h3 class="section_subheading"><?php echo $copy[LANG]['buenaventura_heading']; ?></h3>
						<?php ?>
						<p class="body_copy"><?php echo $copy[LANG]['buenaventura_description']; ?></p>
					</div>
					<div class="col col_3 prize_box">
						<div class="prize_itembox" style="background-image:url('images/El-Cid-Castilla-Beach.png');">
						</div>
						<!-- <img src=""> -->
						<?php ?>
						<h3 class="section_subheading"><?php echo $copy[LANG]['elcid_heading']; ?></h3>
						<?php ?>
						<p class="body_copy"><?php echo $copy[LANG]['elcid_description']; ?></p>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="col_wrapper">
					<div class="col col_3 prize_box">
						<div class="prize_itembox" style="background-image:url('images/Honua-Kai-Resort-Spa.png');">
						</div>
						<!-- <img src=""> -->
						<?php ?>
						<h3 class="section_subheading"><?php echo $copy[LANG]['honua_heading']; ?></h3>
						<?php ?>
						<p class="body_copy"><?php echo $copy[LANG]['honua_description']; ?></p>
					</div>
					<div class="col col_3 prize_box">
						<div class="prize_itembox" style="background-image:url('images/Majestic-Mirage-Punta-Cana.png');">
						</div>
						<!-- <img src=""> -->
						<?php ?>
						<h3 class="section_subheading"><?php echo $copy[LANG]['majestic_heading']; ?></h3>
						<?php ?>
						<p class="body_copy"><?php echo $copy[LANG]['majestic_description']; ?></p>
					</div>
					<div class="col col_3 prize_box">
						<div class="prize_itembox" style="background-image:url('images/Club-Marival-Resort-Suites.png');">
						</div>
						<!-- <img src=""> -->
						<?php ?>
						<h3 class="section_subheading"><?php echo $copy[LANG]['clubmarival_heading']; ?></h3>
						<?php ?>
						<p class="body_copy"><?php echo $copy[LANG]['clubmarival_description']; ?></p>
					</div>
					<div class="col col_3 prize_box">
						<div class="prize_itembox" style="background-image:url('images/Grand-Palladium-Punta-Cana-Resort-Spa.png');">
						</div>
						<!-- <img src="images/Grand-Palladium-Punta-Cana-Resort-Spa.png"> -->
						<?php ?>
						<h3 class="section_subheading"><?php echo $copy[LANG]['grandpalladium_heading']; ?></h3>
						<?php ?>
						<p class="body_copy"><?php echo $copy[LANG]['grandpalladium_description']; ?></p>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="col_wrapper">
					<div class="col col_3 prize_box">
					</div>
					<div class="col col_3 prize_box">
						<div class="prize_itembox" style="background-image:url('images/Hyatt-Ziva-Cancun.png');">
						</div>
						<!-- <img class="prize_item" src="images/Hyatt-Ziva-Cancun.png"> -->
						<?php ?>
						<h3 class="section_subheading"><?php echo $copy[LANG]['hyattziva_heading']; ?></h3>
						<?php ?>
						<p class="body_copy"><?php echo $copy[LANG]['hyattziva_description']; ?></p>
					</div>
					<div class="col col_3 prize_box">
						<div class="prize_itembox" style="background-image:url('images/SandosPlayacarBeachResort.png');">
						</div>
						<!-- <img class="prize_item" src="images/SandosPlayacarBeachResort.png"> -->
						<?php ?>
						<h3 class="section_subheading"><?php echo $copy[LANG]['sandos_heading']; ?></h3>
						<?php ?>
						<p class="body_copy"><?php echo $copy[LANG]['sandos_description']; ?></p>
					</div>
					<div class="col col_3 prize_box">
					</div>
				</div>
			</div>
		</div>
		<div id="grand_prizes" class="section">
			<div class="container">
				<div class="col_wrapper">
					<div class="col col_1"></div>
					<div class="col col_10">
						<h1 class="section_heading"><?php echo $copy[LANG]['round_trip_flights_heading']; ?></h1>
					</div>
					<div class="col col_1"></div>
				</div>
			</div>
			<div class="container">
				<div class="col_wrapper">
					<div class="col col_6 prize_box">
						<img src="images/beach_anywhere.png">
						<?php ?>
						<p class="body_copy"><?php echo $copy[LANG]['mex_description']; ?></p>
					</div>
					<div class="col col_6 prize_box">
						<img src="images/golf_NA.png">
						<?php ?>
						<p class="body_copy"><?php echo $copy[LANG]['plane_description']; ?></p>
					</div>
				</div>
			</div>
		</div>
		<div id="cash_giveaways" class="section">
			<div class="container">
				<div class="col_wrapper cta_buttons prizes_bottom-cta">
					<div class="col col_12">
						<a href="home.php#bookings" class="btn btn-default"><?php echo $copy[LANG]['enter_now']; ?></a>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php
	include('footer.php');
?>