<?php
	require_once('includes/config.php');
	require_once('check_session.php');
		
	include('header.php');
?>
	<div id="main">
		<div id="how_to_enter" class="section">
			<div class="container">
				<div class="col_wrapper">
					<div class="col col_1"></div>
					<div class="col col_10">
					<!-- NOTE: Different from logged out state -->
						<h1 class="section_heading"><?php echo $copy[LANG]['how_to_enter_2_heading']; ?></h1>
						<p class="section_description"><?php echo $copy[LANG]['how_to_enter_2_description']; ?></p>
					</div>
					<div class="col col_1"></div>
				</div>
				<div class="col_wrapper extra_padding">
					<div class="col col_4">
						<img src="images/plane.gif">
						<h3 class="section_subheading"><?php echo $copy[LANG]['submit_flights_heading']; ?></h3>
						<p class="body_copy"><?php echo $copy[LANG]['submit_flights_description']; ?></p>
					</div>
					
					<div class="col col_4">
						<img src="images/palm.gif">
						<h3 class="section_subheading"><?php echo $copy[LANG]['submit_vacations_heading']; ?></h3>
						<p class="body_copy"><?php echo $copy[LANG]['submit_vacations_description']; ?></p>
					</div>
					
					<div class="col col_4">
						<img src="images/trophy.gif">
						<h3 class="section_subheading"><?php echo $copy[LANG]['play_game_heading']; ?></h3>
						<p class="body_copy"><?php echo $copy[LANG]['play_game_2_description']; ?></p>
					</div>
				</div>
				<div class="col_wrapper cta_buttons">
					<div class="col col_12">
						<a href="home.php#bookings" class="btn btn-default"><?php echo $copy[LANG]['enter_bookings']; ?></a>
						<a href="home.php#game" class="btn btn-default"><?php echo $copy[LANG]['play_the_game']; ?></a>
					</div>
				</div>
				<!--
				<div class="col col_1"></div>
				<div class="col_wrapper cta_buttons extra_padding">
						<div class="col col_5">
							<a href="home.php#bookings" class="btn btn-default"><?php echo $copy[LANG]['enter_bookings']; ?></a>
						</div>
						<div class="col col_4">
							<a href="home.php#game" class="btn btn-default"><?php echo $copy[LANG]['play_the_game']; ?></a>
						</div>
					
				</div>
				-->
			</div>
		</div>
	</div>

<?php
	include('footer.php');
	exit;
?>