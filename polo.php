<?php
/**
 * polo.php
 *
 * This is a heartbeat monitor application used to monitor our websites. This file
 * reports "polo!" when everything is okay. Optionally we can verify a db connection.
 *
 * @author            E. Scott Eastman <scott@idearebel.com>
 * @date              2012 Nov
 * @package           Marco Polo
 */

// Make sure results are not cached
$ts = gmdate("D, d M Y H:i:s") . " GMT";
header("Expires: $ts");
header("Last-Modified: $ts");
header("Pragma: no-cache");
header("Cache-Control: no-cache, must-revalidate");

/*
//
// Word Press
//
// Use the DB Creds from the wp-config.php.
//

// the wp-config includes wp-settings.php which need the
// ABSPATH to be set so the include doesn't fail
define('ABSPATH', getcwd().'/');
require_once('wp-config.php');

$host = DB_HOST;
$user = DB_USER;
$pwd = DB_PASSWORD;
//*/

/*

//
// Yii
//
// Use the DB Creds from protected/config/<HTTP_HOST>
//

$file = $_SERVER['HTTP_HOST'] . '.php';
$yii_config = require_once('protected/config/' . $file);

$con_string = $yii_config['components']['db']['connectionString'];
$parts = explode(';', $con_string);
$host = str_replace('mysql:host=', '', $parts[0]);

$user = $yii_config['components']['db']['username'];
$pwd = $yii_config['components']['db']['password'];
//*/

/*
//
// IR Custom
//
// Use the include/connect.php to test the db connection
//

require_once('includes/connect.php');

//*/

/*
//
// Joomla 1.5 compatibility layer
//

require_once('configuration.php');

if (isset($password)) {
	$pwd = $password;
} else {
	$pwd = '';
}
//*/

/*
//
//Joomla 2.5+ compatibility layer
//

require_once('configuration.php');

if (isset($password)) {
	$pwd = $password;
} else {
	$pwd = '';
}
//*/

/*
// osCommerce

require_once('includes/configure.php');

$host = DB_SERVER;
$user = DB_SERVER_USERNAME;
$pwd = DB_SERVER_PASSWORD;

//*/

/*
// AlphaCore

$config = require_once('../config/config.application.php');

$host = $config['database']['master']['host'];
$user = $config['database']['master']['user'];
$pwd = $config['database']['master']['password'];

//*/

//*
//
// Other
//
// Use the following DB Creds
//
	require_once 'includes/config.php';

$host = DB_HOST;
$user = DB_USER;
$pwd = DB_PASS;

//*/



// Check that we can access the db (if db params are set)
if ( isset($host) && !empty($host) )
	mysql_connect($host, $user, $pwd) or die();

// Answer back that everything is ok
echo "polo!";

