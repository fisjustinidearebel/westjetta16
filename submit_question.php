<?php
	require_once 'includes/config.php';
	header('Content-Type: text/html; charset=ISO-8859-1'); 
	$valid = true;
	$errors = array();

	$user_id = $_SESSION['user']['user_id'];
	$question_id = (isset($_POST['question_id']))? escape($_POST['question_id']) : null;
	$user_answer = (isset($_POST['question_answer']))? escape($_POST['question_answer']) : null;
	$user_answers = array();
	
	// validate
	if(empty($question_id) || empty($user_answer)){
		$return = array('status'=>'error', 'message_title'=>$copy[LANG]['error'], 'errors'=>$copy[LANG]['submission_error']);
		echo json_encode($return);
		exit;
	}
	
	//put answers in array
	if(is_array($user_answer)){
		foreach($user_answer as $answer_id){
			$user_answers[] = "($user_id, $question_id, $answer_id)";
		}
	}else{
		$user_answers[] = "($user_id, $question_id, $user_answer)";
	}
	// create insert string
	$insert_str = implode(',', $user_answers);
	
	// delete any previous answers
	$sql = "DELETE FROM user_answer WHERE user_id = $user_id AND question_id = $question_id";		
	if(!$result = $db->query($sql)){
		$return = array('status'=>'error', 'message_title'=>$copy[LANG]['error'], 'errors'=>$copy[LANG]['submission_error']);
		echo json_encode($return);
		exit;
	}

	// insert new answers
	$sql = "INSERT INTO user_answer (user_id, question_id, answer_id) VALUES $insert_str";		
	if(!$result = $db->query($sql)){
		$return = array('status'=>'error', 'message_title'=>$copy[LANG]['error'], 'errors'=>$copy[LANG]['submission_error']);
		echo json_encode($return);
		exit;
	}

	// determine point value earned from question 
	$sql = "SELECT 
				point_value
			FROM answer a
			LEFT JOIN question q USING(question_id) 
			LEFT JOIN user_answer ua USING(answer_id)
			WHERE num_correct_answers = 
				(
					SELECT 
						COUNT(answer_id) 
					FROM user_answer 
					WHERE user_id = ua.user_id 
						AND question_id = q.question_id
				)
				AND num_correct_answers = 
				(
					SELECT 
						COUNT(answer_id) 
					FROM user_answer LEFT JOIN answer USING(answer_id) 
					WHERE user_id = ua.user_id 
						AND user_answer.question_id = q.question_id
						AND correct ='1'
				)
				AND user_id = $user_id 
				AND q.question_id = $question_id";
	if(!$result = $db->query($sql)){
		$return = array('status'=>'error', 'message_title'=>$copy[LANG]['error'], 'errors'=>$copy[LANG]['submission_error']);
		echo json_encode($return);
		exit;
	}
	if($result->num_rows == 0){
		$point_value = 0;
	}else{
		$row = $result->fetch_assoc();
		$point_value = $row['point_value'];
	}
	
	// record points earned for this question
	$sql = "INSERT INTO user_question_points (user_id, question_id, points) 
			VALUES ( $user_id, $question_id, $point_value )
			ON DUPLICATE KEY UPDATE
				points = VALUES(points);";
	if(!$result = $db->query($sql)){
		$return = array('status'=>'error', 'message_title'=>$copy[LANG]['error'], 'errors'=>$copy[LANG]['submission_error']);
		echo json_encode($return);
		exit;
	}
	
	// update user points
 	update_user_points($user_id, $point_value);

	// get question html
	$question_html = get_single_question_html($question_id);

	$return = array('status'=>'ok', 'question_html'=>$question_html);

	$event = get_active_event();
	$user_results = get_user_question_results_for_event($event['event_id'], $user_id);
	
	if($user_results['num_answered'] == $event['num_questions']){
		$next_event = get_next_event();
		if (LANG == 'fr'){
			setlocale(LC_TIME, 'fr_FR');
			$date = strtolower(strftime('%A %e %B', strtotime($next_event['start_date'])));
			setlocale(LC_TIME, NULL);
		}else{
			$date = date('l F jS', strtotime($next_event['start_date']));
		}
		$complete_message = ($next_event)? sprintf($copy[LANG]['questions_complete_message'], $user_results['points_earned'], $event['possible_points']) : sprintf($copy[LANG]['questions_complete_message'], $user_results['points_earned'], $event['possible_points']);
		
		$return['message_title'] = $copy[LANG]['congrats'];
		$return['message'] = $complete_message;
	}
    
    //French copy needs to be reencoded as UTF8 in order fro json_encode to work (returns false otherwise)
	//$utf_result = array_map('utf8_encode', $return);
	//echo json_encode($utf_result);
	echo json_encode($return);
	exit;
?>
