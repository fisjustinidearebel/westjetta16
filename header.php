<!DOCTYPE html>
<html>
<head>
	<title><?php echo $copy[LANG]['title']; ?></title>
	<link rel="icon" type="image/x-icon" href="/favicon.ico" />

	<meta charset="utf-8">
	<meta name="description" content="<?php echo $copy[LANG]['description_header']; ?>">
	
	<!-- jquery -->
	<script src="assets/jquery-1.11.2.min.js"></script>
	
	<!-- jquery.ui -->
	<script src="assets/jquery.ui-1.11.4.min.js"></script>
	<!-- jquery.ui.touch -->
	<script src="assets/jquery.ui.touch.js"></script>
	
	<!-- jquery.qaptcha -->
	<script src="assets/qaptcha/jquery.qaptcha.min.js"></script>
	<link type="text/css" href="assets/qaptcha/jquery.qaptcha.css" rel="stylesheet">

	<!-- font awesome -->
	<link type="text/css" href="assets/font-awesome/css/font-awesome.css" rel="stylesheet">


	<link rel="stylesheet" href="assets/flexslider.css" type="text/css">
	<script src="assets/jquery.flexslider-min.js"></script>


	<link type="text/css" href="styles.css?v=1" rel="stylesheet">
	<script type="text/javascript" src="scripts.js?v=1"></script>
</head>
<body class="<?php echo LANG; ?>">
	<noscript>
		<div class="nojs_alert"><div>JavaScript must be enabled to use this site. Please enable JavaScript in your browser and refresh the page.</div></div>
	</noscript>
	<div id="wrapper">
		<div id="navbar">
			<div class="container">
				<div id="logo">
					<a href="home.php" class="home_button">
						<img src="images/logo.gif" type="" media="">
					</a>
				</div>
				<div id="user_nav">
					<ul>
						<?php if (isset($_SESSION['user'])): ?>
							<li><a href="logout.php"><?php echo $copy[LANG]['logout']; ?></a></li>
						<?php else: ?>
							<li><a href="javascript:void(0);" class="login_link"><?php echo $copy[LANG]['login'].' / '.$copy[LANG]['register']; ?></a></li>
						<?php endif; ?>
						<li class="languages">
							<ul>
								<?php if(LANG == 'en'): ?>
								<li><?php echo $copy[LANG]['english']; ?></li>
								<li><a href="<?php echo FRENCH_URL; ?>"><?php echo $copy[LANG]['french']; ?></a></li>
								<?php else: ?>
								<li><?php echo $copy[LANG]['french']; ?></li>
								<li><a href="<?php echo ENGLISH_URL; ?>"><?php echo $copy[LANG]['english']; ?></a></li>
								<?php endif; ?>
							</ul>
						</li>
					</ul>
				</div>
				<?php if (isset($_SESSION['user'])): ?>
					<div id="site_nav">
						<ul>
							<?php if(LANG=='en'): ?>
								<li><a href="home.php"><?php echo $copy[LANG]['home']; ?></a></li>
								<li><a href="prizes.php"><?php echo $copy[LANG]['prizes']; ?></a></li>
								<li><a href="how_to_enter.php"><?php echo $copy[LANG]['how_to_enter']; ?></a></li>
								<li><a href="leaderboard.php"><?php echo $copy[LANG]['leaderboard']; ?></a></li>
							<?php else: ?>
								<li><a href="home.php"><?php echo $copy[LANG]['home']; ?></a></li>
								<li><a href="prizes.php"><?php echo $copy[LANG]['prizes']; ?></a></li>
								<li><a href="how_to_enter.php"><?php echo $copy[LANG]['how_to_enter']; ?></a></li>
								<li><a href="leaderboard.php"><?php echo $copy[LANG]['leaderboard']; ?></a></li>
							<?php endif; ?>

							
							<li class="welcome"><?php echo sprintf($copy[LANG]['welcome'], $_SESSION['user']['username'], $_SESSION['user']['total_points']); ?></li>
						</ul>
					</div>
				<?php endif; ?>
				
				
				

			</div>
		</div>