<?php
	require_once('includes/config.php');
	require_once('check_session.php');
		
	include('header.php');
?>
	<div id="main">
		<div id="prizes" class="section">
			<div class="container">
				<div class="col_wrapper">
					<div class="col col_1"></div>
					<div class="col col_10">
						<h1 class="section_heading"><?php echo $copy[LANG]['prizes_heading']; ?></h1>
						<p class="section_description"><?php echo $copy[LANG]['prizes_description']; ?></p>
					</div>
					<div class="col col_1"></div>
				</div>
			</div>
		</div>

		
		
		<div id="grand_prizes" class="section">
			<div class="container">
				<div class="col_wrapper">
					<div class="col col_1"></div>
					<div class="col col_10">
						<h2 class="section_heading"><?php echo $copy[LANG]['grand_prize_heading']; ?></h2>
					</div>
					<div class="col col_1"></div>
				</div>


				<div class="col_wrapper extra_padding">
					<div class="col col_4" >
						<img src="images/la_brisas2.jpg">
					</div>
					<div class="col col_4">
						<img src="images/grand_riviera_princess2.jpg">
					</div>
					<div class="col col_4">
						<img src="images/crown_paradise_golden2.jpg">
					</div>
				</div>
				<div class="col_wrapper extra_padding">
					<div class="col col_4">
						<!--<h3 class="section_subheading"><?php echo $copy[LANG]['la_brisas_heading']; ?></h3>-->
						<p class="body_copy"><?php echo $copy[LANG]['la_brisas_description']; ?></p>
					</div>
					<div class="col col_4">
						<!--<h3 class="section_subheading"><?php echo $copy[LANG]['grand_riviera_princess_heading']; ?></h3>-->
						<p class="body_copy"><?php echo $copy[LANG]['grand_riviera_princess_description'];?></p>
					</div>
					<div class="col col_4">
						<!--<h3 class="section_subheading"><?php echo $copy[LANG]['crown_paradise_golden_heading']; ?></h3>-->
						<p class="body_copy"><?php echo $copy[LANG]['crown_paradise_golden_description'];?></p>
					</div>
				</div>
				<div class="col_wrapper extra_padding" style="margin-top:20px;">
					<div class="col col_4">
						<img src="images/crocs_casino_resort2.jpg">
					</div>
					<div class="col col_4" style="padding:0px 10px">
						<img src="images/royal_lahaina2.jpg">
					</div>
					<div class="col col_4">
						<img src="images/mgm_resorts.jpg">
					</div>
				</div>
				<div class="col_wrapper extra_padding">
					<div class="col col_4">
						<!--<h3 class="section_subheading"><?php echo $copy[LANG]['crocs_casino_resort_heading']; ?></h3>-->
						<p class="body_copy"><?php echo $copy[LANG]['crocs_casino_resort_description'];?></p>
					</div>
					<div class="col col_4" style="padding:0px 10px">
						<!--<h3 class="section_subheading"><?php echo $copy[LANG]['royal_lahaina_resort_heading'];?></h3>-->
						<p class="body_copy"><?php echo $copy[LANG]['royal_lahaina_resort_description'];?></p>
					</div>
					<div class="col col_4">
						<!--<h3 class="section_subheading"><?php echo $copy[LANG]['mandalay_bay_heading']; ?></h3>-->
						<p class="body_copy"><?php echo $copy[LANG]['mandalay_bay_description'];?></p>
					</div>
				</div>

				<div class="col_wrapper extra_padding" style="margin-top:20px;">
					<div class="col col_4">
					</div>
					<div class="col col_4">
						<img src="images/springhill_suites_marriott.jpg">
						<?php?>
						<!--<h3 class="section_subheading"><?php echo $copy[LANG]['springhill_suites_marriot_heading']; ?></h3>-->
						<?php?>
						<p class="body_copy"><?php echo $copy[LANG]['springhill_suites_marriot_description'];?></p>
					</div>
				</div>
				<div class="col col_4">
				</div>
			</div>
		</div>
		
		<div id="round_trip_flights" class="section">
			<div class="container">
				<div class="col_wrapper">
					<div class="col col_1"></div>
					<div class="col col_10">
						<h2 class="section_heading"><?php echo $copy[LANG]['round_trip_flights_heading']; ?></h2>
					</div>
					<div class="col col_1"></div>
				</div>
				<div class="col_wrapper extra_padding">
					<div class="col col_4">
						<img src="images/WestJet_RTflight8.png">
						<p class="body_copy"><?php echo $copy[LANG]['flight_prize_1_description']; ?></p>
					</div>
					<div class="col col_4">
						<img src="images/WestJet_RTflight6.png">
						<p class="body_copy"><?php echo $copy[LANG]['flight_prize_2_description']; ?></p>
					</div>
					<div class="col col_4">
						<img src="images/WestJet_RTflight7.png">
						<p class="body_copy"><?php echo $copy[LANG]['flight_prize_3_description']; ?></p>
					</div>
				</div>
			</div>
		</div>
		
		<div id="cash_giveaways" class="section">
			<div class="container">
				<!--
				<div class="col_wrapper">
					<div class="col col_1"></div>
					<div class="col col_10">
						<h2 class="section_heading"><?/*php echo $copy[LANG]['cash_giveaways_heading']; */?></h2>
					</div>
					<div class="col col_1"></div>
				</div>
				<div class="col_wrapper extra_padding">
					<div class="col col_12">
						<img src="images/cash_prize_1.jpg">
						<h3 class="section_subheading"><?/*php echo $copy[LANG]['cash_prize_1_heading']; */?></h3>
					</div>
					
					<div class="col col_6">
						<img src="images/cash_prize_2.jpg">
						<h3 class="section_subheading"><?/*php echo $copy[LANG]['cash_prize_2_heading']; */?></h3>
					</div>
					
				</div>
				-->
				<div class="col_wrapper cta_buttons">
					<div class="col col_12">
						<a href="home.php#bookings" class="btn btn-default"><?php echo $copy[LANG]['enter_bookings']; ?></a>
						<a href="home.php#game" class="btn btn-default"><?php echo $copy[LANG]['play_the_game']; ?></a>
					</div>
				</div>
			</div>
		</div>
		
		<!--
		<div id="prizes_cta" class="section">
			<div class="container">
				<div class="col_wrapper">
					<div class="col col_1"></div>
					<div class="col col_10">
						<h1 class="section_heading"><?/*php echo $copy[LANG]['client_prizes_heading']; */?></h1>
						<p class="section_description"><?/*php echo $copy[LANG]['client_prizes_description']; */?></p>
					</div>
					
				</div>
			</div>
		</div>
		-->

	</div>

<?php
	include('footer.php');
?>