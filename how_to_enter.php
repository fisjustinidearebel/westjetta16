<?php
	require_once('includes/config.php');
//	require_once('check_session.php');
		
	include('header.php');
?>
	<div id="main">
		<div id="how_to_enter" class="section">
			<div class="container">
				<div class="col_wrapper">
					<div class="col col_2"></div>
					<div class="col col_8">
						<h1 class="section_heading"><?php echo $copy[LANG]['how_to_enter_heading']; ?></h1>
						<p class="section_description"><?php echo $copy[LANG]['how_to_enter_description']; ?></p>
					</div>
					<div class="col col_2"></div>
				</div>
				<div class="col_wrapper">
					<div class="col col_2"></div>
					<div class="col col_8">
						<div class="col_wrapper">
							<div class="col col_4">
								<img src="images/palm.gif">
								<h3 class="section_subheading"><?php echo $copy[LANG]['submit_vacations_heading']; ?></h3>
								<p class="body_copy"><?php echo $copy[LANG]['submit_vacations_description']; ?></p>
							</div>
							<div class="col col_4">
								<img src="images/plane.gif">
								<h3 class="section_subheading"><?php echo $copy[LANG]['submit_flights_heading']; ?></h3>
								<p class="body_copy"><?php echo $copy[LANG]['submit_flights_description']; ?></p>
							</div>
							<div class="col col_4">
								<img src="images/trophy.gif">
								<h3 class="section_subheading"><?php echo $copy[LANG]['play_game_heading']; ?></h3>
								<p class="body_copy"><?php echo $copy[LANG]['play_game_description']; ?></p>
							</div>
						</div>
					</div>
					<div class="col col_2"></div>
				</div>
				<div class="col_wrapper cta_buttons hte_bottom">
					<div class="col col_12">
						<a href="home.php#bookings" class="btn btn-default howto__play-btn"><?php echo $copy[LANG]['enter_now']; ?></a>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php
	include('footer.php');
	exit;
?>